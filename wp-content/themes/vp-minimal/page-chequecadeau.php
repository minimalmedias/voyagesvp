<?php
/** Template Name: chequecadeau */
get_header();
minimal_get_template_part('/templates/heading.php', array('caption' => get_field('cadeau-header_blurb')));
$ff = get_fields();
//print_r($ff);
?>
    <section class="spaced-top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-7 text-center">
                    <h3 class="mb-5 py-0 mx-auto"><?php the_field('cadeau-blurb_blurb'); ?></h3>
                </div>
            </div>
        </div>
    </section>
    <section class="reveal">
        <?php
        #Text + Image
        minimal_get_template_part('/templates/image-texte.php', array(
            'image' => array(
                'url' => standardimage(get_field('cadeau-bloc_image')),
                'alt' => get_field('cadeau-bloc_label'),
                'classes' => 'img-fluid my-md-5'
            ),
            'text' => array(
                'subtitle' => get_field('cadeau-bloc_title'),
                'classes' => 'text-left',
                'text' => get_field('cadeau-bloc_text'),
            ),
            'overlap' => true,
            'layout' => 'text-image'
        ));
        ?>
    </section>
    <section class="spaced-top reveal">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-7 text-center">
                    <h3 class="mb-5 py-0 mx-auto"><?php the_field('cadeau-blurb2_blurb'); ?></h3>
                </div>
            </div>
        </div>
    </section>
    <section id="giftdetails">
        <div class="container my-md-5">
            <div class="row justify-content-center mb-md-5">
                <?php
                $giftdetails = array(
                    'gift' => __('UNIQUE GIFT', 'vp'),
                    'money' => __('CHOOSE THE VALUE YOU WANT TO OFFER', 'vp'),
                    'infinity' => __('NO EXPIRY DATE', 'vp')
                );
                foreach ($giftdetails as $k => $d) {
                    ?>
                    <div class="col-sm-3 px-5 mb-5">
                        <div class="icon icon-<?php echo $k; ?>"></div>
                        <h5 class="px-lg-5"><?php echo $d; ?></h5>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="container">
        <?php
        $lang = get_locale();
        $form['fr'] = '[contact-form-7 id="17545" title="Page Certificat Cadeau FR" html_class="form text-left use-floating-validation-tip"]';
        $form['en'] = '[contact-form-7 id="17546" title="Page Certificat Cadeau EN" html_class="form text-left use-floating-validation-tip"]';
        echo do_shortcode($form[ICL_LANGUAGE_CODE]);
        ?>
        </div>
    </section>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>