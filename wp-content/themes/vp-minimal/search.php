<?php get_header(); ?>

<section class="min-screen">
    <div class="container">
        <?php
        $posttype = array();

        $the_posts = get_posts(array('post_type' => 'post_type_name'));

        if (have_posts()):
            $types = array(
                'post' => __("Articles", "wpml_theme"),
                'voyages' => __("Voyages", "wpml_theme")
            );
            ?>
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <table class="table table-sm search-results" width="100%">
                        <?php
                        foreach ($types as $type => $name) {
                            $i = 0;
                            while (have_posts()) {
                                the_post();
                                if ($type == get_post_type()) {
                                    if ($i == 0) {
                                        ?>
                                        <tr>
                                            <td colspan="2"><br><h4><?php echo ($name!='')?$name:'Groupe Voyages VP'; ?></h4></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                $i++;
                            }

                            while (have_posts()) {
                                the_post();
                                if ($type == get_post_type()) {
                                ?>
                                <tr>
                                    <?php
                                        $taxonomy_type = get_the_terms(get_the_ID(), 'type');
                                        if (!empty($taxonomy_type)) {
                                            $cat_name = $taxonomy_type[0]->name; // Voyage
                                        } else {
                                            $cat_name = 'Groupe Voyages VP';
                                            $categories = get_the_category();
                                            if($categories) {
                                                $cat_name = $categories[0]->cat_name; // Article
                                            }
                                        }
                                        $cat_name = str_replace('|', '<br>', $cat_name);
                                        ?>
                                        <td width="1%" class="text-right"><h5><span
                                                        class="label"><?php echo $cat_name; ?></span></a></h5></td>
                                        <td width="99%"><h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                            </h5></td>
                                </tr>
                                <?php } ?>
                            <?php }
                            rewind_posts();
                        }
                        ?>
                    </table>
                </div>
            </div>
            <?php
        else:
            ?>
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <h4><?php echo __("No result for this search.", "vp"); ?></h4>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>
