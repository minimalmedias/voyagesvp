<?php /** Template Name: contact */
get_header();
minimal_get_template_part('/templates/heading.php', array(
    'nav' => true,
    'caption' => "",
    'subcaption' => ""
));
minimal_get_template_part('/templates/heading-details.php', array(
    'classes' => 'col-12 col-md-7 col-lg-6 col-xl-5',
    'header' => __('<h1>Contact Us</h1>','vp'),
    'details' => array(
        array(
            "icon" => "icon-telephone",
            "title" => __("Telephone", "vp"),
            "text" => array(
                "514 939-9999 /",
                "1 888 713-3311"
            )
        ),
        /*
        array(
            "icon" => "icon-chat",
            "title" => __("Clavardage", "wpml_theme"),
            "text" => __("9 AM - 7 PM", "wpml_theme")
        ),
        */
        array(
            "icon" => "icon-time",
            "title" => __("Opening hours", "vp"),
            "text" => array(
                __('MON-FRI : 8:30 to 6:00', 'vp'),
                __('SAT-SUN : closed','vp')
            )
        ),
        array(
            "icon" => "icon-map",
            "title" => __("Address", "vp"),
            "text" => array(
                __('355 Sainte-Catherine St. W. Office 601', 'vp'),
                __('Montreal, QC H3B 1A5 CANADA', 'vp')
            ),
            'url' => 'https://www.google.ca/maps/place/355+Rue+Sainte-Catherine+O,+Montr%C3%A9al,+QC+H3B/@45.5061239,-73.5695164,17z/data=!3m1!4b1!4m5!3m4!1s0x4cc91a4f79615c17:0x519a00457a2c10b3!8m2!3d45.5061239!4d-73.5673277'
        ),
    )));
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 col-lg-8">
                <?php
                $lang = get_locale();
                $form['fr'] = '[contact-form-7 id="17543" title="Page Contact FR" html_class="form use-floating-validation-tip"]';
                $form['en'] = '[contact-form-7 id="17544" title="Page Contact EN" html_class="form use-floating-validation-tip"]';
                echo do_shortcode($form[ICL_LANGUAGE_CODE]);
                /*
                <form class="form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="h3 subsubheading"><?php _e('Votre projet', 'vp'); ?></h2>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-md-6 col-lg-5">
                                <div class="form-group">
                                    <label><?php _e('Destination', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Date de départ', 'vp'); ?></label>
                                    <input type="text" class="form-control" data-provide="datepicker">
                                    <span class="datepicker-icon"></span>
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Durée (en jours)', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Nombre d\'adulte(s)', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Nombre d\'enfant(s)', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-5">
                                <div class="form-group fullheight">
                                    <label><?php _e('Décrivez votre projet', 'vp'); ?></label>
                                    <textarea type="text" class="form-control fullheightfit"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="h3 bordered subsubheading"><?php _e('Vos informations', 'vp'); ?></h2>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-md-6 col-lg-5">
                                <div class="form-group">
                                    <label><?php _e('Nom', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Prénom', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Adresse', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Appartement', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Code postal', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-5">
                                <div class="form-group">
                                    <label><?php _e('Ville', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Pays', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Téléphone', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Email', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Comment vous nous avez connu ?', 'vp'); ?></label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-center spaced">
                                <input type="submit" value="<?php _e('Envoyer', 'vp'); ?>" class="btn btn-secondary">
                            </div>
                        </div>
                    </div>
                </form>
                */
                ?>
            </div>
        </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2796.196449694969!2d-73.56951638444112!3d45.50612387910161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc91a4f79615c17%3A0x519a00457a2c10b3!2s355+Rue+Sainte-Catherine+O%2C+Montr%C3%A9al%2C+QC+H3B!5e0!3m2!1sen!2sca!4v1509983655182" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>
