<?php
/** Template Name: Exception */
get_header();
#Grille Voyages
minimal_get_template_part('/templates/evasion-exception.php', array(
    'page' => 'exception',
    'subcaption' => ((ICL_LANGUAGE_CODE=='fr')?'Exception':'Extraordinary Travel'),
    'filtresections' => filtresections('exception',ICL_LANGUAGE_CODE)
));
get_footer();
?>
