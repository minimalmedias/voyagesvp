<?php /** Template Name: inspirations */
get_header();
minimal_get_template_part('/templates/heading.php');

$get_categories = get_terms(
    'category',
    array(
        'parent' => 0,
        'exclude' => array(67, 477)
    )
);
$categories = array();
foreach ($get_categories as $category) {
    $posts = get_posts(array('post_type' => 'post', 'category' => $category->cat_ID));
    $countposts = count($posts);
    if ($countposts > 0) {
        $categories[] = $category;
    }
}
if (!empty($categories)):
    ?>
    <section class="searchform">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="dropdown d-inline-block mx-auto">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="inspirationstypes"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <?php
                            $default = (ICL_LANGUAGE_CODE == 'fr') ? 'Thématiques' : 'Themes';
                            $temoign = (ICL_LANGUAGE_CODE == 'fr') ? 'Témoignages' : 'Testimonials';
                            echo (get_query_var('filter') == 'temoignages')?$temoign:$default;
                            ?>
                        </button>
                        <div class="dropdown-menu inspirationstypes" aria-labelledby="inspirationstypes">
                            <?php
                            foreach ($categories as $c) {
                                $active = (get_query_var('filter') == 'temoignages' && $c->slug == 'temoignages')?'active':'';
                                echo '<a class="dropdown-item ' . $active . '" href="#' . $c->slug . '">' . $c->name . '</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php else: ?>
    <br><br><br>
<?php endif; ?>
    <section>
        <div class="container">
            <div class="row" id="articleloader"></div>
        </div>
    </section>
<?php minimal_get_template_part('/templates/newsletter.php', array('classes' => 'spaced-top')); ?>
<?php get_footer(); ?>