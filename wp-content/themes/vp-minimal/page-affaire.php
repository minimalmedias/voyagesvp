<?php
/** Template Name: affaire */
get_header();
minimal_get_template_part('/templates/heading.php', array(
    'caption' => __('Your time is precious. Let us take care of the details.', 'vp'),
    'subcaption' => __('', 'vp'),
    'button' => array(
        'url' =>  '#'.__('demandersoumission'),
        'label' => __("Let's begin", 'vp')
    )
));
?>
<?php if (get_field('affaire_bloc-1_blurb') != '') { ?>
    <section class="description">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-5 text-center">
                    <h3 class="my-5 py-0 mx-auto"><?php the_field('affaire_bloc-1_blurb'); ?></h3>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<section class="spaced-top">
    <?php
    #Text + Image
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-2_image')),
            'alt' => get_field('affaire_bloc-2_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-2_title'),
            'classes' => 'text-center',
            'text' => get_field('affaire_bloc-2_text'),
        ),
        'layout' => 'image-text'
    ));
    ?>
</section>
<section id="<?php _e('premium-assistance', 'vp'); ?>">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class="h1 subheading"><?php _e('Premium assistance', 'vp'); ?></h2>
            </div>
        </div>
    </div>
    <?php
    #Text + Image
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-3_image')),
            'alt' => get_field('affaire_bloc-3_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-3_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-3_text'),
        ),
        'overlap' => true,
        'layout' => 'text-image'
    ));
    ?>
    <?php
    #Image + Text
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-4_image')),
            'alt' => get_field('affaire_bloc-4_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-4_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-4_text'),
        ),
        'overlap' => true,
        'layout' => 'image-text'
    ));
    ?>
    <?php
    #Text + Image
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-5_image')),
            'alt' => get_field('affaire_bloc-5_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-5_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-5_text'),
            'button' => array(
                'label' => get_field('affaire_bloc-5_button_label'),
                'url' => get_field('affaire_bloc-5_button_link')
            )
        ),
        'overlap' => true,
        'layout' => 'text-image'
    ));
    ?>
    <?php
    #Image + Text
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-6_image')),
            'alt' => get_field('affaire_bloc-6_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-6_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-6_text'),
            'button' => array(
                'label' => get_field('affaire_bloc-6_button_label'),
                'url' => get_field('affaire_bloc-6_button_link')
            )
        ),
        'overlap' => true,
        'layout' => 'image-text'
    ));
    ?>

    <?php
    #Affaire Apps Logos
    $appinfo = array();
    if (have_rows('app')):
        $appinfocount = 0;
        while (have_rows('app')) : the_row();
            $appinfo[$appinfocount] = array(
                'image' => get_sub_field('app_logo'),
                'text' => get_sub_field('app_description'),
                'url' => get_sub_field('app_link')
            );
            $appinfocount++;
        endwhile;
    endif;
    $appinfochunks = array_chunk($appinfo, 3);
    ?>
    <div class="container spaced-top">
        <div class="row justify-content-center">
            <div class="col-sm-7 text-center mb-md-5">
                <h3 class="mb-md-5 py-0 mx-auto"><?php _e('Indispensable travel apps', 'vp'); ?></h3>
            </div>
        </div>
        <?php foreach ($appinfochunks as $approw): ?>
            <div class="row row justify-content-center">
                <?php foreach ($approw as $app): ?>
                    <div class="col-lg-3 text-center">
                        <div class="card appinfo">
                            <a href="<?php echo $app['url']; ?>"><img
                                        src="<?php echo $app['image']['sizes']['applogo']; ?>" alt=""></a>
                            <div class="card-body">
                                <p class="card-text"><?php echo $app['text']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<section id="<?php _e('Meeting-and-event-planning','vp'); ?>" class="spaced-top">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class=""><?php _e('Meeting & event planning', 'vp'); ?></h2>
            </div>
        </div>
    </div>
    <?php
    #Text + Image
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-7_image')),
            'alt' => get_field('affaire_bloc-7_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-7_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-7_text'),
        ),
        'overlap' => true,
        'layout' => 'text-image'
    ));
    ?>
    <?php
    #Image + Text
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('affaire_bloc-8_image')),
            'alt' => get_field('affaire_bloc-8_image_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('affaire_bloc-8_title'),
            'classes' => '',
            'text' => get_field('affaire_bloc-8_text'),
        ),
        'overlap' => true,
        'layout' => 'image-text'
    ));
    ?>
    <div class="container spaced" id="<?php echo __('demandersoumission'); ?>">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5">
                <div class="text-cell ml-md-5 pull-mb-5 z-index-2">
                    <div class="lighterblue p-5">
                        <h3><?php the_field('affaire_bloc-9_title'); ?></h3>
                        <?php the_field('affaire_bloc-9_text'); ?>
                    </div>
                </div>
                <div class="image-cell mb-4 mb-md-0 mr-md-5 pull-mt-5">
                    <div class="zoomfx"><?php echo responsiveimage(array(
                            'url' => standardimage(get_field('affaire_bloc-9_image')),
                            'alt' => get_field('affaire_bloc-9_label'),
                            'classes' => 'img-fluid my-md-5'
                        )); ?></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-5">
                <div class="image-cell ml-md-5">
                    <div class="zoomfx"><?php echo responsiveimage(array(
                            'url' => standardimage(get_field('affaire_form_image_image')),
                            'alt' => get_field('affaire_form_image_label'),
                            'classes' => 'img-fluid my-md-5'
                        )); ?></div>
                </div>
                <div class="text-cell z-index-2 pull-mt-10 mr-md-5" id="<?php _e('soumission'); ?>">
                    <div class="lighterblue p-5">
                        <h3><?php _e('How can we help you?', 'vp'); ?></h3>
                        <?php
                        $lang = get_locale();
                        $form['fr'] = '[contact-form-7 id="17567" title="Page Affaire FR" html_class="form borderedinputs"]';
                        $form['en'] = '[contact-form-7 id="17568" title="Page Affaire EN" html_class="form borderedinputs"]';
                        echo do_shortcode($form[ICL_LANGUAGE_CODE]);
                        /*
                        <form class="form borderedinputs">
                            <?php
                            $checks = array(
                                'c1' => array('label' => __('Je souhaite recevoir une soumission pour mon entreprise.', 'vp'),
                                'c2' => array('label' => __('J\'aimerais qu\'on me négocie des contrats aériens et hôteliers.', 'vp'),
                                'c3' => array('label' => __('J\'ai besoin de conseils pour notre politique de voyages.', 'vp'),
                                'c4' => array('label' => __('Je veux réaliser jusqu\'à 20% d\'économie.', 'vp'),
                                'c5' => array('label' => __('Je veux un service 24/7.', 'vp')
                            );
                            foreach ($checks as $k => $check) {
                                ?>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="<?php echo $k; ?>" class="form-check-input">
                                        <?php echo $check['label']; ?>
                                    </label>
                                </div>
                            <?php } ?>
                            <div class="formspacer"></div>
                            <?php
                            $inputs = array(
                                'i1' => array('label' => __('Nom*', 'vp'), 'type' => 'text'),
                                'i2' => array('label' => __('Entreprise*', 'vp'), 'type' => 'text'),
                                'i3' => array('label' => __('Téléphone*', 'vp'), 'type' => 'telephone'),
                                'i4' => array('label' => __('Courriel*', 'vp'), 'type' => 'email'),
                                'i5' => array('label' => __('Meilleur moment pour vous joindre', 'vp'), 'type' => 'text'),
                            );
                            foreach ($inputs as $k => $input) {
                                ?>
                                <div class="form-group">
                                    <label for="input_<?php echo $k; ?>"><?php echo $input['label']; ?></label>
                                    <input type="<?php echo $input['type']; ?>" class="form-control"
                                           name="<?php echo $k; ?>" id="input_<?php echo $k; ?>">
                                </div>
                            <?php } ?>
                            <input type="submit" class="btn btn-primary" value="<?php _e('envoyer', 'vp'); ?>">
                        </form>
                        */
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
#FAQ
minimal_get_template_part('/templates/collapse.php', array(
    'id' => 'faq',
    'title' => 'FAQ',
    'acfrows' => 'affaire_faq'
));
?>

<?php get_footer(); ?>
