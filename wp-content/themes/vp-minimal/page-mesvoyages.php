<?php /** Template Name: mesvoyages */
get_header();
/*
/// GET FAVORITED VOYAGES
$query_result = array();
$wp_query_array = array(
    'post_type' => 'voyages',
    'post_status' => 'publish',
    'showposts' => 9999,
);
$wp_query = new WP_Query($wp_query_array);
if ($wp_query->have_posts()) {
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();
        $post_id = get_the_ID();
        $query_result[$i]['title'] = get_the_title();
        $query_result[$i]['description'] = get_the_excerpt($id);
        $query_result[$i]['url'] = get_permalink();
        $query_result[$i]['image'] = array(
            'url' => output_thumbnails($post_id, 'fiche'),
            'alt' => get_the_title(),
            'classes' => 'img-fluid'
        );
        $query_result[$i]['label'] = extractterm($post_id, 'pays');
        $i++;
    endwhile;
}
wp_reset_query();
*/

minimal_get_template_part('/templates/heading.php', array(
    'nav' => true,
    'caption' => __("My travels", "vp"),
    'subcaption' => __('Wish list', 'vp')
));

if (function_exists('get_user_favorites')): //if plugin "favorites" is active

    $favorites = get_user_favorites();

    $posts_voyages = array();
    $posts_articles = array();
    $posts_info = array();

    if($favorites) {
        //print_r($favorites);
        $posts_voyages = get_posts(array(
            'post_type' => 'voyages',
            'post__in' => $favorites
        ));
        $posts_articles = get_posts(array(
            'post_type' => 'post',
            'category__not_in' => 'infos',
            'post__in' => $favorites
        ));
        $posts_info = get_posts(array(
            'post_type' => 'post',
            'category_name' => 'infos',
            'post__in' => $favorites
        ));
    }

    $sections = array(
        'souhaits' => array(
            'title' => ((ICL_LANGUAGE_CODE == 'fr') ? 'Ma liste de souhaits Voyages' : 'My travel wish list'),
            'listid' => 'souhaitsvoyages',
            'template' => 'listing',
            'posts' => $posts_voyages,
            'config' => array(
                'classes' => 'col-md-6',
            )
        ),
        'articles' => array(
            'title' => __('My articles', 'vp'),
            'listid' => 'listearticles',
            'template' => 'article',
            'posts' => $posts_articles,
            'config' => array(
                'classes' => 'col-md-6',
                'format' => 'col-md-6',
            )
        ),
        'infoutiles' => array(
            'title' => __('My useful infos', 'vp'),
            'listid' => 'listeinfosutiles',
            'template' => 'article',
            'posts' => $posts_info,
            'config' => array(
                'classes' => 'col-md-6',
                'format' => 'col-md-6',
            )
        )
    );
    foreach ($sections as $id => $section): if (!empty($section['posts'])): ?>
        <section id="<?php echo $id; ?>" class="reveal spaced-top">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h4 class="fiche"><?php echo $section['title']; ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?php
                        minimal_get_template_part('/templates/carousel.php', array(
                            'id' => $section['listid'],
                            'nodiv' => true,
                            'rowclasses' => 'card-deck',
                            'classes' => 'card col-md-6',
                            'chunksize' => 2,
                            'template' => '/templates/' . $section['template'] . '.php',
                            'items' => format_for_template($section['posts'], $section['template'] . '.php', $section['config'])
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    endif; endforeach;
endif;
?>
<?php minimal_get_template_part('/templates/certificat.php', array('classes' => 'spaced-top')); ?>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>
