<?php /** Template Name: manuvie */
get_header();

$imgfilename = (ICL_LANGUAGE_CODE == 'fr') ? 'manuvie.png' : 'manulife.png';
minimal_get_template_part('/templates/heading.php', array(
    'caption' => (ICL_LANGUAGE_CODE == 'fr') ? 'Assurances' : 'Insurance',
    'captionimg' => get_stylesheet_directory_uri() . '/assets/img/' . $imgfilename
));
?>
<section class="spaced-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-7 text-center">
                <h3 class="mb-5 py-0 mx-auto"><?php the_field('manuvie-blurb_blurb'); ?></h3>
            </div>
        </div>
    </div>
</section>
<section class="reveal">
    <?php
    #Image + Text
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('manuvie-bloc_image')),
            'alt' => get_field('manuvie-bloc_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('manuvie-bloc_title'),
            'classes' => 'text-left',
            'text' => get_field('manuvie-bloc_text'),
        ),
        'overlap' => true,
        'layout' => 'text-image'
    ));
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto text-center">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="<?php the_field('manuvie-video_blurb'); ?>"
                            allowfullscreen></iframe>
                </div>
                <a href="<?php the_field('manuvie-button_button_link'); ?>" target="_blank"
                   class="btn btn-secondary  my-5"><?php the_field('manuvie-button_button_label'); ?></a>
            </div>
        </div>
    </div>
</section>

<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>
