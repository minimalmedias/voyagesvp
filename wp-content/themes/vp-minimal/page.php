<?php
get_header();
minimal_get_template_part('/templates/heading.php');
?>
<section id="basic" class="spaced-top spaced-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-1">&nbsp;</div>
            <div class="col-lg-8">
                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                    <?php the_content() ?>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>

