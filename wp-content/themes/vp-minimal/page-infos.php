<?php
/** Template Name: infos */
get_header();
minimal_get_template_part('/templates/heading.php');
?>
<section class="searchform">
    <form class="form form-inline justify-content-center">
        <div class="form-group">
            <input type="text" name="s" class="form-control" placeholder=" <?php echo (ICL_LANGUAGE_CODE=='fr')?'rechercher':'search'; ?> ">
            <button class="btn" type="submit"></button>
        </div>
    </form>
</section>
<section class="spaced-top">
    <div class="container">
        <div class="row" id="articleloader">
            <div style="width:100%; text-align: center">
                <div class="loader lds-css ng-scope" style="margin:auto; width:100px;">
                    <div style="width:100%;height:100%" class="lds-dual-ring">
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php minimal_get_template_part('/templates/newsletter.php', array('classes' => 'spaced-top')); ?>

<?php get_footer(); ?>
