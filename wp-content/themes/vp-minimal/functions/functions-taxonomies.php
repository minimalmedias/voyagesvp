<?php
add_action('init', 'create_type_hierarchical_taxonomy', 0);
add_action('init', 'create_pays_hierarchical_taxonomy', 0);
add_action('init', 'create_theme_hierarchical_taxonomy', 0);
add_action('init', 'create_compagnie_hierarchical_taxonomy', 0);
add_action('init', 'my_custom_post_blog');

function setCategories($fichetype,$lang){
    $type = array(
        'page-exception.php' => array(
            'fr' => 'voyages-exception',
            'en' => 'extraordinary-travel'
        ),
        'page-evasion.php' => array(
            'fr' => 'vacances-evasion',
            'en' => 'leisure-travel'
        )
    );
    return $type[$fichetype][$lang];
}

function create_type_hierarchical_taxonomy()
{

    $labels = array(

        'name' => _x('Types de voyage', 'taxonomy general name'),
        'singular_name' => _x('Type de voyage', 'taxonomy singular name'),
        'search_items' => __('Chercher parmis les type de voyage'),
        'all_items' => __('Tous les types de voyage'),
        'parent_item' => __('Type parent'),
        'parent_item_colon' => __('Type parent:'),
        'edit_item' => __('Modifier type'),
        'update_item' => __('Mettre à jour'),
        'add_new_item' => __('Ajouter'),
        'new_item_name' => __('Nouveau type de voyage'),
        'menu_name' => __('Type de voyage'),

    );

    register_taxonomy('type', array(), array(

        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'type'),

    ));
}
function create_pays_hierarchical_taxonomy()
{

    $labels = array(

        'name' => _x('Pays', 'taxonomy general name'),
        'singular_name' => _x('Pays', 'taxonomy singular name'),
        'search_items' => __('Chercher parmis les pays'),
        'all_items' => __('Tous les pays'),
        'parent_item' => __('Pays parent'),
        'parent_item_colon' => __('Pays parent:'),
        'edit_item' => __('Modifier pays'),
        'update_item' => __('Mettre à jour'),
        'add_new_item' => __('Ajouter'),
        'new_item_name' => __('Nouveau pays'),
        'menu_name' => __('Pays'),

    );

    register_taxonomy('pays', array(), array(

        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'pays'),

    ));
}
function create_theme_hierarchical_taxonomy()
{

    $labels = array(

        'name' => _x('Thèmes', 'taxonomy general name'),
        'singular_name' => _x('Thème', 'taxonomy singular name'),
        'search_items' => __('Chercher parmis les thèmes'),
        'all_items' => __('Tous les thèmes'),
        'parent_item' => __('Thème parent'),
        'parent_item_colon' => __('Thèmes parent:'),
        'edit_item' => __('Modifier thème'),
        'update_item' => __('Mettre à jour'),
        'add_new_item' => __('Ajouter'),
        'new_item_name' => __('Nouveau thème'),
        'menu_name' => __('Thèmes'),

    );

    register_taxonomy('theme', array(), array(

        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'theme'),

    ));
}
function create_compagnie_hierarchical_taxonomy()
{

    $labels = array(

        'name' => _x('Compagnies', 'taxonomy general name'),
        'singular_name' => _x('Compagnie', 'taxonomy singular name'),
        'search_items' => __('Chercher parmis les compagnies'),
        'all_items' => __('Toutes les compagnies'),
        'parent_item' => __('Compagnie parent'),
        'parent_item_colon' => __('Compagnies parent:'),
        'edit_item' => __('Modifier compagnie'),
        'update_item' => __('Mettre à jour'),
        'add_new_item' => __('Ajouter'),
        'new_item_name' => __('Nouvelle compagnie'),
        'menu_name' => __('Compagnies'),

    );

    register_taxonomy('compagnie', array(), array(

        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'compagnie'),

    ));
}
function my_custom_post_blog()
{
    $labels = array(
        'name' => _x('Voyages', 'post type general name', 'source'),
        'singular_name' => _x('Voyage', 'post type singular name', 'source'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter un voyage'),
        'edit_item' => __('Modifier cette fiche de voyage'),
        'new_item' => __('Nouveau'),
        'all_items' => __('Tous les voyages'),
        'view_item' => __('Afficher la fiche de voyage'),
        'search_items' => __('Chercher parmis les voyages'),
        'not_found' => __('Aucun voyage trouvée'),
        'not_found_in_trash' => __('Aucun voyage trouvée dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => 'Voyages'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Voyages',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'taxonomies' => array('pays', 'theme', 'type', 'compagnie'),
        'has_archive' => true,
        'menu_icon' => 'dashicons-palmtree',
    );
    register_post_type('voyages', $args);
}

