<?php
add_action( 'init', function(){
    return add_rewrite_rule(
        'les-temoignages',
        'index.php?pagename=idees-destination-voyage&filter=temoignages',
        'sitemap_index\.xml$'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        'testimonials/',
        'index.php?pagename=ourinspirations&filter=temoignages',
        'sitemap_index\.xml$'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        'testimonials',
        'index.php?pagename=ourinspirations&filter=temoignages',
        'sitemap_index\.xml$'
    );
});
add_filter('query_vars', function($v){ return array_merge(array('filter'),$v); });
