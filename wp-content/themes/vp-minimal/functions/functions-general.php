<?php

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_filter('pre_get_posts', 'searchfilter');
add_filter('body_class', function ($classes) {
    return array_merge($classes, array('logospin'));
});
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function custom_excerpt_length($length)
{
    return 20;
}

function quicktran($en,$fr){
    if(ICL_LANGUAGE_CODE=='fr'){ return $fr; } else { return $en; }
}

function extractterm($id, $type)
{
    $term = get_the_terms($id, $type);
    return $term[0]->name;
}

function wp_get_attachment_image_url_with_fallback($id, $size)
{
    $imagesizes = get_image_sizes();
    $outputimage = wp_get_attachment_image_url($id, $size);
    if ($outputimage == '') {
        $outputimage = get_stylesheet_directory_uri() . '/assets/img/' . $imagesizes[$size]['width'] . 'x' . $imagesizes[$size]['height'] . '.gif';
    }
    return $outputimage;
}

function standardimage($image)
{
    return array(
        '(max-width:400px)' => $image['sizes']['standardphotothumbnail'],
        '(min-width:401px)' => $image['sizes']['standardphoto']
    );
}

function output_thumbnails($id, $type)
{
    $thumbnailformat = array(
        'fiche' => array('fiche', 800, 386),
        'article' => array('standardphoto', 800, 540)
    );
    $mobileformat = array(
        'fiche' => array('fichethumbnail', 400, 193),
        'article' => array('standardphotothumbnail', 400, 270)
    );
    $thumbnail = get_the_post_thumbnail_url($id, $thumbnailformat[$type][0]);
    $mobilethumbnail = get_the_post_thumbnail_url($id, $mobileformat[$type][0]);

    if ($thumbnail == '') {
        $thumbnail = get_stylesheet_directory_uri() . '/assets/img/800x' . $thumbnailformat[$type][2] . '.gif';
        $mobilethumbnail = get_stylesheet_directory_uri() . '/assets/img/400x' . $mobileformat[$type][2] . '.gif';
    }
    return array(
        '(max-width: 400px)' => $mobilethumbnail,
        '(min-width:401px)' => $thumbnail
    );
}

function filter_terms_lang($terms)
{
    //hack to seperate fr and en terms
    $lang = get_locale();
    $output['fr'] = array();
    $output['en'] = array();
    foreach ($terms as $term) {
        if (substr($term->slug, -3) == '-en') {
            if ($term->count > 0) { // only if posts exist for this term
                $output['en'][] = $term;
            }
        } else {
            if ($term->count > 0) { // only if posts exist for this term
                $output['fr'][] = $term;
            }
        }
    }
    return $output[ICL_LANGUAGE_CODE];
}

function get_terms_lang($taxonomy)
{
    $terms = get_terms(array(
        'taxonomy' => $taxonomy,
        'hide_empty' => true,
    ));
    return filter_terms_lang($terms);
}

function get_random_post($type){
    $return = array();
    $wp_query = new WP_Query(array(
        'post_type' => 'voyages', 'post_status' => 'publish',  'orderby' => 'rand',
        'posts_per_page' => '1',
        'tax_query' =>  array(
            'post_status' => 'publish',
            'taxonomy' => 'type',
            'field' => 'slug',
            'terms' => $type,
        ),
    ));
    if ($wp_query->have_posts()) { $wp_query->the_post();
        $return['ID'] = get_the_ID();
        $return['post_title'] = get_the_title();// get_title();
    }
    wp_reset_query();
    return $return;
}

function filtresections($section,$lang)
{
    $taxonomies = array('theme'=>array(), 'pays'=>array());
    foreach ($taxonomies as $tax => $array) {
        $terms = get_terms($tax);
        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $term) {
                $args = array(
                    //'posts_per_page' => -1,
                    'post_type' => 'voyages',
                    'post_status' => 'publish',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'type',
                            'field' => 'slug',
                            'terms' => setCategories('page-'.$section.'.php',$lang)
                        ),
                        array(
                            'taxonomy' => $tax,
                            'field' => 'slug',
                            'terms' => $term->slug
                        )
                    )
                );

                $posts_array = get_posts($args);
                $numposts = count($posts_array);
                if($numposts>0){
                    $taxonomies[$tax][$term->slug] =  $term->name.' <span class="badge badge-pill badge-light">'.$numposts.'</span>';
                }
            }
        }
    }
    $filters = array(
        array('label' => 'Destinations', 'type' => 'pays', 'filtres' => $taxonomies['pays']),
        array('label' => ((ICL_LANGUAGE_CODE=='fr')?'Thématiques':'Themes'), 'type' => 'theme', 'filtres' => $taxonomies['theme'])
    );
    return $filters;
}

function format_for_template($posts, $template, $config)
{
    $output = array();
    $i = 0;
    foreach ($posts as $p) {
        if ($template == 'listing.php') {
            $output[$i] = array(
                'label' => extractterm($p->ID, 'pays'),
                'title' => $p->post_title,
                'description' => $p->post_excerpt,
                'image' => array(
                    'url' => output_thumbnails($p->ID, 'fiche'),
                    'alt' => get_the_title(),
                    'classes' => 'img-fluid'
                ),
                'url' => get_post_permalink($p->ID)
            );
            if (!empty($config)) {
                foreach ($config as $key => $value) {
                    $output[$i][$key] = $value;
                }
            }
            $i++;
        }
        if ($template == 'article.php') {
            $output[$i] = array(
                'image' => array(
                    'url' => output_thumbnails($p->ID, 'article'),
                    'alt' => get_the_title(),
                    'classes' => 'img-fluid'
                ),
                'date' => get_the_date(),
                'url' => get_post_permalink($p->ID),
                'title' => $p->post_title,
                'description' => $p->post_excerpt,
                'social' => array()
            );
            if (!empty($config)) {
                foreach ($config as $key => $value) {
                    $output[$i][$key] = $value;
                }
            }
            $i++;
        }
    }
    return $output;
}

function acf_hack($extract)
{
    //hack to extract acf key or value from database because in some cases get_field isnt working for some reason
    if (preg_match_all('/"([^"]+)"/', $extract, $extractelement)) {
        return $extractelement[1];
    }

}

function minimal_language_switcher()
{
    $languages = apply_filters('wpml_active_languages', NULL);


    if (!empty($languages)) {
        $return = '<ul>';
        foreach ($languages as $l) {
            //if (!$l['active'])
            $return .= '<li><a href="' . $l['url'] . '">' . $l['language_code'] . '</a></li>';
            //}
        }
        return $return . '</ul>';
    }
}

function minimal_language_switcher2()
{
    $languages = apply_filters('wpml_active_languages', NULL);
    if (!empty($languages)) {
        foreach ($languages as $l) {
            if (!$l['active']){
            return '<a href="' . $l['url'] . '">' . $l['language_code'] . '</a>';
            }
        }
    }
}

function searchfilter($query)
{
    if (basename(get_page_template()) == 'page-infos.php') {
        if ($query->is_search && !is_admin()) {
            $query->set('post_type', array('post', 'page'));
        }
    }
    return $query;
}


