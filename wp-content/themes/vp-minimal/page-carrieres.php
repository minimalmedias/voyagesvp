<?php /** Template Name: carrieres */
get_header();
minimal_get_template_part('/templates/heading.php', array(
    'nav' => true,
    'caption' => get_field('carriere-header_blurb'),
    'subcaption' =>  __('Careers', 'vp')
));

?>
<?php minimal_get_template_part('/templates/description.php', array('description' => get_field('carriere-blurb_blurb'))); ?>
<section class="reveal">
    <?php
    #Image + Text
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => array(
            'url' => standardimage(get_field('carriere-bloc_image')),
            'alt' => get_field('carriere-bloc_label'),
            'classes' => 'img-fluid my-md-5'
        ),
        'text' => array(
            'subtitle' => get_field('carriere-bloc_title'),
            'classes' => 'text-center',
            'text' => get_field('carriere-bloc_text'),
            'button' => array(
                'label' => get_field('carriere-bloc_button_label'),
                'url' => get_field('carriere-bloc_button_link'),
                //'classes' => ''
            )
        ),
        'layout' => 'image-text'
    ));
    ?>
</section>
<?php
#OFFRES
minimal_get_template_part('/templates/collapse.php', array(
    'id' => 'nosoffres',
    'title' => __('Our openings', 'vp'),
    'acfrows' => 'carriere-offres'
));
?>

<?php minimal_get_template_part('/templates/newsletter.php', array('classes' => 'spaced-top')); ?>

<?php get_footer(); ?>
