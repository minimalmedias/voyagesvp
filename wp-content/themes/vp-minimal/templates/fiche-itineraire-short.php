<section class="spaced-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="mobile-left"><?php _e('Itinerary', 'vp'); ?></h2>
                <p><?php _e('Cette villa de luxe, aux allures tantôt africaines, tantôt coloniales, a tout
                    pour plaire au couple,
                    comme aux familles. Le bois et la pierre sont omniprésents et sont en
                    harmonie avec
                    l’environnement.', 'vp'); ?></p>
                <p><?php _e('Elle est située sur la côte ouest du Costa-Rica, à Las Catalinas, très joli
                    bourg pavé de rues
                    piétonnes, des jardins et des places.', 'vp'); ?></p>
                <p><?php _e('Sa plage, Playa Danta, est baignée d’une eau bleue et tranquille, ce qui en
                    fait un endroit parfait
                    pour les familles avec des enfants.', 'vp'); ?></p>
                <p><?php _e('Vous serez à dix minutes à pied des commerces et des restaurants.', 'vp'); ?></p>
                <p><?php _e('De une à quatre chambres, elles sont séparés par des cours intérieures et des
                    fontaines. Les cuisines
                    sont toutes équipées pour offrir les commodités aux résidents.', 'vp'); ?></p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <?php
                echo responsiveimage(array(
                    'url' => array(
                        '(max-width: 300px)' => 'http://via.placeholder.com/300x200',
                        '(min-width:768px) and (max-width:991px)' => 'http://via.placeholder.com/600x800'
                    ),
                    'alt' => "Photo",
                    'classes' => 'img-fluid'
                ));
                ?>
            </div>
        </div>
    </div>
</section>