<?php if (have_rows($this->acfrows)):  ?>
    <section id="<?php echo $this->id; ?>" <?php echo(isset($this->classes))?'class="'.$this->classes.'"':''; ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="h1 subheading"><?php echo $this->title; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 collapsemenu" id="<?php echo $this->id; ?>-accordion" role="tablist">
                    <?php
                    $i = 0;
                    while (have_rows($this->acfrows)) : the_row();
                        ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="heading<?php echo $i; ?>">
                                <h5 class="mb-0">
                                    <a
                                        <?php echo ($i != 0) ? 'class="collapsed"' : ''; ?>
                                        data-toggle="collapse"
                                        href="#collapse<?php echo $i; ?>"
                                        aria-expanded="true"
                                        aria-controls="collapse<?php echo $i; ?>">
                                        <span class="title"><?php the_sub_field( $this->acfrows.'_label'); ?></span>
                                        <i class="fa fa-chevron-right pull-right" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse<?php echo $i; ?>" class="collapse  <?php echo ($i == 0) ? 'show' : ''; ?>"
                                 role="tabpanel" aria-labelledby="heading<?php echo $i; ?>"
                                 data-parent="#<?php echo $this->id; ?>-accordion">
                                <div class="card-body">
                                    <?php the_sub_field( $this->acfrows.'_description') ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>