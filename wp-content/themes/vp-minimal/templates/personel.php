<?php echo responsiveimage($this->photo); ?>
<div class="card-body">
    <h5 class="card-title"><?php echo $this->name; ?></h5>
    <p class="small text-uppercase"><?php echo (ICL_LANGUAGE_CODE=='fr')?$this->titre:$this->titre_english; ?></p>
</div>
<div class="card-footer">
    <p class="social">
        <?php foreach ($this->social as $key => $social): ?>
            <a href="<?php echo (($key=='telephone')?'tel:+':'').$social; ?>" target="_blank" class="i_<?php echo $key; ?>"></a>
        <?php endforeach; ?>
    </p>
    <p class="desktoptelephone"><?php echo $this->social['telephone']; ?></p>
</div>

