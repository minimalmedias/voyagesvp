<section id="lesavantages" class="reveal <?php echo (isset($this->classes)) ? $this->classes : ''; ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h5>Groupe Voyages VP</h5>
                <h2><?php echo (ICL_LANGUAGE_CODE == 'fr') ? 'Les avantages' : 'The advantages'; ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <div class="icon-champagne"></div>
                <h3 class="h5"><?php echo (ICL_LANGUAGE_CODE == 'fr') ? 'Un accompagnement premium' : 'Premium assistance'; ?></h3>
                <p class="px-md-5">
                    <?php if (ICL_LANGUAGE_CODE == 'fr'): ?>
                        De notre première conversation jusqu’au retour, nous prenons soin de gérer les détails de votre voyage pour qu’ils répondent, avec attention et anticipation, à chacun de vos besoins et de vos désirs.
                    <?php else: ?>
                        From our first point of contact to your return home, we’ll manage all the logistics of your trip so that they meet, with attention and anticipation, each of your needs and desires.
                    <?php endif; ?>
                </p>
            </div>
            <div class="col-md-6 text-center">
                <div class="icon-gate"></div>
                <h3 class="h5"><?php echo (ICL_LANGUAGE_CODE == 'fr') ? 'Exclusivité et primeur' : 'Exclusivity & Privileges'; ?></h3>
                <p class="px-md-5">
                    <?php if (ICL_LANGUAGE_CODE == 'fr'): ?>
                        Partir avec nous, c’est avoir accès à une foule d’avantages. Vols, hôtels, croisières… Partir n’aura jamais été aussi exclusif. Bénéficiez d’offres exceptionnelles et de propositions uniques grâce à notre vaste réseau qui nous permet de bénéficier de tarifs avantageux.
                    <?php else: ?>
                        Arranging your travels with us brings you many benefits on flights, hotels, cruises, etc. that make planning a trip easy! Take advantage of exceptional and unique offers thanks to our vast partner network.
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
</section>