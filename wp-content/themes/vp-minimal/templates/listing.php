<div class="card fiche offre clickbox <?php echo (isset($this->classes))?$this->classes:''; echo (isset($this->delay))?' fadeInSequence':''; ?>"
        <?php echo(isset($this->delay))?' '.$this->delay:'';
        echo(isset($this->rowid))?' id="'.$this->rowid.'"':''; ?>>
        <?php echo responsiveimage($this->image);?>
        <div class="card-body text-center">
            <?php if(isset($this->label)){ ?>
            <div class="labelcontainer fiche"><h5 class="verticallabel"><?php echo $this->label; ?></h5></div>
            <?php } ?>
            <div class="cardicon i_evasion"></div>
            <h4 class="card-title fiche"><a href="<?php echo $this->url; ?>" class="clickurl"><?php echo $this->title; ?></a></h4>
            <p class="card-text"><?php echo $this->description; ?></p>
        </div>
        <div class="card-footer text-center">
            <a href="<?php echo $this->url; ?>" class="btn btn-link"><?php echo (($this->lang=='fr')?'Voir cette offre':'See this offer'); ?></a>
        </div>
</div>