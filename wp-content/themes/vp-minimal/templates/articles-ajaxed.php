<?php
require_once("../../../../wp-load.php");

$items_per_load = 2;
$wp_query_array = array('post_status' => 'publish', 'posts_per_page'=>-1);
$showposts = 9999;

if (isset($_POST['query'])) {
    $postquery = $_POST['query'];
}

if (is_array($_POST['type'])) {
    $posttype = $_POST['type'][0];
    $postquery = $_POST['type'][1];
} else {
    $posttype = $_POST['type'];
}

if ($posttype == 'home') {
    $wp_query_array['showposts'] = 3;
} elseif ($posttype == 'inspirations') {
    $wp_query_array['category__not_in'] = 'infos';
} elseif ($posttype == 'infos') {
    $wp_query_array['category_name'] = 'infos';
} else {
    $wp_query_array['category_name'] = $posttype;
    $wp_query_array['category__not_in'] = 'infos';
}

if (isset($postquery)) {
    if ($postquery != '') {
        $wp_query_array['s'] = $postquery;
    }
}

$wp_query_array['suppress_filters'] = 1; //set to 0 to only display post for language
$wp_query = new WP_Query($wp_query_array);

if ($posttype != 'infos') { // TEMPORARY SWITCH TO DISPLAY FRENCH POSTS ON ENGLISH PAGE
    if (isset($_POST['wpml_lang'])) {
        do_action('wpml_switch_language', $_POST['wpml_lang']); // switch the content language
    }
}

$query_result = array();
if ($wp_query->have_posts()) {
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();
        //$id = icl_object_id(get_the_ID(), 'post', false, $_POST['wpml_lang']);
        $id = get_the_ID();
        $imageid = get_post_thumbnail_id();
        $query_result[$i]['image'] = array(
            'url' => output_thumbnails($id, 'article'),
            'alt' => get_the_title(),
            'classes' => 'img-fluid'
        );
        $format['fr']='j F, Y';
        $format['en']='F j, Y';
        $query_result[$i]['imageid'] = $imageid;
        $query_result[$i]['title'] = get_the_title();
        $query_result[$i]['url'] = get_permalink();
        $query_result[$i]['date'] = translatedate(get_the_date($format[$_POST['wpml_lang']]),$_POST['wpml_lang']);
        /*
        $query_result[$i]['social'] = array(
            'pinterest' => 'http://pinterest.com/pin/create/button/?url='.get_permalink().'&media='.$media,
            'twitter' => 'https://twitter.com/home?status='.get_permalink(),
            'facebook' => 'http://www.facebook.com/sharer.php?u='.get_permalink().'&amp;t='.get_the_title()
        );
        */
        if (!is_front_page()) {
            $query_result[$i]['description'] = get_the_excerpt($id);
        }
        $i++;
    endwhile;
}
/////////////////////////////////


//echo '<pre style="display:block;">';
//print_r($wp_query_array);
//echo count($query_result);
//echo '</pre>';

$first_seven_items = array_chop($query_result, 7);
$chunks = array_chunk($query_result, $items_per_load);
array_unshift($chunks, $first_seven_items); // insert back first 7 items as first chunk

$totalpages = count($chunks);
$nextpage = false;
$firstload = $_POST['firstload'];
if ($_POST['page'] < $totalpages) {
    $nextpage = $_POST['page'] + 1;
}

$cumulatedchunks = array();
foreach ($chunks as $index => $chunk) {
    if ($index < $_POST['page']) { //0 < 0
        $cumulatedchunks[] = $chunk;
    }
}

$i = 0;
foreach ($cumulatedchunks as $index => $articles) {
    $ii = 0;
    foreach ($articles as $config) {
        $config['format'] = '';
        if ($posttype == 'home') {
            $configformat = 'col-md-4';
        } else {
            $configformat = 'col-md-6';
            if ($i == 0) { //if first load
                if ($ii == 0) { // first page, first image special formatting
                    $configformat = 'col-md-6 col-lg-8';
                    $config['format'] = 'large';
                }
                if ($ii == 1) {
                    $configformat = 'col-md-6 col-lg-4';
                }
                if ($ii == 2 || $ii == 3 || $ii == 4) {
                    $configformat = 'col-md-4';
                }
            }
        }
        if ($i == $_POST['page']) { // Fadein animation only for newest page loaded
            $config['delay'] = 'style="animation-delay: ' . ($i / 2) . 's;"';
        }
        if ($ii == 0) {
            $config['rowid'] = 'paged_' . ($index + 1);
        }

        echo '<div class="' . $configformat . '">';
        minimal_get_template_part('/templates/article.php', $config);
        echo '</div>';
        $ii++;
    }
    $i++;
}

if (isset($nextpage)) {
    if ($nextpage != false) {
        $ajaxbutton['nextpage'] = $nextpage;
    }
}
if (isset($items_per_load)) {
    $ajaxbutton['items_per_load'] = $items_per_load;
}
if ($posttype != 'home') {
    $ajaxbutton['data']['type'] = $posttype;
    if (isset($postquery)) {
        if ($postquery != '') {
            $ajaxbutton['data']['query'] = $postquery;
        }
    }
    minimal_get_template_part('/templates/ajaxbutton.php', $ajaxbutton);
}