<section id="certificat" class="reveal <?php echo(isset($this->classes))?$this->classes:''; ?>">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-7 col-lg-5 text-center certificatcard">
                <div class="shadowcard navbar-brand mx-auto"><span class="sr-only"><?php _e('Voyages VP', 'vp'); ?></span></div>
            </div>
            <div class="col-md-4 col-lg-7 certificattext">
                <div>
                    <p><?php _e('The VOYAGES gift certificate is the perfect solution to please your loved ones throughout the year and for all your last-minute gifts.', 'vp'); ?></p>
                    <a href="<?php echo get_template_link('page-chequecadeau.php'); ?>" class="btn btn-primary"><?php _e('Details', 'vp'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>