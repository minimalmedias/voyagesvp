<?php $id = get_the_ID(); ?>
<section id="grillevoyages" class="reveal spaced-top">
    <input type="hidden" id="postid" value="<?php echo $id; ?>">
    <div class="container">
        <div class="row">
            <?php foreach ($this->filtresections as $filtresection): ?>
                <?php if(!empty($filtresection['filtres'])): ?>
                <div class="col-md-6">
                    <h4 class="fiche label_<?php echo $filtresection['type']; ?>"><?php echo $filtresection['label']; ?></h4>
                    <nav class="filters nav nav-pills nav-fill">
                        <?php foreach ($filtresection['filtres'] as $key => $filtre): ?>
                            <a class="nav-item nav-link hvr-rectangle-out <?php //active //disabled ?>"
                               data-type="<?php echo $filtresection['type'] ?>"
                               href="#<?php echo $key; ?>"><?php echo $filtre; ?></a>
                        <?php endforeach; ?>
                    </nav>
                </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div id="ficheloader"></div>
    </div>
</section>