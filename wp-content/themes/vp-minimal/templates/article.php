<div class="card article clickbox fadeInSequence" <?php echo (isset($this->delay)) ? $this->delay : ''; ?> <?php echo (isset($this->rowid)) ? ' id="' . $this->rowid . '"' : ''; ?>>
    <?php
    if ($this->format == 'large') {
        $image = array(
            'url' => array(
                'max-width:991px' => wp_get_attachment_image_url_with_fallback($this->imageid, 'standardphoto'),
                'min-width:992px' => wp_get_attachment_image_url_with_fallback($this->imageid, 'standardphotodouble')
            ),
            'alt' => '',
            'classes' => 'full-width'
        );
    } else {
        $image = $this->image;
    }
    echo responsiveimage($image)
    ?>
    <div class="card-body text-center">
        <h5><?php echo $this->date; ?></h5>
        <h4 class="card-title"><a class="clickurl" href="<?php echo $this->url; ?>"><?php echo $this->title; ?></a></h4>
        <?php echo (isset($this->description)) ? '<p>' . $this->description . '</p>' : ''; ?>
    </div>
    <div class="card-footer">
        <p class="social">
            <?php if(isset($this->social)): foreach ($this->social as $key => $social): ?>
                <a href="<?php echo $social; ?>" class="i_<?php echo $key; ?>"></a>
            <?php endforeach; endif; ?>
        </p>
    </div>
</div>