<?php
if (isset($this->nextpage) and isset($this->items_per_load)) {
    $delay = ($this->items_per_load / 4);
    $data = '';
    if(isset($this->data)){
        foreach($this->data as $k=>$d){
            $data .= ' data-'.$k.'="'.$d.'" ';
        }
    }
    //$data = (isset($this->datatype) ? 'data-type="' . $this->datatype . '"' : '');
    echo '<div class="text-center col-md-12">';
    echo '<a href="#paged_' . $this->nextpage . '" ' . $data . ' class="pager" style="animation-delay: ' . $delay . 's;">';
    echo '<div class="loader lds-css ng-scope"><div style="width:100%;height:100%" class="lds-dual-ring"><div></div></div></div>';
    echo '<span class="plus">+</span>';
    echo '</a></div>';
}