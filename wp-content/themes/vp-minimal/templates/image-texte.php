<?php
$verticallabel = (isset($this->verticallabel)) ? (($this->verticallabel != '') ? '<div class="labelcontainer"><h5 class="verticallabel">' . $this->verticallabel . '</h5></div>' : '') : '';
$texttitle = (isset($this->text['title'])) ? (($this->text['title'] != '') ? '<h3 class="h2">' . $this->text['title'] . '</h3>' : '') : '';
$subtitle = (isset($this->text['subtitle'])) ? (($this->text['subtitle'] != '') ? '<h4 class="h3">' . $this->text['subtitle'] . '</h4>' : '') : '';
$paragraph = '';

//hack thropugh object, optimize later. used to display image block only if image exists
$thisimage = '';
if (isset($this->image)) {
    $it = 0;

    foreach ($this->image['url'] as $u) {
        if ($it == 0) {
            if ($u) {
                $thisimage = $this->image;
            }
        }
        $it++;
    }
}
if (isset($this->text['paragraph'])) {
    foreach ($this->text['paragraph'] as $p) {
        $paragraph .= '<p>' . $p . '</p>';
    }
}
if (isset($this->text['text'])) {
    if ($this->text['text'] != '') {
        $paragraph = '<p>' . $this->text['text'] . '</p>';
    }
}
$button = '';
if (isset($this->text['button']) and $this->text['button']['label'] != '') {
    $buttonclasses = (isset($this->text['button']['classes'])) ? $this->text['button']['classes'] : 'btn-primary';
    $button .= '<a href="' . $this->text['button']['url'] . '" class="btn ' . $buttonclasses . '">';
    $button .= $this->text['button']['label'];
    $button .= '</a>';
}
$textcellcontent = $verticallabel . $texttitle . $subtitle . $paragraph . $button;
$containerclasses = 'layout-' . $this->layout;
$imagecellclasses = 'col-md-6 col-lg-7 col-xl-6 text-center ';
$textcellclasses = 'col-md-6 col-lg-5 p-4 py-md-5';
$subdivclasses = (isset($this->text['classes'])) ? $this->text['classes'] : '';

if ($this->layout == 'image-text') {
    // IMAGE + TEXT
    $rowclasses = 'row';
    $imagecellclasses = $imagecellclasses;
    $subdivclasses = $subdivclasses . ' mr-lg-5  ml-md-0 pr-lg-5';
} else {
    // TEXT + IMAGE
    $rowclasses = 'row flex-column-reverse flex-md-row ';
    $imagecellclasses = $imagecellclasses . ' pl-md-0 pr-md-5';
    $subdivclasses = $subdivclasses . ' pl-md-5 pl-lg-5 pr-lg-0';
}

if (isset($this->overlap)) {
    $containerclasses = $containerclasses . ' overlap ';
    $imagecellclasses = 'col-md-6 text-center ';
    $textcellclasses = 'col-md-6';
    $subdivclasses = (isset($this->text['classes'])) ? ' ' . $this->text['classes'] . ' ' : '';
}

$fullstring = $verticallabel . $texttitle . $subtitle;

if(array_key_exists('frames',$thisimage['url'])){
//if($thisimage['url']['frames']){
    $framesclass = (isset($thisimage['url']['framesclass']))?$thisimage['url']['framesclass']:'';
    $imagearea  = '<div class="frameloop '.$framesclass.'">';
    $i=0;
    foreach($thisimage['url']['frames'] as $frame){
        $imagearea .= '<img src="' . $frame . '" class="frame'.$i.' '. $thisimage['classes'] . '" alt="' . $thisimage['alt'] . '">';
        $i++;
    }
    $imagearea .= '</div>';
} else {
    $imagearea = '<div class="zoomfx">' . responsiveimage($thisimage) . '</div>';
}

if ($fullstring != '') {
    $text = '<div class="text-cell ' . $textcellclasses . '"><div class="' . $subdivclasses . '">' . $textcellcontent . '</div></div>';
    $image = ($thisimage != '') ? '<div class="image-cell ' . $imagecellclasses . '">'.$imagearea.'</div>' : '';
    ?>
    <div class="container mb-md-4 <?php echo $containerclasses; ?> reveal">
        <div class="row align-items-center justify-content-between <?php echo $rowclasses; ?>">
            <?php echo ($this->layout == 'image-text') ? $image . $text : $text . $image; ?>
        </div>
    </div>
<?php } ?>