<div id="<?php echo $this->id; ?>" class="carousel responsive-carousel slide" data-ride="carousel" data-interval="0">
    <div class="carousel-inner">
        <?php
        $navigation = false;
        if($this->chunksize){ //if multiple items displayed per slide
        $i = 0;
        $chunks = array_chunk($this->items,$this->chunksize);
        if(count($chunks) > 1){ $navigation = true; }
        foreach ($chunks as $chunk):
            $chunkcount = count($chunk);
            $empties = ($this->chunksize - $chunkcount);
            ?>
            <div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
                <div class="container-fluid">
                    <div class="<?php echo (isset($this->rowclasses)) ? $this->rowclasses : 'row'; ?>">
                        <?php $ii = 0; foreach ($chunk as $card): ?>
                            <?php if(!isset($this->nodiv)){ ?>
                            <div class="<?php echo (isset($this->classes)) ? $this->classes : ''; ?>">
                                <?php minimal_get_template_part($this->template, $card); ?>
                            </div>
                            <?php } else {
                                minimal_get_template_part($this->template, $card);
                                }
                                ?>
                        <?php $ii++; endforeach; ?>
                        <?php for ($k = 0 ; $k < $empties; $k++): ?>
                            <div class="emptycell <?php echo (isset($this->classes)) ? $this->classes : ''; ?>"></div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
            <?php $i++; endforeach; ?>

        <?php } else { ?>

            <?php if(count($this->items) > 1){ $navigation = true; } ?>
            <?php $i = 0; foreach ($this->items as $card): ?>
                <div class="carousel-item <?php echo (isset($this->classes)) ? $this->classes : '';  echo ($i == 0) ? ' active' : ''; ?>">
                    <?php minimal_get_template_part($this->template, $card); ?>
                </div>
            <?php $i++; endforeach; ?>

        <?php } ?>
    </div>
    <?php if ($navigation): ?>
        <a class="carousel-control-prev" href="#<?php echo $this->id; ?>" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php _e('Previous', 'vp'); ?></span>
        </a>
        <a class="carousel-control-next" href="#<?php echo $this->id; ?>" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php _e('Next', 'vp'); ?></span>
        </a>
    <?php endif; ?>
</div>