<?php
if (!isset($this->slides)) {
    $images = get_post_meta(get_the_ID(), 'vdw_gallery_id', true);
    if (!$images) {
        $images = array(0);
    }
    $slides = array();
    $i = 0;
    foreach ($images as $image) {
        $img = wp_get_attachment_image_src($image, 'header');
        $slides[$i]['img'] = $img[0];
        if (!isset($this->caption)) {
            $slides[$i]['caption'] = get_the_title();
        } else {
            if ($this->caption != '') {
                $slides[$i]['caption'] = $this->caption;
            }
        }
        $slides[$i]['caption'] = (isset($this->caption)) ? $this->caption : get_the_title();

        if (isset($this->subcaption)) {
            $slides[$i]['subcaption'] = $this->subcaption;
        }
        if (isset($this->button)) {
            $slides[$i]['button'] = $this->button;
        }
        if (isset($this->captionimg)) {
            $slides[$i]['captionimg'] = $this->captionimg;
        }
        $i++;
    }
} else {
    $slides = $this->slides;
}
/*
function metapreloader($img){
    echo '<link rel="preload" href="'.$img.'" as="image">';
}
foreach($slides as $img) {
    metapreloader($img->img);
    do_action('wp_head', 'metapreloader' );
}
*/
$carousel = array(
    'id' => 'heading',
    'items' => $slides,
    'interval' => 10000,
    'template' => '/templates/headerslides.php',
);
if(isset($this->nav)){
    $carousel['nav'] = $this->nav;
}
?>
<section class="heading jumbotron jumbotron-fluid screen-height-md">
    <?php minimal_get_template_part('/components/carousel.php',$carousel); ?>
</section>