<section id="newsletter" class="<?php echo(isset($this->classes))?$this->classes:''; ?>">
    <div class="col text-center">
        <div class="icon"></div>
        <h4><?php _e('Get our latest offers', 'vp'); ?></h4>
        <?php
        $class = 'form form-inline justify-content-center';
        $lang = get_locale();
        $form['fr'] = '[contact-form-7 id="17630" title="Infolettre FR" html_class="'.$class.'"]';
        $form['en'] = '[contact-form-7 id="17631" title="Infolettre EN" html_class="'.$class.'"]';
        echo do_shortcode($form[ICL_LANGUAGE_CODE]);
        ?>
    </div>
    <!--div class="labelcontainer"><h5 class="verticallabel"><?php _e('Voyages VP Newsletter', 'vp'); ?></h5></div-->
</section>