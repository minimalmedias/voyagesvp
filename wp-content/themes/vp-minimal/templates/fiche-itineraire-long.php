<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11"><h2 class="mobile-left"><?php _e('Itinerary', 'vp'); ?></h2></div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-<?php echo($this->les_points_forts !='')?'6':'9' ?> largetext pr-5">
            <?php echo $this->itineraire; ?>
            <?php echo $this->texte_inclusions; ?>
        </div>
        <?php if($this->les_points_forts !=''): ?>
        <div class="col-md-6 col-lg-5 pointsforts">
            <h5 class="desktop verticallabel"><?php _e('Highlights', 'vp'); ?></h5>
            <div class="m-5 icon-thumbsup"></div>
            <div class="m-5">
                <?php echo $this->les_points_forts; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>