<?php /** Template Name: search */
get_header();
//minimal_get_template_part('/templates/heading.php');
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 min-screen">
                <?php get_template_part('loops/content', get_post_format()); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
