<?php /** Template Name: apropos */
get_header();

$users = get_users(array('fields' => array('ID')));
$equipearray = array(
    'direction' => array(),
    'affaires' => array(),
    'loisir' => array()
);
$i = 0;
foreach ($users as $u) {
    $data = get_user_meta($u->ID);
    if (isset($data['equipe'])) {
        $user = get_user_by('id',$u->ID);
        $avatar = wp_get_attachment_image_src($data['avatar'][0],'avatar');
        $personne = array(
            'name' => $data['first_name'][0] . ' ' . $data['last_name'][0],
            'titre' =>  $data['titre'][0],
            'titre_english' =>  (array_key_exists('titre_english',$data))?$data['titre_english'][0]:'',
            'social' => array('mail' => 'mailto:'.$user->user_email),
            'photo' => array(
                'url' => array(
                    '(min-width: 50px)' => $avatar[0],
                ),
                'alt' => $data['first_name'][0] . ' ' . $data['last_name'][0],
                'classes' => 'full-width'
            )
        );
        foreach(array('telephone','linkedin','twitter') as $detail){
            if($data[$detail][0]!=''){
                $personne['social'][$detail] = $data[$detail][0];
            }
        }



        foreach ( acf_hack($data['equipe'][0]) as $equipe) {
            if ($equipe == 'direction1') {
                $equipearray['direction'][0] = $personne;
            } elseif ($equipe == 'direction2') {
                $equipearray['direction'][1] = $personne;
            } elseif ($equipe == 'direction3') {
                $equipearray['direction'][2] = $personne;
            } elseif ($equipe == 'direction4') {
                $equipearray['direction'][3] = $personne;
            } else {
                $equipearray[$equipe][] = $personne;
            }
            $i++;
        }
    }
}
ksort($equipearray['direction']);

$architectesgroups = array(
    ((ICL_LANGUAGE_CODE=='fr')?"Les architectes de vos voyages":"Travel architects")  => $equipearray['direction'],
    ((ICL_LANGUAGE_CODE=='fr')?"Équipe voyages d'affaires":"Business Travel Team") => $equipearray['affaires'],
    ((ICL_LANGUAGE_CODE=='fr')?"Équipe voyages loisirs":"Leisure Travel Team") => $equipearray['loisir']
);
minimal_get_template_part('/templates/heading.php', array('nav' => true, 'caption' => get_field('apropos-header_blurb')));
?>
<section class="spaced-top">
    <div class="container reveal">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-10 col-xxl-6 pb-md-5">
                    <h1 class="h3"><?php the_field('apropos-blurb_blurb'); ?></h1>
                </div>
                <div class="text-center-mobile">
                    <div class="zoomfx"><?php echo responsiveimage(array(
                            'url' => standardimage(get_field('apropos-image_image')),
                            'alt' => get_field('apropos-image_label'),
                            'classes' => 'img-fluid my-md-5'
                        )); ?></div>
                </div>
            </div>
            <div class="col-md-6 px-5 pt-4 pt-md-0 lead">
                <?php the_field('apropos-description_textarea'); ?>
            </div>
        </div>
    </div>
</section>
<section id="lepersonel">
    <div class="container">
        <?php $i = 0;
        foreach ($architectesgroups as $title => $architecgroup): ?>
            <div class="row reveal">
                <div class="col-md-12 text-center">
                    <?php if ($i == 0) { ?>
                        <h2 class="h3 subheading"><?php echo $title; ?></h2>
                    <?php } else { ?>
                        <h4 class="fiche subsubheading text-left"><?php echo $title; ?></h4>
                    <?php } ?>
                </div>
            </div>
            <div class="row reveal">
                <div class="col-md-12">
                    <?php
                    minimal_get_template_part('/templates/carousel.php', array(
                        'id' => 'personel' . $i,
                        'rowclasses' => 'card-deck',
                        'classes' => 'card personel',
                        'chunksize' => 4,
                        'template' => '/templates/personel.php',
                        'items' => $architecgroup
                    ));
                    ?>
                </div>
            </div>
            <?php $i++; endforeach; ?>
    </div>
</section>
<?php minimal_get_template_part('/templates/newsletter.php', array('classes' => 'spaced-top')); ?>

<?php get_footer(); ?>
