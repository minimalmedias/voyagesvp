<?php
/** Template Name: Évasion */
get_header();
#Grille Voyages
minimal_get_template_part('/templates/evasion-exception.php', array(
    'page' => 'evasion',
    'subcaption' => ((ICL_LANGUAGE_CODE=='fr')?'Évasion':'Leisure Travel'),
    'filtresections' => filtresections('evasion',ICL_LANGUAGE_CODE)
));
get_footer();
?>
