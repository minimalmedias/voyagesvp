Minimal Bootstrap Child Theme

The purpose of this theme is to be copied and modified as a child of the Minimal Bootstrap theme.
All SCSS classes and mixins are pulled from the style.scss of the parent theme to be made accessible in this child theme.
All css and js is them combined and minified via the gulp file to be loaded from the project root.

Please change the info in square brackets in the theme's root style.css and replace the screenshot.png to reflect the theme's design.