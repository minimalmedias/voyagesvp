<!DOCTYPE html>
<html class="no-js" lang="<?php echo ICL_LANGUAGE_CODE; ?>">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-12434838-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-12434838-1');
    </script>
    <title><?php wp_title('•', true, 'right');
        bloginfo('name'); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <?php wp_head(); ?></head>
<body <?php body_class(); ?> <?php echo(get_query_var('filter') == 'temoignages')?'data-filter="temoignages"':''; ?>>
<input type="hidden" name="wpml_lang" id="wpml_lang"
                                    value="<?php echo ICL_LANGUAGE_CODE; ?>">
<div id="minimalwrapper">
    <div class="minimalload site lang-<?php echo ICL_LANGUAGE_CODE; ?>"
         data-namespace="<?php echo (is_front_page()) ? 'home' : 'common'; ?>">
        <div class="leftvertical swinginleft <?php echo (!is_front_page()) ? 'standard-layout' : ''; ?>">            <?php $leftlink = get_template_link('page-contact.php');
            $leftlabel = __('Customise your trip', 'vp');
            if (basename(get_page_template()) == 'page-affaire.php') {
                $leftlink = '#soumission';
                $leftlabel = quicktran('Request a quote', 'Demander une soumission');
            } ?>
            <div><a class="btn btn-link" href="<?php echo $leftlink; ?>"><span
                            class="icon"></span> <?php echo $leftlabel; ?></a></div>
        </div>
        <div class="rightvertical swinginright <?php echo (!is_front_page()) ? 'standard-layout' : ''; ?>">
            <div>
                <div class="partagebox"><span><?php _e('Share', 'vp'); ?></span>
                    <div>                        <?php $meta = get_post_meta(get_the_ID(), 'vdw_gallery_id', true);
                        $media = wp_get_attachment_url($meta[0]); ?> <a target="_blank" class="share s_facebook"
                                                                        href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>"
                                                                        title="Share on Facebook."></a> <a
                                target="_blank" class="share s_linkedin"
                                href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                                title="Share on LinkedIn"></a> <a target="_blank" class="share s_pinterest"
                                                                  href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo $media; ?>"></a>
                        <!--a target="_blank" href="#" class="share s_instagram"></a-->                    </div>
                </div>
            </div>
        </div>
        <div class="site-container <?php echo (!is_front_page()) ? 'standard-layout' : ''; ?>">
            <header class="main-header <?php echo (is_front_page()) ? 'homesplash' : 'fixed-top'; ?>">
                <div class="top">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-auto mr-auto"><span
                                        class="align-middle"><?php _e('ONLINE BOOKING BY CONCUR', 'vp'); ?></span> <a
                                        href="https://www.concursolutions.com/" target="_blank"><img id="concur"
                                                                                                     src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/concur.png"></a>
                                <a href="tel:514-939-9999" id="topphone"><span class="align-middle">514 939-9999</span></a>
                            </div>
                            <div class="col-auto">
                                <form id="searchform" class="form-inline" role="search" method="get"
                                      action="<?php echo home_url('/'); ?>"><input type="text" id="s" name="s"
                                                                                   class="form-control pull-right"><input
                                            type="submit" value="" class="btn" id="searchbtn"></button>
                                </form>
                            </div>
                            <div class="col-auto langswitch">                                <?php echo minimal_language_switcher(); ?>                            </div>
                        </div>
                    </div>
                </div>
                <div class="container menucontainer">
                    <div class="row">
                        <div class="col">                            <?php ob_start(); ?>
                            <div class="container-fluid menurowcontainer">
                                <div class="row flex-md-row-reverse">
                                    <div class="menucol1 <?php echo (!is_front_page()) ? 'col-md-6 col-lg-7 col-xl-7' : ''; ?>"><?php echo wp_nav_menu(array('theme_location' => 'navbar', 'container' => false, 'echo' => false, 'fallback_cb' => '__return_false', 'items_wrap' => '<ul id="%1$s" class="topmenu1 navbar-nav ' . ((is_front_page()) ? 'justify-content-end' : 'justify-content-center') . '">%3$s</ul>', 'depth' => 2, 'walker' => new vp_nav_menu())); ?></div>
                                    <div class="menucol2 <?php echo (!is_front_page()) ? 'col-md-5 col-lg-4 col-xl-5' : ''; ?>"><?php echo wp_nav_menu(array('theme_location' => 'subnavbar', 'container' => false, 'echo' => false, 'fallback_cb' => '__return_false', 'items_wrap' => '<ul id="%1$s" class="topmenu2 navbar-nav justify-content-end">%3$s</ul>', 'depth' => 2, 'walker' => new vp_nav_menu())); ?></div>
                                    <div class="mobilelang">
                                        <ul class="list-unstyled">
                                            <li><?php echo minimal_language_switcher2(); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <?php $menu = ob_get_clean(); ?>                            <?php minimal_get_template_part('/components/headernavigation.php', array('homeurl' => esc_url(home_url('/')), 'logo' => ' <img src="' . get_stylesheet_directory_uri() . '/assets/img/voyages_vp_w.png" class="logo_white img-fluid" alt="' . __('Voyages VP', 'vp') . '">', 'classes' => 'main-menu', 'headerextra' => '<a href="tel:514-939-9999" class="mobile-phone pull-right"></a>', 'menuobject' => $menu)); ?>
                        </div>
                    </div>
                </div> <?php if (is_front_page()) { ?>
                    <video poster="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/homeslider/slide1-2000.jpg"
                           id="homeslider" playsinline autoplay muted loop>
                        <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/vp.webm"
                                type="video/webm">
                        <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/vp.mp4"
                                type="video/mp4">
                    </video>
                    <div class="homeoverlay"><h1><?php _e('Travel light', 'vp'); ?></h1></div>                <?php } ?>
            </header>