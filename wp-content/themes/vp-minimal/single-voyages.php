<?php
get_header();
$fiche = get_post(get_the_ID());
$fichetype = 'long'; # short|long
//if this has been favorited. $favorited = true or false dans la bd ou selon comment c'est monté.
$favorited = false;
$headeroptions = array('nav' => true, 'caption' => '', 'slides' => array());

if (get_field('banniere') != '') {
    $headeroptionslides[0] = wp_get_attachment_image_url(get_field('banniere'), 'header'); //first image in header is ACF banniere
    $slidecount = 1;
} else if (get_the_post_thumbnail_url(get_the_ID(), 'header') != '') {
    $headeroptionslides[0] = get_the_post_thumbnail_url(get_the_ID(), 'header'); //first image in header is featured thumbnail
    $slidecount = 1;
} else {
    $slidecount = 0;
}

if( have_rows('photos') ):
    while ( have_rows('photos') ) : the_row();
        if(get_sub_field('photo') !=''){
            $photo = get_sub_field('photo');
            if ($photo['sizes']['header'] != '') {
                $headeroptionslides[] = $photo['sizes']['header'];
                //$slidecount++;
            } else {
                $headeroptionslides[] = $photo['url'];
                //$slidecount++;
            }
        }
    endwhile;
endif;

echo '<pre style="display:none">';
print_r($headeroptionslides);
echo '</pre>';

$headeroptionslides = array_unique($headeroptionslides);
$slidecount = 0;
foreach ($headeroptionslides as $slide) {
    $headeroptions['slides'][$slidecount]['img'] = $slide;
    $slidecount++;
}
$details['short'] = array(
    array(
        "icon" => "icon-destination",
        "title" => "Destination",
        "text" => "Las Catalinas, Guanacaste (Costa Rica)"
    ),
    array(
        "icon" => "icon-family",
        "title" => "Famille",
        "text" => "Friendly"
    ),
    array(
        "icon" => "icon-rooms",
        "title" => "Chambres",
        "text" => "1 à 4"
    ),
    array(
        "icon" => "icon-people",
        "title" => "Acommodation",
        "text" => "Acommodation"
    ),
    array(
        "icon" => "icon-washrooms",
        "title" => "salles de bain",
        "text" => "4"
    )
);
$details['long'] = array();
$date_de_depart = array();
$d = 0;
if (trim(get_field('date_de_depart')) != '') {
    $details['long'][$d] = array(
        "icon" => "icon-dates",
        "title" => __("Dates de départ", "wpml_theme"),
        "text" => $date_de_depart
    );
    foreach (get_field('date_de_depart') as $row) {
        $date_de_depart[] = $row['date_de_depart_paragraphe'];
    }
    $d++;
}
if (get_field('duree') != '') {
    $details['long'][$d] = array(
        "icon" => "icon-days",
        "title" => quicktran("Duration", "Durée"),
        "text" => get_field('duree')
    );
    $d++;
}
if (get_field('depart_de') != '') {
    $details['long'][$d] = array(
        "icon" => "icon-map",
        "title" => quicktran("Departing from", "Départ de"),
        "text" => get_field('depart_de')
    );
    $d++;
}
if (get_field('pays') != '') {
    $details['long'][$d] = array(
        "icon" => "icon-destination",
        "title" => "Destination",
        "text" => get_field('pays')
    );
}
$config = array(
    'classes' => 'col-12 col-md-8',
    'subclasses' => 'col-xl-2',
    'header' => do_shortcode('[favorite_button]') . '<h5>' . extractterm(get_the_ID(), 'type') . '</h5><h1 class="h2">' . get_the_title() . '</h1>',
    'details' => $details[$fichetype],
);
if ($favorited) {
    $config['favorited'] = true;
}



minimal_get_template_part('/templates/heading.php', $headeroptions);
minimal_get_template_part('/templates/heading-details.php', $config);
?>
<section id="affixedbutton" <?php echo (empty($details[$fichetype])) ? 'class="spaced-top desktop-positioned"' : ''; ?>>
    <div class="container">
        <div class="row">
            <div class="col mobile-centered">
                <a href="#inforequest<?php //echo get_template_link('page-contact.php'); ?>"
                   class="btn btn-secondary pull-right"><span
                            class="icon bell"></span> <?php _e('Request information', 'vp'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php if ($fichetype == 'long'): ?>
    <section>
        <?php
        #Image + text
        $blocid = 'fichevoyage-bloc1';
        if ((get_field($blocid . '_title') == '') and (get_field($blocid . '_text') == '')) {
        } else {
            minimal_get_template_part('/templates/image-texte.php', array(
                'image' => array(
                    'url' => standardimage(get_field($blocid . '_image')),
                    'alt' => get_field($blocid . '_image_label'),
                    'classes' => 'img-fluid my-md-5'
                ),
                'text' => array(
                    'classes' => 'text-right-md',
                    'title' => get_field($blocid . '_title'),
                    'text' => get_field($blocid . '_text'),
                    'button' => array(
                        'label' => get_field($blocid . '_button_label'),
                        'url' => get_field($blocid . '_button_link')
                    )
                ),
                'overlap' => true,
                'layout' => 'image-text'
            ));
        }
        #Texte + image
        $blocid = 'fichevoyage-bloc2';
        if ((get_field($blocid . '_title') == '') and (get_field($blocid . '_text') == '')) {
        } else {
            minimal_get_template_part('/templates/image-texte.php', array(
                'image' => array(
                    'url' => standardimage(get_field($blocid . '_image')),
                    'alt' => get_field($blocid . '_image_label'),
                    'classes' => 'img-fluid my-md-5'
                ),
                'text' => array(
                    'classes' => 'text-right-md',
                    'title' => get_field($blocid . '_title'),
                    'text' => get_field($blocid . '_text'),
                    'button' => array(
                        'label' => get_field($blocid . '_button_label'),
                        'url' => get_field($blocid . '_button_link')
                    )
                ),
                'overlap' => true,
                'layout' => 'text-image'
            ));
        }
        #itineraire
        minimal_get_template_part('/templates/fiche-itineraire-long.php', array(
            'les_points_forts' => get_field('les_points_forts'),
            'texte_inclusions' => get_field('texte_inclusions'),
            'itineraire' => apply_filters('the_content', $fiche->post_content)
        ));
        ?>
    </section>
    <?php
    if (get_field('map_select')) {
        if (get_field('map_select') != '') {
            minimal_get_template_part('/templates/fiche-escales.php', array('map_select' => get_field('map_select')));
        }
    }
    ?>

<?php endif; ?>
<?php if ($fichetype == 'short'): ?>
    <?php minimal_get_template_part('/templates/fiche-itineraire-short.php', array()); ?>
    <?php minimal_get_template_part('/templates/avantages.php', array('classes' => 'spaced-top')); ?>
<?php endif; ?>
<?php
//$classes = ($fichetype == 'long') ? array('classes' => 'spaced-top') : array();
//minimal_get_template_part('/templates/newsletter.php', $classes);
?>

<?php
$similar = array();
$z = 0;
if (have_rows('vous_aimerez_aussi')):
    while (have_rows('vous_aimerez_aussi')) : the_row();
        $id = get_sub_field('voyage');
        $status = get_post_status($id);

        if($status=='publish') {
            $similar[$z]['id'] = $id;
            $similar[$z]['lang'] = ICL_LANGUAGE_CODE;
            $similar[$z]['title'] = get_the_title($id);
            $similar[$z]['description'] = wp_trim_words(get_the_excerpt($id), 25, '...');
            $similar[$z]['url'] = get_permalink($id);
            $similar[$z]['image'] = array(
                'url' => output_thumbnails($id, 'fiche'),
                'alt' => get_the_title(),
                'classes' => 'img-fluid'
            );
            $similar[$z]['label'] = extractterm($id, 'pays');
            $z++;
        }

    endwhile;
endif;
if (intval(count($similar)) > 0):
    ?>
    <pre style="display:none">
        <?php print_r($similar); ?>
    </pre>
    <section class="reveal spaced-top">
        <div class="container">
            <div class="row">
                <div class="col"><h5 class="subtitle"><?php _e('You will also enjoy', 'vp'); ?></h5></div>
            </div>
            <div class="card-deck">
                <?php
                foreach ($similar as $voyage):
                    minimal_get_template_part('/templates/listing.php', $voyage);
                endforeach;
                $empties = (3 - intval(count($similar)));
                for ($k = 0; $k < $empties; $k++):
                    echo '<div class="card fiche offre emptycell" style="background:none"></div>';
                endfor;
                ?>
            </div>
        </div>
    </section>
<?php endif; ?>

    <span id="inforequest" class="spaced-top"></span>

<?php
//$current_user = wp_get_current_user();
//if (user_can($current_user, 'administrator')):
    ?>
    <section class="reveal">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <?php
                    $formulaire['en'] = '[contact-form-7 id="21599" title="Fiche Voyage EN"]';
                    $formulaire['fr'] = '[contact-form-7 id="21598" title="Fiche Voyage FR"]';
                    echo do_shortcode($formulaire[ICL_LANGUAGE_CODE]);
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php //endif; ?>

<section class="spaced-top">&nbsp;</section>

<?php get_footer(); ?>