<?php get_header(); ?>
<section class="articleheading">
    <?php
    if (have_posts()): while (have_posts()):
    the_post();
    if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
        $image_id = get_post_thumbnail_id(get_the_ID());
        $smallimage = wp_get_attachment_image_src($image_id, 'articleheadermobile');
        $largeimage = wp_get_attachment_image_src($image_id, 'articleheader');
        echo responsiveimage(
            array(
                'url' => array(
                    '(max-width: 400px)' => $smallimage[0],
                    '(min-width:401px)' => $largeimage[0]
                ),
                'alt' => get_the_title(),
                'classes' => 'img-fluid'
            )
        );
    }
    ?>
</section>
    <nav class="topnav">
        <div class="container">
            <span class="navback"><a href="<?php echo get_template_link('page-inspirations.php'); ?>"><?php _e('Return to menu', 'vp'); ?></a></span>
        </div>
    </nav>
    <article>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="articlehead">
                        <?php echo do_shortcode('[favorite_button]'); ?>
                        <h5><?php the_time('d-m-Y') ?></h5>
                        <h1 class="h2"><?php the_title() ?></h1>
                        <p><?php the_author(); ?></p>
                        <p class="blurb"><?php //the_excerpt();
                            ?></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6 articlecontent">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </article>
<!--section>
    <div class="container">
        <div class="row">
            <div class="col text-center">
                [instagram]
            </div>
        </div>
    </div>
</section-->
<?php endwhile; ?>
<?php else: ?>
    <?php wp_redirect(get_bloginfo('url') . '/404', 404);
    exit; ?>
<?php endif; ?>
<nav class="bottomnav">
    <div class="container">
        <?php next_post_link( '<span class="navback">%link</span>', __('Previous article', 'vp') ); ?>
        <span class="icon"></span>
        <?php next_post_link( '<span class="navforward">%link</span>',  __('Next article', 'vp') ); ?>
    </div>
</nav>
<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>
<?php get_footer(); ?>

