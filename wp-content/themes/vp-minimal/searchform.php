<?php
/**
 * Template for displaying search forms
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="s" class="assistive-text"><?php _e( 'Search', 'twentyeleven' ); ?></label>
		<input type="text" class="field" name="s" id="s"/>
		<input type="submit" class="submit" class="btn pull-right" name="submit" value="" id="searchsubmit"/>
	</form>
