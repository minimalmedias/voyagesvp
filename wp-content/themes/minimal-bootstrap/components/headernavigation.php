<?php
/*
COMPONENT: HEADER NAVIGATION
    minimal_get_template_part('/components/headernavigation.php',array(
        'homeurl' => string         // Url for logo link
        'logo' => string,           // text/image/object for logo
        'classes' => string,        // optional classes for nav container
        'headerextra' => string,    // optional content for header, typicaly aligned right and mobile only. ie: Language
        'menuobject' => obj|string        // if array, provides basic list menu, typicaly wp_nav_menu() object
    ));
    TIP: Place into <header> tags and use class <header class="fixed-top"> to make fixed.
    Optionaly activate affix javascript to use with scroll-spy
    Optionaly load fullscreen hero into <header>
*/
?>
<nav class="navbar navbar-expand-md <?php echo (isset($this->classes))?$this->classes:''; ?>">
    <button class="navbar-toggler float-left" type="button" data-toggle="collapse"
            data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-bars"></span>
    </button>
    <a class="navbar-brand" href="<?php echo $this->homeurl; ?>"><?php echo $this->logo; ?></a>
    <?php echo (isset($this->headerextra))?$this->headerextra:''; ?>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <?php echo $this->menuobject; ?>
    </div>
</nav>