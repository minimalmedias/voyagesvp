<?php
/*
COMPONENT: CAROUSEL
    minimal_get_template_part('/components/carousel.php',array(
        //minimal configuration
        'id' => $var,           // unique id for carousel
        'items' => array,       // array of items to loop through. Items can be strings or arrays if optional templates are used.
        //optional configuration
        'interval' => int,      // Speed of autoplay. Defaults to no autoplay.
        'chunksize' => int,     // number of items per slide. Defaults to 1 per slide if not set.
        'rowclasses' => $var,   // optional classes for items container row in slide
        'template' => $var,     // optional wrapper for content
        'classes' => $var,      // classes applied to looped item. If chunks are used, this applies to the chunk's sub item.
        'nav' => bool           // if set to false, will hide nav even if there are multiple items. defaults to true.
    ));
*/
?>
<div id="<?php echo $this->id; ?>" class="carousel <?php /* responsive-carousel */ ?> slide" data-ride="carousel" data-interval="<?php echo (isset($this->interval))?$this->interval:false; ?>">
    <div class="carousel-inner">
        <?php
        $navigation = false;
        echo $navigation;
        if (isset($this->chunksize)) { //if multiple items displayed per slide
            $i = 0;
            $chunks = array_chunk($this->items, $this->chunksize);
            if (count($chunks) > 1) {
                $navigation = true;
            }
            foreach ($chunks as $chunk):
                $chunkcount = count($chunk);
                $empties = ($this->chunksize - $chunkcount);
                ?>
                <div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
                    <div class="container-fluid">
                        <div class="<?php echo (isset($this->rowclasses)) ? $this->rowclasses : 'row'; ?>">
                            <?php $ii = 0;
                            foreach ($chunk as $card): ?>
                                <?php
                                if(isset($this->template)){
                                    minimal_get_template_part($this->template, $card);
                                } else {
                                    echo $card;
                                }
                                ?>
                                <?php $ii++; endforeach; ?>
                            <?php for ($k = 0; $k < $empties; $k++): ?>
                                <div class="emptycell <?php echo (isset($this->classes)) ? $this->classes : ''; ?>"></div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <?php $i++; endforeach; ?>

        <?php } else { ?>
            <?php
            if (count($this->items) > 1) {
                $navigation = true;
            } ?>
            <?php $i = 0;
            foreach ($this->items as $card): ?>
                <div class="carousel-item <?php echo (isset($this->classes)) ? $this->classes : '';
                echo ($i == 0) ? ' active' : ''; ?>">
                    <?php minimal_get_template_part($this->template, $card); ?>
                </div>
                <?php $i++; endforeach; ?>
        <?php } ?>
    </div>
    <?php if(isset($this->nav)){ if($this->nav === false){ $navigation = false; }  }; ?>
    <?php if ($navigation): ?>
        <a class="carousel-control-prev" href="#<?php echo $this->id; ?>" role="button" data-slide="prev">
            <span class="arrownav" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#<?php echo $this->id; ?>" role="button" data-slide="next">
            <span class="arrownav" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    <?php endif;  ?>
</div>