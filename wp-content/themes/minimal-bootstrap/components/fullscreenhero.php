<?php
/*
COMPONENT: FULLSCREEN HERO
    minimal_get_template_part('/components/fullscreenhero.php',array(
        'content' => $var, // content string.
    ));
    TIP: add class "vertical-align-center" to content to verticaly align to middle
*/
?>
<section class="heading jumbotron jumbotron-fluid screen-height-md">
    <?php echo $this->content; ?>
</section>