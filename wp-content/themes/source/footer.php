<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 */
?>

	</div><!-- #content -->


	<div class="infolettre">
		<div class="container">
			<div class="infolettre-gauche">
				<?php if(get_field('titre_infolettre', get_option('page_on_front'))): ?>
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/Paper-plane.png" alt="paper-plane" /><?php the_field('titre_infolettre', get_option('page_on_front')); ?></h2>
				<?php endif; ?>
			</div>
			<div class="infolettre-droite">
				<form name="infolettreFooter" method="post" action="/cakemail.php" onsubmit="return validaterInfolettreFooter()">
					<div class="input-email-container">
						<input type="email" name="courriel" placeholder="Entrez votre adresse courriel" required />
					</div>
					<input type="submit" value="Inscription" />
				</form>
			</div>
		</div>
	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div itemscope itemtype="http://schema.org/LocalBusiness">
						<?php if(get_field('colonne_1_titre', 'option')): ?>
							<h2><span itemprop="name"><?php the_field('colonne_1_titre', 'option'); ?></span></h2>
						<?php endif; ?>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<?php if(get_field('colonne_1_texte', 'option')): ?>
								<div class="adresse"><?php the_field('colonne_1_texte', 'option'); ?></div>
							<?php endif; ?>
						</div>
					</div>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"  class="logo-footer"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo<?php if(ICL_LANGUAGE_CODE=='en') {echo "-en";}?>.png" class="img-responsive" alt="<?php bloginfo( 'name' ); ?>"/></a>
				</div>
				<div class="col-sm-3 hidden-xs">
					<?php if(get_field('colonne_2_titre', 'option')): ?>
						<h2><?php the_field('colonne_2_titre', 'option'); ?></h2>
					<?php endif; ?>
					<?php 
						$args = array('theme_location' => 'footer', 'container_class' => 'footermenu'); 
						wp_nav_menu($args);
					?>
				</div>
				<div class="col-sm-3 hidden-xs">
					<?php if(get_field('colonne_3_titre', 'option')): ?>
						<h2><?php the_field('colonne_3_titre', 'option'); ?></h2>
					<?php endif; ?>
					<?php 
						$args = array('theme_location' => 'footer2', 'container_class' => 'footermenu'); 
						wp_nav_menu($args);
					?>
				</div>
				<div class="col-sm-3 hidden-xs">

					<?php /*if(get_field('colonne_4_titre_1', 'option')): ?>
						<h2><?php the_field('colonne_4_titre_1', 'option'); ?></h2>
					<?php endif; ?>
					<?php 
						$args = array('theme_location' => 'footer3', 'container_class' => 'footermenu'); 
						wp_nav_menu($args);
					*/ ?>

					<?php if(get_field('colonne_4_titre_2', 'option')): ?>
						<h2 <?php /* class="deuxieme-h2" */ ?> ><?php the_field('colonne_4_titre_2', 'option'); ?></h2>
					<?php endif; ?>
					<?php 
						$args = array('theme_location' => 'footer4', 'container_class' => 'footermenu'); 
						wp_nav_menu($args);
					?>
					<div class="social-buttons">
						<?php if(get_field('lien_page_facebook', 'option')){?>
						<a href="<?php the_field('lien_page_facebook', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Facebook.png" alt="" /></a>
						<?php } ?>
						<?php if(get_field('lien_page_instagram', 'option')){?>
						<a href="<?php the_field('lien_page_instagram', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Instagram.png" alt="" /></a>
						<?php } ?>
						<?php if(get_field('lien_page_twitter', 'option')){?>
						<a href="<?php the_field('lien_page_twitter', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_twitter.png" alt="" /></a>
						<?php } ?>
						<?php if(get_field('lien_page_linkedin', 'option')){?>
						<a href="<?php the_field('lien_page_linkedin', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_google.png" alt="" /></a>
						<?php } ?>
						<?php if(get_field('lien_page_pinterest', 'option')){?>
						<a href="<?php the_field('lien_page_pinterest', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_pinterest.png" alt="" /></a>
						<?php } ?>
						<?php if(get_field('lien_page_google_+', 'option')){?>
						<a href="<?php the_field('lien_page_google_+', 'option');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_google.png" alt="" /></a>
						<?php } ?>
					</div>
				</div>
				<div class="col-sm-12 bas-de-pied-de-page">
					<?php if(get_field('texte_bas_du_pied_de_page', 'option')): ?>
						<?php the_field('texte_bas_du_pied_de_page', 'option'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
	

</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>