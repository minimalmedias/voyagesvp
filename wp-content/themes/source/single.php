<?php
/**
 * The Template for displaying all single posts.
 *
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */
get_header(); ?>


<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
			
				<?php while ( have_posts() ) : the_post(); ?>
			
					<?php get_template_part( 'content', 'single' ); ?>
					<hr />
					<?php upbootwp_content_nav( 'nav-below' ); ?>
			
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>
			
				<?php endwhile; // end of the loop. ?>
			
				</main>
			</div>
			
		</div>
	</div>
</div>
<?php get_footer(); ?>