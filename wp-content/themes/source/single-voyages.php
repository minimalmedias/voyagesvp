<?php
/**
 * The Template for displaying all single posts.
 *
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<div class="header-fiche-voyage" style="">
	<?php $img = get_field('banniere');
		if($img):
		 $thumb = $img['sizes']['banner-fiche-voyage'] ?>
		<img src="<?php echo $thumb; ?>" alt="<?php echo $img['alt']; ?>" />
	<?php endif; ?>
	<div class="titre-fiche-voyage">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9 col-md-offset-3">
					<div class="overlay-table">
						<div class="overlay-cell">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="banniere-bleu">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-9 col-md-offset-3">
				<?php //$terms = wp_get_post_terms( $post_id, $taxonomy, $args ); ?>
				<?php
					$pays = wp_get_post_terms( $post->ID, 'pays' );
					if($pays): ?>
						<div class="pays"><span class="etiquette">Destination : </span>
							<?php foreach($pays as $p): ?>
								<span ><?php echo $p->name; ?></span>
							<?php endforeach; ?>
						</div>
				<?php endif; ?>
				<?php
					$theme = wp_get_post_terms( $post->ID, 'theme' );
					if($theme): ?>
						<div class="theme"><span class="etiquette">Thème : </span>
							<?php foreach($theme as $t): ?>
								<span><?php echo $t->name; ?></span>
							<?php endforeach; ?>
						</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="banniere-bleu-gris">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-9 col-md-offset-3">
				<button type="button" class="navbar-toggle navbar-toggle-fiche-voyage" data-toggle="collapse" data-target=".navbar-collapse-fiche-voyage">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<div class="visible-xs">
					<div class="navbar-collapse collapse navbar-collapse-fiche-voyage">
						<ul class="tab-selector-mobile">
							<li role="presentation" class="active"><a>ITINÉRAIRE</a></li>
							<?php if(get_field('texte_prix')): ?>
								<li role="presentation"><a>PRIX</a></li>
							<?php endif; ?>
							<?php if(get_field('texte_hebergement')): ?>
								<li role="presentation"><a>HÉBERGEMENT(S)</a></li>
							<?php endif; ?>
							<?php if(get_field('texte_inclusions')): ?>
								<li role="presentation"><a>INCLUSIONS</a></li>
							<?php endif; ?>
							<?php if(get_field('texte_conditions_generales')): ?>
								<li role="presentation"><a>CONDITIONS GÉNÉRALES</a></li>
							<?php endif; ?>
							<?php if(get_field('liste_de_specialistes')): ?>
								<li role="presentation"><a>NOS SPÉCIALISTES</a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>

				<div class="hidden-xs tab-selector">
					<ul>
						<li role="presentation" class="active"><a href="#itineraire" aria-controls="itineraire" role="tab" data-toggle="tab">ITINÉRAIRE</a></li>
						<?php if(get_field('texte_prix')): ?>
							<li role="presentation"><a href="#prix" aria-controls="prix" role="tab" data-toggle="tab" id="bouton-prix">PRIX</a></li>
						<?php endif; ?>
						<?php if(get_field('texte_hebergement')): ?>
							<li role="presentation"><a href="#hebergement" aria-controls="hebergement" role="tab" data-toggle="tab">HÉBERGEMENT(S)</a></li>
						<?php endif; ?>
						<?php if(get_field('texte_inclusions')): ?>
							<li role="presentation"><a href="#inclusions" aria-controls="inclusions" role="tab" data-toggle="tab">INCLUSIONS</a></li>
						<?php endif; ?>
						<?php if(get_field('texte_conditions_generales')): ?>
							<li role="presentation"><a href="#conditions" aria-controls="conditions" role="tab" data-toggle="tab">CONDITIONS GÉNÉRALES</a></li>
						<?php endif; ?>
						<?php if(get_field('liste_de_specialistes')): ?>
							<li role="presentation"><a href="#specialistes" aria-controls="specialistes" role="tab" data-toggle="tab">NOS SPÉCIALISTES</a></li>
						<?php endif; ?>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="itineraire">

					<div class="itineraire">
						<?php /* <h2>Itinéraire</h2> */ ?>
						<div class="row">
							<div class="col-sm-12 col-md-11">
								<?php the_content(); ?>
							</div>
						</div>
						<?php if(get_field('zone_italique')): ?>
						<div class="zone-italic">
							<?php while( has_sub_field('zone_italique') ): ?>
								<div class="row">
									<div class="col-sm-4 text-zone-italic">
										<?php if(get_sub_field('colonne_1')): ?>
											<?php the_sub_field('colonne_1'); ?>
										<?php endif; ?>
									</div>
									<div class="col-sm-4 text-zone-italic">
										<?php if(get_sub_field('colonne_2')): ?>
											<?php the_sub_field('colonne_2'); ?>
										<?php endif; ?>
									</div>
									<div class="col-sm-4 text-zone-italic">
										<?php if(get_sub_field('colonne_3')): ?>
											<?php the_sub_field('colonne_3'); ?>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
					</div>
		
					<?php if(get_field('les_points_forts')): ?>
					<div class="points-forts">
						<h3>Les points forts</h3>
						<?php the_field('les_points_forts'); ?>
					</div>
					<?php endif; ?>

					<?php if( get_field('map_select') ): ?>
					<?php while( has_sub_field('map_select') ): ?>

						<?php if(get_row_layout() == "google_map"):  ?>
							
							<?php if(get_sub_field('map')): ?>
							<div class="map">
								<?php the_sub_field('map'); ?>
							</div>
							<?php endif; ?>

						<?php elseif(get_row_layout() == "image_map"): ?>
							
							<?php $img = get_sub_field('image_map_f');
								if($img): ?>
									<img class="map" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>"/>
							<?php endif; ?>
							
						<?php endif; ?>

					<?php endwhile; ?>
					<?php endif; ?>
		
					<?php if(get_field('map')): ?>
					<div class="map">
						<?php the_field('map'); ?>
					</div>
					<?php endif; ?>
		
					<?php if(get_field('boites_sous_map')): ?>
					<div class="horaire">
						<?php while( has_sub_field('boites_sous_map') ): ?>
						<div class="horaire-boite">
							<?php if(get_sub_field('titre')): ?>
								<h2><?php the_sub_field('titre'); ?></h2>
							<?php endif; ?>
							<?php if(get_sub_field('texte')): ?>
								<?php the_sub_field('texte'); ?>
							<?php endif; ?>
						</div>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
			
				</div>


				<?php if(get_field('texte_prix')): ?>
				<div role="tabpanel" class="tab-pane fade" id="prix">
					<div class="itineraire">
						<?php /* <h2>Prix</h2> */ ?>
						<?php the_field('texte_prix'); ?>
					</div>
				</div>
				<?php endif; ?>
				<?php if(get_field('texte_hebergement')): ?>
				<div role="tabpanel" class="tab-pane fade" id="hebergement">
					<div class="itineraire">
						<?php /* <h2>Hébergement(s)</h2> */ ?>
						<?php the_field('texte_hebergement'); ?>
					</div>
				</div>
				<?php endif; ?>
				<?php if(get_field('texte_inclusions')): ?>
				<div role="tabpanel" class="tab-pane fade" id="inclusions">
					<div class="itineraire">
						<?php /* <h2>Inclusions</h2> */ ?>
						<?php the_field('texte_inclusions'); ?>
					</div>
				</div>
				<?php endif; ?>
				<?php if(get_field('texte_conditions_generales')): ?>
				<div role="tabpanel" class="tab-pane fade" id="conditions">
					<div class="itineraire">
						<?php /* <h2>Conditions générales</h2> */ ?>
						<?php the_field('texte_conditions_generales'); ?>
					</div>
				</div>
				<?php endif; ?>
				<?php if(get_field('liste_de_specialistes')): ?>
				<div role="tabpanel" class="tab-pane fade" id="specialistes">
					<div class="itineraire">
						<?php /* <h2>Nos spécialistes</h2> */ ?>

						<div class="row">
						<?php while ( have_rows('liste_de_specialistes') ) : the_row(); ?>

							<?php 

								$author = get_sub_field('specialiste');

								if( $author ){

									$authorid = $author["ID"];
									$photo = get_field("photo", "user_".$authorid."");
									$nom_complet = get_field("nom_complet",  "user_".$authorid."");
									$titre = get_field("titre",  "user_".$authorid."");
									$lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");

								?>

								<div class="col-md-6">
									<div class="section-specialiste">
	
										<div class="media">
											<div class="media-left">
												<a href="<?php echo $lien_vers_la_page_de_profil; ?>">
													<img class="media-object specialiste-image" src="<?php echo $photo["url"]; ?>" alt="<?php echo $photo["alt"]; ?>">
												</a>
											</div>
											<div class="media-body">
												<strong><?php echo $nom_complet; ?></strong><br />
												<?php echo $titre; ?>
											</div>
										</div>
	
									</div>
								</div>

							<?php } ?>

						<?php endwhile; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<div class="fiche-voyage-bouton hidden-xs">
					<div class="row">
						<?php
							$lien = get_field('page_demande_dinformation', 'option');
							$lien .= '?voyage=' . $post->ID;
						?>
						<div class="col-sm-6">
							<a class="btn btn-bleu" href="<?php echo $lien; ?>">
								<span>Demande d’informations</span>
								<img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" />
								<span class="clearfix"></span>
							</a>
						</div>

						<script type="text/javascript">var switchTo5x=true;</script>
						<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
						<script type="text/javascript">stLight.options({publisher: "a9241dc9-5ad3-4372-b9b2-6a60d6d656d2", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

						<div class="col-sm-6">
							<div class="partager-liens-bas">
								<span class='st_email_large' displayText='Email'></span>
								<span class='st_facebook_large' displayText='Facebook'></span>
								<span class='st_pinterest_large' displayText='Print'></span>
								<span class='st_sharethis_large' displayText='ShareThis'></span>
								<span class='st_print_large' displayText='Print'></span>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

		<div class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9">
			<div class="sidebar-fiche-voyage">

				<?php if( get_field('date_de_depart') || get_field('duree') || get_field('depart_de') || get_field('pays') ):  ?>

				<div class="sidebar-fiche-voyage-boite">

					<?php if( get_field('date_de_depart') ): ?>
					<h2>DATES DE DÉPART</h2>

					<?php while( has_sub_field('date_de_depart') ): ?>

						<?php if(get_row_layout() == "par_mois"):  ?>
							<div class="month-list">

								<?php if(get_sub_field('janvier')): ?>
									<div>Janvier : <?php the_sub_field('janvier'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('fevrier')): ?>
									<div>Février : <?php the_sub_field('fevrier'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('mars')): ?>
									<div>Mars : <?php the_sub_field('mars'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('avril')): ?>
									<div>Avril : <?php the_sub_field('avril'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('mai')): ?>
									<div>Mai : <?php the_sub_field('mai'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('juin')): ?>
									<div>Juin : <?php the_sub_field('juin'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('juillet')): ?>
									<div>Juillet : <?php the_sub_field('juillet'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('aout')): ?>
									<div>Août : <?php the_sub_field('aout'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('septembre')): ?>
									<div>Septembre : <?php the_sub_field('septembre'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('octobre')): ?>
									<div>Octobre : <?php the_sub_field('octobre'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('novembre')): ?>
									<div>Novembre : <?php the_sub_field('novembre'); ?></div>
								<?php endif; ?>
								<?php if(get_sub_field('decembre')): ?>
									<div>Décembre : <?php the_sub_field('decembre'); ?></div>
								<?php endif; ?>

							</div>

						<?php elseif(get_row_layout() == "paragraphe"): ?>
							<div class="depart-p">
								<?php if(get_sub_field('date_de_depart_paragraphe')): ?>
									<?php the_sub_field('date_de_depart_paragraphe'); ?>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					<?php endwhile; ?>
					<?php endif; ?>

					<?php if(get_field('duree')): ?>
					<div class="sfvb-section">
						<h2>DURÉE</h2>
						<p><?php the_field('duree'); ?></p>
					</div>
					<?php endif; ?>

					<?php if(get_field('depart_de')): ?>
					<div class="sfvb-section">
						<h2>DÉPART DE</h2>
						<p><?php the_field('depart_de'); ?></p>
					</div>
					<?php endif; ?>

					<?php if(get_field('pays')): ?>
					<div class="sfvb-section">
						<h2>Destination</h2>
						<?php the_field('pays'); ?>
					</div>
					<?php endif; ?>

				</div>
				<?php endif; ?>

				<?php if(get_field('prix_colonne_laterale')): ?>
				<div class="sidebar-fiche-voyage-boite boite-prix">
					<h3>À partir de</h3>
					<h4><?php the_field('prix_colonne_laterale'); ?></h4>
					<?php if(get_field('texte_prix')): ?>
						<a class="lien-bleu voir-tous-les-prix">Voir tous les prix<img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-blue-next.png" /></a>
					<?php endif; ?>
				</div>
				<?php endif; ?>

				<div class="sidebar-fiche-voyage-boite boite-reservation">
					<a class="btn btn-jaune" href="<?php echo $lien; ?>">
						<span>Demande d’informations</span>
						<img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" />
						<span class="clearfix"></span>
					</a>

					<div class="sfvb-section">
						<p>Pour en savoir plus sur ce voyage, contactez-nous au <strong>514 939-9999.</strong></p>
					</div>

				</div>

				<div class="sidebar-fiche-voyage-boite" id="boite-partager">
					<h3>PARTAGER</h3>
					<div class="partager-liens">
						<span class='st_email_large' displayText='Email'></span>
						<span class='st_facebook_large' displayText='Facebook'></span>
						<span class='st_pinterest_large' displayText='Print'></span>
						<span class='st_sharethis_large' displayText='ShareThis'></span>
						<span class='st_print_large' displayText='Print'></span>
					</div>
				</div>

				<?php if(get_field('photos')): ?>
				<div class="galerie-photo">

					<h2>GALERIE PHOTOS</h2>

					<?php while( has_sub_field('photos') ): ?>
						<?php $image = null;
							$image = get_sub_field('image');
							$origial = $image['original_image']['url'];
							if($image): ?>
							<div class="photo">
								<a href="<?php echo $origial; ?>" rel="prettyPhoto[Galery]" title="<?php the_sub_field('description_de_la_photo'); ?>">
									<div class="overlay-photo">
										<div class="overlay-table">
											<div class="overlay-cell">
												<img src="<?php echo get_template_directory_uri(); ?>/images/loupe.png" />
											</div>
										</div>
									</div>
									<img src="<?php echo $image['url']; ?>" alt="alt" title="title" class="img-responsive" />
								</a>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>

					<div class="clearfix"></div>

				</div>
				<?php endif; ?>


				<?php if(get_field('video')): ?>
				<div class="galerie-video">

					<h2>GALERIE VIDÉOS</h2>

					<?php while( has_sub_field('video') ): ?>
						<?php
							$url = get_sub_field('url');
							if($url){
								if (strpos($url,'www.youtube.com/watch?v=') !== false) {
									$thumb = $url;
									if(strcmp(substr($thumb, 0, 8), 'https://')){
										$thumb = substr($thumb, 8);
									}
									elseif(strcmp(substr($thumb, 0, 7), 'http://')){
										$thumb = substr($thumb, 7);
									}
									$thumb = substr($thumb, 25);
									$thumb = 'http://img.youtube.com/vi/' . $thumb . '/hqdefault.jpg';
							?>
							<div class="video">
								<a href="<?php the_sub_field('url'); ?>" rel="prettyPhoto" title="<?php the_sub_field('description_du_video'); ?><span id='video-url'><?php the_sub_field('url'); ?></span>">
									<div class="overlay-video">
										<div class="overlay-table">
											<div class="overlay-cell">
												<div class="img-container">
													<img src="<?php echo get_template_directory_uri(); ?>/images/play-fiche-voyage.png" />
												</div>
											</div>
										</div>
									</div>
									<img src="<?php echo $thumb; ?>" class="img-responsive" />
								</a>
							</div>
						<?php } } ?>
					<?php endwhile; ?>

				</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>