<?php
/**
 * Template Name: Page - Sitemap
 * The template used for displaying page content in page.php
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<div class="section">
		<div class="container">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="entry-content">
				<?php the_content(); ?>
				
				<?php $pages = wp_list_pages('title_li=&exclude=8,10&echo=0'); ?>
				<?php if($pages){ ?>
					<h3>Pages</h3>
					<ul>
						<?php echo $pages; ?>
					</ul>
				<?php } ?>
					
				<?php 
				$query = new WP_Query( array( 
					'post_type' => 'product',
					'posts_per_page' => -1,
					'post_status' => 'publish',
					'order' => 'ASC',
					'orderby' => 'title'
				) );
				
				if ( $query->have_posts() ) : ?>
					<h3><?php _e("Products"); ?></h3>
					<ul>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</li>
					<?php endwhile; wp_reset_postdata();  ?>
					</ul>
				<?php endif;  ?>
				
				<?php
				$query = new WP_Query( array( 
					'post_type' => 'collection',
					'posts_per_page' => -1,
					'post_status' => 'publish',
					'order' => 'ASC',
					'orderby' => 'title'
				) );
				
				if ( $query->have_posts() ) : ?>
					<h3><?php _e("Collections"); ?></h3>
					<ul>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</li>
					<?php endwhile; wp_reset_postdata();  ?>
					</ul>
				<?php endif;  ?>
				
			</div>
		</div>
	</div>
<?php endwhile; ?>	
<?php get_footer(); ?>