<?php
/**
 * Template Name: Page - Full width
 * The template used for displaying page content in page.php
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
<?php
	$urlImage = "";
	$image = get_field('image-banner');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<section class="blue" <?php if(!empty($image)){ ?>style="<?php echo $urlImage; ?>" <?php } ?>>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			<div class="col-sm-12">
				<?php /* <h1 class="page-title text-center"><?php the_title(); ?></h1> */ ?>

				<div class="entry-content">
					    <?php the_content(); ?>
					    <?php endwhile; // end of the loop. ?>
					    
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		
<?php get_footer(); ?>