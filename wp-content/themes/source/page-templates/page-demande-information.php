<?php
/*
Template Name: Demande d'information
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

<?php 
	$voyage = $_GET["voyage"];

	if( $voyage ){

		if( get_post_type( $voyage ) == 'voyages' ){

			$voyageOK = true;

			$titre = get_the_title( $voyage );

		}

	}

?>


<section class="blue">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>


<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			<div class="col-sm-6">
				<?php if($voyageOK): ?>
					<h2 id="titre-du-voyage"><?php echo $titre; ?></h2>
				<?php endif; ?>

				<?php if(get_field('formulaire')): ?>
					<div class="contact-form">
						<?php the_field('formulaire'); ?>
					</div>
				<?php endif; ?>
			</div>

		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>