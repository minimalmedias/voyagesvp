<?php
/**
 * Template Name: Page - Équipe - Fiche
 * The template used for displaying page content in page.php
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>


    <?php 
    // get this page's specified author
    $author = get_field("auteur");
    // find ID for ACF
    $authorid = $author["ID"];

    // retrieve all ACF fields from author
    $photo = get_field("photo", "user_".$authorid."");
    $banniere_du_profil = get_field("banniere_du_profil", "user_".$authorid."");
    $nom_complet = get_field("nom_complet",  "user_".$authorid."");
    $titre = get_field("titre",  "user_".$authorid."");
    $telephone = get_field("telephone",  "user_".$authorid."");
    $courriel = get_field("courriel",  "user_".$authorid."");
    $biographie_ou_temoignage = get_field("biographie_ou_temoignage",  "user_".$authorid."");
    $certifications = get_field("certifications",  "user_".$authorid."");
    $mes_specialites = get_field("mes_specialites",  "user_".$authorid."");
    $mes_destinations_preferees = get_field("mes_destinations_preferees",  "user_".$authorid."");
    $lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");

    $photos = get_field("photos",  "user_".$authorid."");
    $videos = get_field("videos",  "user_".$authorid."");

    /*echo "<strong>Photo : </strong>".$photo["url"]."<br />";
    echo "<strong>Titre : </strong>".$titre."<br />";
    echo "<strong>Téléphone : </strong>".$telephone."<br />";
    echo "<strong>Courriel : </strong>".$courriel."<br />";
    echo "<strong>Bio : </strong>".$biographie_ou_temoignage."<br />";
    echo "<strong>Certifications : </strong>".$certifications."<br />";
    echo "<strong>Spécialités : </strong>".$mes_specialites."<br />";
    echo "<strong>Destinations : </strong>".$mes_destinations_preferees."<br />";
    echo "<strong>Lien vers profil : </strong>".$lien_vers_la_page_de_profil."<br />";*/
    ?>
    <?php if ($banniere_du_profil) { 

        // vars
        $url = $banniere_du_profil['url'];
        $title = $banniere_du_profil['title'];
        $alt = $banniere_du_profil['alt'];
        $caption = $banniere_du_profil['caption'];

        // thumbnail
        $size = 'banner-profil';
        $thumb = $banniere_du_profil['sizes'][ $size ];
        $width = $banniere_du_profil['sizes'][ $size . '-width' ];
        $height = $banniere_du_profil['sizes'][ $size . '-height' ];

        ?>
    <section class="gray nopadding">  
        <img src="<?php echo $thumb; ?>" />
    </section>
    <section class="blue bandeau nopadding">
        &nbsp;
    </section>
    <?php } ?>  

	<section class="gray offsetequipe  <?php if (!$banniere_du_profil) {echo "nooffset";} ?> ">
		<div class="container">
			<div class="row">

                <div class="col-sm-4 col-md-3 side">



                    <img src="<?php echo $photo["url"]; ?>" alt="<?php echo $photo["alt"]; ?>" width="100%">

                    <h2 class="visible-xs text-center"><?php the_title(); ?></h2>
                    <h3 class="visible-xs text-center"><?php echo $titre; ?></h3>

                    <?php if($courriel || $telephone): ?>
                        <div class="fakewell hidden-xs">
                            <?php if($courriel): ?>
                                <h4>Adresse courriel</h4>
                                <p><a calss="email" href="mailto:<?php echo $courriel; ?>"><?php echo $courriel; ?></a></p>
                            <?php endif; ?>

                            <?php if($telephone): ?>
                                <h4>Téléphone</h4>
                                <p><?php echo $telephone; ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if($certifications): ?>
                        <div class="fakewell hidden-xs">
                            <h4>Certifications</h4>
                            <p><?php echo $certifications; ?></p>
                        </div>
                    <?php endif; ?>
                    <?php if(get_usernumposts( $authorid )): ?>
                        <div class="fakewell hidden-xs">
                            <h4>Mon blogue</h4>
                            <p><a href="<?php echo get_author_posts_url( $authorid ); ?>">Consulter le blogue</a></p>
                        </div>
                    <?php endif; ?>

                    <script type="text/javascript">var switchTo5x=true;</script>
                    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
                    <script type="text/javascript">stLight.options({publisher: "a9241dc9-5ad3-4372-b9b2-6a60d6d656d2", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
                    
                    <div class="fakewell hidden-xs">
                        <h4>Partager</h3>
                        <p>
                            <span class='st_email_large' displayText='Email'></span>
                            <span class='st_facebook_large' displayText='Facebook'></span>
                            <span class='st_pinterest_large' displayText='Print'></span>
                            <span class='st_sharethis_large' displayText='ShareThis'></span>
                            <span class='st_print_large' displayText='Print'></span>
                        </p>
                    </div>
                    
                    <div class="fakewell visible-xs">
                        <div class="row">
                            <?php if($certifications){ ?>
                                <div class="col-xs-6">
                            <?php }else{ ?>
                                <div class="col-xs-12">
                            <?php } ?>
                                <?php if($courriel): ?>
                                    <h4>Adresse courriel</h4>
                                    <p><a calss="email" href="mailto:<?php echo $courriel; ?>"><?php echo $courriel; ?></a></p>
                                <?php endif; ?>
                                <?php if($telephone): ?>
                                    <h4>Téléphone</h4>
                                    <p><?php echo $telephone; ?></p>
                                <?php endif; ?>
                                <?php if(get_usernumposts( $authorid )): ?>
                                    <h4>Mon blogue</h4>
                                    <p><a href="<?php echo get_author_posts_url( $authorid ); ?>">Consulter le blogue</a></p>
                                <?php endif; ?>
                                <h4>Partager</h3>
                                <p>
                                    <span class='st_email_large' displayText='Email'></span>
                                    <span class='st_facebook_large' displayText='Facebook'></span>
                                    <span class='st_pinterest_large' displayText='Print'></span>
                                    <span class='st_sharethis_large' displayText='ShareThis'></span>
                                    <span class='st_print_large' displayText='Print'></span>
                                </p>
                            </div>
                            <?php if($certifications): ?>
                                <div class="col-xs-6">
                                    <h4>Certifications</h4>
                                    <p><?php echo $certifications; ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>

				<div class="col-sm-8 col-md-9 cont">
					<h1 class="hidden-xs"><?php the_title(); ?></h1>
					<h2 class="hidden-xs"><?php echo $titre; ?></h2>
                    <?php if($biographie_ou_temoignage): ?>
                        <?php /*<h3>Pour mieux me connaître</h3>*/ ?>
                        <blockquote><?php echo $biographie_ou_temoignage; ?></blockquote>
                        <hr>
                    <?php endif; ?>
                    <?php if($mes_specialites): ?>
                        <div class="specs">
                            <h3>Passionné(e) par</h3>
                            <?php 
                                foreach ($mes_specialites as $s) {
                                    $term = get_term( $s, 'theme' );
                                    echo '<span class="badge">'. $term->name .'</span> '; 
                                }

                                /*$specArray = explode(",", $mes_specialites);
                                foreach ($specArray as &$value) {
                                    echo '<span class="badge">'.$value.'</span> ';
                                }*/
                            ?>
                        </div>
                        <hr>
                    <?php endif; ?>

                    <?php if($mes_destinations_preferees): ?>
                        <div class="dest">
                            <h3>Mes destinations</h3>
                            <?php 
                                foreach ($mes_destinations_preferees as $d) {
                                    $term = get_term( $d, 'pays' );
                                    echo '<span class="badge">'. $term->name .'</span> '; 
                                }

                                /*$destArray = explode(",", $mes_destinations_preferees);
                                foreach ($destArray as &$value) {
                                    echo '<span class="badge">'.$value.'</span> ';
                                }*/
                            ?>
                        </div>
                        <hr>
                    <?php endif; ?>

                    
                    
                    <?php if(get_field("photos",  "user_".$authorid."")): ?>
                        <div class="galerie-photo">
                            <h3>Mes photos</h3>
                            <div class="row">
                            <?php while( has_sub_field('photos',  "user_".$authorid."") ): ?>
                                <?php $image = null;
                                    $image = get_sub_field('image');
                                    $origial = $image['original_image']['url'];
                                    if($image): ?>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="<?php echo $origial; ?>" rel="prettyPhoto[Galery]">
                                            <div class="overlay-photo">
                                                <div class="overlay-table">
                                                    <div class="overlay-cell">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/loupe.png" />
                                                    </div>
                                                </div>
                                            </div>
                                            <img src="<?php echo $image['url']; ?>" class="img-responsive" />
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>

                            <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(get_field("video",  "user_".$authorid."")): ?>
                        <div class="galerie-video">
                            <h3>Mes vidéos</h3>
                            <div class="row">
                            <?php while( has_sub_field('video',  "user_".$authorid."") ): ?>
                                <?php
                                    $url = get_sub_field('url');
                                    if($url){
                                        if (strpos($url,'www.youtube.com/watch?v=') !== false) {
                                            $thumb = $url;
                                            if(strcmp(substr($thumb, 0, 8), 'https://')){
                                                $thumb = substr($thumb, 8);
                                            }
                                            elseif(strcmp(substr($thumb, 0, 7), 'http://')){
                                                $thumb = substr($thumb, 7);
                                            }
                                            $thumb = substr($thumb, 25);
                                            $thumb = 'http://img.youtube.com/vi/' . $thumb . '/hqdefault.jpg';
                                    ?>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="<?php the_sub_field('url'); ?>" rel="prettyPhoto">
                                            <div class="overlay-video">
                                                <div class="overlay-table">
                                                    <div class="overlay-cell">
                                                        <div class="img-container">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/images/play-fiche-voyage.png" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <img src="<?php echo $thumb; ?>" class="img-responsive equipe-video-thumb" />
                                        </a>
                                    </div>
                                <?php } } ?>
                            <?php endwhile; ?>
                        </div>  
                        </div>
                        <?php endif; ?>

				</div>

			</div>
		</div>
	</section>


<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>