<?php
/*
Template Name: Infolettre
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
<?php
	$urlImage = "";
	$image = get_field('image-banner');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<section class="blue" <?php if(!empty($image)){ ?>style="<?php echo $urlImage; ?>" <?php } ?>>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>


<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			<div class="col-sm-12">
				<div class="main-contact">
					<?php the_content(); ?>
				</div>

				<div class="contact-form">

					<form name="infolettre" method="post" action="/cakemail-2.php" onsubmit="return validaterInfolettre()">
						<?php if(ICL_LANGUAGE_CODE == 'fr'): ?>
						<p>
							<label id="courriel-label" for="courriel">Courriel * :</label><br/>
							<input type="text" name="courriel" id="courriel" />
						</p>
	
						<p>
							<label for="prenom">Prénom :</label><br/>
							<input type="text" name="prenom" id="prenom" />
						</p>
	
						<p>
							<label for="nom">Nom :</label><br/>
							<input type="text" name="nom" id="nom" />
						</p>
	
						<p>
							<label>De quelle(s) divisions(s) aimeriez-vous recevoir de l'information ? <br/>
							Veuillez cochez vos choix.</label>
						</p>
	
						<p>
							<input type="checkbox" value="Corpo" name="groupe[]" id="Corpo">
							<label class="radio-label" for="Corpo">Voyages d’affaires</label><br/>
							<input type="checkbox" value="Culturel" name="groupe[]" id="Culturel">
							<label class="radio-label" for="Culturel">Voyages culturels</label><br/>
							<input type="checkbox" value="Exception" name="groupe[]" id="Exception">
							<label class="radio-label" for="Exception">Voyages d'exception</label><br/>
							<input type="checkbox" value="Croisiere" name="groupe[]" id="Croisiere">
							<label class="radio-label" for="Croisiere">Croisières</label><br/>
							<input type="checkbox" value="DepartGroupe" name="groupe[]" id="DepartGroupe">
							<label class="radio-label" for="DepartGroupe">Départs de groupes</label>
						</p>

						<p>
							<input type="submit" value="Soumettre" id="subm-btn" class="btn btn-contact">
						</p>

						<p>
							<span style="display:block; color:red; visibility:hidden;" id="error-span">Veuillez corrigez les champs requis afin de soumettre le formulaire</span>
						</p>
						
						<?php else: ?>
						<p>
							<label id="courriel-label" for="courriel">Email * :</label><br/>
							<input type="text" name="courriel" id="courriel" />
						</p>
	
						<p>
							<label for="prenom">First name :</label><br/>
							<input type="text" name="prenom" id="prenom" />
						</p>
	
						<p>
							<label for="nom">Last name :</label><br/>
							<input type="text" name="nom" id="nom" />
						</p>
	
						<p>
							<label>Which Division (s) would you like to receive information about ? <br/>
							Please check your choices.</label>
						</p>
	
						<p>
							<input type="checkbox" value="Corpo" name="groupe[]" id="Corpo">
							<label class="radio-label" for="Corpo">Business trips</label><br/>
							<input type="checkbox" value="Culturel" name="groupe[]" id="Culturel">
							<label class="radio-label" for="Culturel">Cultural trips</label><br/>
							<input type="checkbox" value="Exception" name="groupe[]" id="Exception">
							<label class="radio-label" for="Exception">Exceptional trips</label><br/>
							<input type="checkbox" value="Croisiere" name="groupe[]" id="Croisiere">
							<label class="radio-label" for="Croisiere">Cruises</label><br/>
							<input type="checkbox" value="DepartGroupe" name="groupe[]" id="DepartGroupe">
							<label class="radio-label" for="DepartGroupe">Group Departures</label>
						</p>

						<p>
							<input type="submit" value="Submit" id="subm-btn" class="btn btn-contact">
						</p>

						<p>
							<span style="display:block; color:red; visibility:hidden;" id="error-span">Please correct the required fields to submit the form</span>
						</p>
						<?php endif; ?>

					</form>

				</div>

			</div>
		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>