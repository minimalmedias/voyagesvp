<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>



<?php
	$urlImage = "";
	$image = get_field('image_banniere');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<div class="banniere-voyage-daffaire" style="<?php echo $urlImage; ?>">
	<div class="container">
		<?php the_field('texte_banniere'); ?>
	</div>
</div>


<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			<div class="col-sm-6">
				<?php if(get_field('formulaire')): ?>
					<div class="contact-form">
						<?php the_field('formulaire'); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="col-sm-6">
				<div class="main-contact">
					<?php the_content(); ?>
				</div>

				<?php if(get_field('map')): ?>
				<div class="map-contact">
					<?php the_field('map'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>