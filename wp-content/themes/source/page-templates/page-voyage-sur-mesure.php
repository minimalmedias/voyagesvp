<?php
/*
Template Name: Voyages sur mesure
*/
?>

<?php get_header(); ?>

<?php
	$url = $_SERVER['REQUEST_URI'];
	$tokens = explode('/', $url);
	
	$slug = $tokens[sizeof($tokens)-2];
	//echo $pays;
	//$theme = $tokens[sizeof($tokens)-2];
	
	$type = get_field('type_de_voyage');
	/*$theme = $_GET["theme"];
	$pays = $_GET["destination"];
	$compagnie = $_GET["compagnie"];*/

	$pays = '';
	$theme = '';
	$compagnie = '';

	if( term_exists( $slug, 'pays' ) ){
		$pays = $slug;
	}
	else if( term_exists( $slug, 'theme' ) ){
		$theme = $slug;
	}
	else if( term_exists( $slug, 'compagnie' ) ){
		$compagnie = $slug;
	}

	$permalink = get_permalink( $post->ID );

	$croisiere_id = 787;

	$permalink_theme_1 = get_permalink( $post->ID );
	$permalink_theme_2 = '';

	$permalink_pays_1 = get_permalink( $post->ID ) . '?';
	$permalink_pays_2 = '';

	$permalink_compagnie = get_permalink( $post->ID ) . '?';

	if($pays){
		$permalink_theme_2 .= '&destination=' . $pays;
		$permalink_compagnie .= 'destination=' . $pays . '&';
		$pays_term = get_term_by( 'slug', $pays, 'pays' );
		$pays_nom = $pays_term->name;
	}
	if($theme){
		$permalink_pays_1 .= 'theme=' . $theme . '&';
		$permalink_compagnie .= 'theme=' . $theme . '&';
		$theme_term = get_term_by( 'slug', $theme, 'theme' );
		$theme_nom = $theme_term->name;
	}
	if($compagnie){
		$permalink_theme_2 .= '&compagnie=' . $compagnie;
		$permalink_pays_2 .= '&compagnie=' . $compagnie;
		$compagnie_term = get_term_by( 'slug', $compagnie, 'compagnie' );
		$compagnie_nom = $compagnie_term->name;
	}

?>

<?php
	$urlImage = "";
	$image = get_field('image_banniere', $post->ID);
	/*if($pays){
		if( get_field( 'image_destination', get_term_by( 'slug', $pays, 'pays' ) ) ){
			$image = get_field( 'image_destination', get_term_by( 'slug', $pays, 'pays' ) );
		}
	}
	if($theme){
		if( get_field( 'image_destination', get_term_by( 'slug', $theme, 'theme' ) ) ){
			$image = get_field( 'image_destination', get_term_by( 'slug', $theme, 'theme' ) );
		}
	}
	if($compagnie){
		if( get_field( 'image_destination', get_term_by( 'slug', $compagnie, 'compagnie' ) ) ){
			$image = get_field( 'image_destination', get_term_by( 'slug', $compagnie, 'compagnie' ) );
		}
	}*/
	if($image){
		$thumb = $image['sizes']['banner-sur-mesure'];
		$urlImage = "background-image: url(" . $thumb . ");";
	}
?>
<div class="banniere-voyage-surmesure" style="<?php echo $urlImage; ?>">
	<div class="container main-container">
		<div class="overlay-table">
			<div class="overlay-cell">
				<h1><?php if (get_field("texte_banniere")) { the_field('texte_banniere'); } else {the_title();} ?>
					<?php 
					if($theme_nom || $pays_nom || $compagnie_nom){
						echo '<br/>';
					}
					if($theme_nom){
						echo $theme_nom;
					}
					if($theme_nom && $pays_nom){
						echo ' - ';
					}
					if($pays_nom){
						echo $pays_nom;
					}
					if( $compagnie_nom ){
						if($theme_nom && $pays_nom){
							echo ' - ';
						}
						echo $compagnie_nom;
					}
					?>
				</h1>
			</div>
		</div>
	</div>
	<?php if (get_field("titre_de_gauche")) { ?>
	<div class="overlay">
		<div class="overlay-blur" style="<?php echo $urlImage; ?>"></div>
		<div class="overlay-top">
			<div class="container">
				<div class="row">
					<?php if (get_field("titre_de_gauche")) { ?>
					<div class="col-sm-6 boite">
						<h2><?php the_field("titre_de_gauche"); ?></h2>
					</div>
					<?php } ?>
					<div class="col-sm-6 boite">
						<?php if (get_field("bouton_lien")) { ?><a href="<?php the_field("bouton_lien"); ?>" class="btn btn-rightarrow"><?php the_field("bouton_texte"); ?></a><?php } ?>
						<?php if (get_field("lien_lien")) { ?><a href="<?php the_field("lien_lien"); ?>"><?php the_field("lien_texte"); ?></a><?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</div>


<section class="gray reenie-title">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center">Nos coups de coeur</h2>
			</div>
		</div>
	</div>
</section>

<section class="gray notoppadding">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<?php if($type == $croisiere_id): ?>
				<div class="menu-thematiques">
					<h3>Compagnies de croisières</h3>

					<?php

					$taxonomy_array = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'type',
								'field'    => 'term_id',
								'terms'    => $type,
							),);
					/*if($pays && !$theme){
						array_push($taxonomy_array, array(
								'taxonomy' => 'pays',
								'field'    => 'slug',
								'terms'    => $pays,
							)
						);
					}*/
					$posts_array = get_posts(
					    array(
					        'posts_per_page' => -1,
					        'post_type' => 'voyages',
					        'tax_query' => $taxonomy_array,
					    )
					);

					$terms = array();
					$terms_slug = array();

					foreach ($posts_array as $post) {
						$term_list = wp_get_post_terms( $post->ID, 'compagnie' );
						foreach ($term_list as $term ) {
							array_push($terms, $term->name);
							$terms_slug[$term->name] = $term->slug;
						}
					}

					$terms = array_unique($terms);
					asort($terms);

					/*$taxonomy = 'theme';
					$terms = get_terms( $taxonomy, '' );*/

					$size = count($terms);
					$i = 0;
					$nofollow = '';
					if($compagnie){
						$nofollow = 'rel="nofollow"';
					}
					echo '<ul>';
					if ($terms) { ?>
						<li <?php if(!$compagnie){ echo 'class="active"'; } ?> >
							<a href="<?php echo $permalink; ?>"><?php _e('Toutes les compagnies'); ?></a>
						</li>
						<?php foreach($terms as $term) {
							$name = $term;
							$slug = $terms_slug[$name];
							$i++;
							$class = '';
							if($compagnie == $slug){
								$class .= 'active ';
							}
							if($i == $size){
								$class .= 'last';
							}
							$echo = '<li';
							if($class){
								$echo .= ' class="'. $class .'" ';
							}
							$echo .= '><a href="' . $permalink . $slug . '/" ' . $nofollow . '>';
							$echo .= $name;
							$echo .= '</a></li>';
							echo $echo;
						}
					}
					echo '</ul>';
					?>


					<?php /* <ul>
						<li><a href="">Circuits</a></li>
						<li><a href="">Groupe de ski</a></li>
						<li><a href="">Villas, Hôtels, Îles et châteaux</a></li>
						<li class="active"><a href="">Longs séjours</a></li>
						<li><a href="">Mariage</a></li>
						<li><a href="">Gastronomie</a></li>
						<li><a href="">Aventures</a></li>
						<li class="last"><a href="">Cultures & arts</a></li>
					</ul> */ ?>
				</div>
				<?php endif; ?>


				<div class="menu-thematiques">
					<h3>Thématiques</h3>

					<?php

					$taxonomy_array = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'type',
								'field'    => 'term_id',
								'terms'    => $type,
							),);
					/*if($pays && !$theme){
						array_push($taxonomy_array, array(
								'taxonomy' => 'pays',
								'field'    => 'slug',
								'terms'    => $pays,
							)
						);
					}*/
					$posts_array = get_posts(
					    array(
					        'posts_per_page' => -1,
					        'post_type' => 'voyages',
					        'tax_query' => $taxonomy_array,
					    )
					);

					$terms = array();
					$terms_slug = array();

					foreach ($posts_array as $post) {
						$term_list = wp_get_post_terms( $post->ID, 'theme' );
						foreach ($term_list as $term ) {
							array_push($terms, $term->name);
							$terms_slug[$term->name] = $term->slug;
						}
					}

					$terms = array_unique($terms);
					asort($terms);

					/*$taxonomy = 'theme';
					$terms = get_terms( $taxonomy, '' );*/

					$size = count($terms);
					$i = 0;
					$nofollow = '';
					if($theme){
						$nofollow = 'rel="nofollow"';
					}
					echo '<ul>';
					if ($terms) { ?>
						<li <?php if(!$theme){ echo 'class="active"'; } ?> >
							<a href="<?php echo $permalink; ?>"><?php _e('Toutes les thématiques'); ?></a>
						</li>
						<?php foreach($terms as $term) {

							$name = $term;
							$slug = $terms_slug[$name];

							$i++;
							$class = '';
							if($theme == $slug){
								$class .= 'active ';
							}
							if($i == $size){
								$class .= 'last';
							}
							$echo = '<li';
							if($class){
								$echo .= ' class="'. $class .'" ';
							}
							$echo .= '><a href="' . $permalink . $slug . '/" ' . $nofollow . '>';
							$echo .= $name;
							$echo .= '</a></li>';
							echo $echo;
						}
					}
					echo '</ul>';
					?>


					<?php /* <ul>
						<li><a href="">Circuits</a></li>
						<li><a href="">Groupe de ski</a></li>
						<li><a href="">Villas, Hôtels, Îles et châteaux</a></li>
						<li class="active"><a href="">Longs séjours</a></li>
						<li><a href="">Mariage</a></li>
						<li><a href="">Gastronomie</a></li>
						<li><a href="">Aventures</a></li>
						<li class="last"><a href="">Cultures & arts</a></li>
					</ul> */ ?>
				</div>

				<div class="menu-thematiques">
					<h3>Destinations</h3>

					<ul>
						<?php

						$taxonomy_array = array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'type',
									'field'    => 'term_id',
									'terms'    => $type,
								),);
						/*if($theme && !$pays){
							array_push($taxonomy_array, array(
									'taxonomy' => 'theme',
									'field'    => 'slug',
									'terms'    => $theme,
								)
							);
						}*/
						$posts_array = get_posts(
						    array(
						        'posts_per_page' => -1,
						        'post_type' => 'voyages',
						        'tax_query' => $taxonomy_array,
						    )
						);
						
						$terms = array();
						
						foreach ($posts_array as $post) {
							$term_list = wp_get_post_terms( $post->ID, 'pays' );
							foreach ($term_list as $term ) {
								$terms[$term->term_id] = true;
							}
						}


						$taxonomy_name = 'pays';
						$term_id_list = array();
						$term_name_list = array();
						$terms_pays = get_terms( $taxonomy_name );
						foreach( $terms_pays as $term_pays ){
							//echo '<span>' . $term_pays->parent . '</span><br/>';
							//$parent = get_term($term->parent, $taxonomy_name );
							if( $term_pays->parent == 0 ){
								if( $terms[$term_pays->term_id] ){
									$term_id_list[$term_pays->name] = $term_pays->term_id;
									array_push($term_name_list, $term_pays->name);
								}
							}
						}

						$term_name_list = array_unique($term_name_list);
						asort($term_name_list);

						//$term_id_list = array(778, 779, 780, 781, 782);
						$i = 0;
						$size = count($term_name_list);
						$nofollow = '';
						if($pays){
							$nofollow = 'rel="nofollow"';
						} ?>

						<li <?php if(!$pays){ echo 'class="active"'; } ?> >
							<a href="<?php echo $permalink; ?>"><?php _e('Toutes les destinations'); ?></a>
						</li>

						<?php foreach ($term_name_list as $term_name):
							$i++;
							$term_id = $term_id_list[$term_name];
							$termchildren = get_term_children( $term_id, $taxonomy_name );
						?>
						<li class="dropdown">
							<?php
								$isDropDown = false;
								foreach ( $termchildren as $child ) {
									$term = get_term_by( 'id', $child, $taxonomy_name );
									if( $terms[$child] ){
										$isDropDown = true;
										break;
									}
								}
							?>
							<?php if( $isDropDown ){ ?>
								<p class="dropdown-open"><?php echo get_term( $term_id, $taxonomy_name )->name; ?></p>
							<?php }else{ ?>
								<?php
									$termd = get_term( $term_id, $taxonomy_name );
									$class = '';
									if($pays == $termd->slug){
										$class = ' class="active"';
									}
								?>
								<a<?php echo $class; ?> href="<?php echo $permalink . $termd->slug . '/'; ?>"><?php echo $termd->name; ?></a>
							<?php } ?>
							<?php
								if( $isDropDown ){
									echo '<ul class="sub-menu">';
									$term = get_term_by( 'id', $term_id, $taxonomy_name );
									$class = '';
									if($pays == $term->slug){
										$class = ' class="active"';
									}
									echo '<li'. $class .'><a href="'. $permalink . $term->slug .'/">'. 'Tout' .'</a></li>';
									foreach ( $termchildren as $child ) {
										$term = get_term_by( 'id', $child, $taxonomy_name );
										if( $terms[$child] ){
											$class = '';
											if($pays == $term->slug){
												$class = ' class="active"';
											}
											echo '<li'. $class .'><a href="' . $permalink . $term->slug . '/" ' . $nofollow . '>' . $term->name . '</a></li>';
										}
									}
									echo '</ul>';
								}
							?>
						</li>
						<?php endforeach; ?>
					</ul>

					<?php /* <ul>
						<li class=""><a href="">Afrique</a></li>
						<li class="dropdown"><a href="">Amérique du Nord</a>
							<ul class="sub-menu">
								<li><a href="">Argentine</a></li>
								<li><a href="">Galapagos</a></li>
								<li><a href="">Pérou</a></li>
								<li><a href="">Équateur</a></li>
							</ul>
						</li>
						<li class="dropdown"><a href="">Amérique du Sud</a>
							<ul class="sub-menu">
								<li><a href="">Argentine</a></li>
								<li><a href="">Galapagos</a></li>
								<li><a href="">Pérou</a></li>
								<li><a href="">Équateur</a></li>
							</ul>
						</li>
						<li class=""><a href="">Asie</a></li>
						<li class="last"><a href="">Europe</a></li>
					</ul> */ ?>
				</div>

			</div>

			<div class="col-sm-9">

				<?php 

				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$taxonomy_array = array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'type',
							'field'    => 'term_id',
							'terms'    => $type,
						),);
				if($theme){
					array_push($taxonomy_array, array(
							'taxonomy' => 'theme',
							'field'    => 'slug',
							'terms'    => $theme,
						)
					);
				}
				if($pays){
					array_push($taxonomy_array, array(
							'taxonomy' => 'pays',
							'field'    => 'slug',
							'terms'    => $pays,
						)
					);
				}
				if($compagnie){
					array_push($taxonomy_array, array(
							'taxonomy' => 'compagnie',
							'field'    => 'slug',
							'terms'    => $compagnie,
						)
					);
				}
				$args = array(
					'post_type' => 'voyages',
					'showposts'=> 9999,
					'paged' => $paged,
					'tax_query' => $taxonomy_array,
				);
				
				$wp_query = new WP_Query( $args );

				if ( $wp_query->have_posts() ){
					while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<div class="resultat">
							<?php $img = get_the_post_thumbnail( null, 'thumb-voyage' ); ?>
							<div class="<?php if(!$img){ echo "no-resultat-image"; } ?> resultat-content">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php if(get_field("elabore_par")){ ?>

									<?php
										$author = get_field("elabore_par");
										$authorid = $author["ID"];
										$photo = get_field("photo", "user_".$authorid."");
										$nom_complet = get_field("nom_complet",  "user_".$authorid."");
										$titre = get_field("titre",  "user_".$authorid."");
										$lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");
									?>
									<div class="media">
										<div class="media-left resultat-author">
											<a href="<?php echo $lien_vers_la_page_de_profil; ?>">
												<img class="media-object resultat-author-image" src="<?php echo $photo["url"]; ?>" alt="<?php echo $photo["alt"]; ?>">
												<img class="favorite" src="<?php echo get_template_directory_uri(); ?>/images/favorite.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<strong>Élaboré par : <?php echo $nom_complet; ?></strong><br />
											<?php echo $titre; ?>
										</div>
									</div>

								<?php }else{ ?>

									<?php
										$theme_post = wp_get_post_terms( $post->ID, 'theme' );
										$pays_post = wp_get_post_terms( $post->ID, 'pays' );
										if( $type == $croisiere_id ){
											$compagnie_post = wp_get_post_terms( $post->ID, 'compagnie' );
										}
										$duree = get_field('duree', $post->ID);
										if($theme_post || $pays_post || $duree){ ?>
										<p>
											<?php 
												if($compagnie_post): ?>
													<?php foreach($compagnie_post as $c): ?>
														<a href="<?php echo $permalink . $c->slug . '/'; ?>"><?php echo $c->name; ?></a>
													<?php endforeach; ?>
													<?php if($theme_post || $pays_post): ?>
														<span> - </span>
													<?php endif; ?>
											<?php endif; ?>
											<?php 
												if($theme_post): ?>
													<?php foreach($theme_post as $t): ?>
														<a href="<?php echo $permalink . $t->slug . '/'; ?>"><?php echo $t->name; ?></a>
													<?php endforeach; ?>
											<?php endif; ?>
											<?php if($theme_post && $pays_post): ?>
												<span> - </span>
											<?php endif; ?>
											<?php
												if($pays_post): ?>
													<?php foreach($pays_post as $p): ?>
														<a href="<?php echo $permalink . $p->slug . '/'; ?>"><?php echo $p->name; ?></a>
													<?php endforeach; ?>
											<?php endif; ?>
											<?php if($duree):
												if($theme_post || $pays_post || $compagnie_post){
													echo '<span> - </span>';
												}
												echo $duree;
											endif; ?>
										</p>
									<?php } ?>

								<?php } ?>
								<?php the_excerpt(); ?>
							</div>
							<?php 
								if($img):
								//$thumb = $img['sizes']['thumb-voyage']; ?>
								<div class="<?php /*col-sm-6*/ ?> nopadding text-right resultat-image hidden-xs">
									<?php echo $img; ?>
								</div>
								<div class="clearfix"></div>
							<?php endif; ?>
						</div>

					<?php endwhile; ?>

					<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="col-sm-12 <?php echo $nav_class; ?> text-center">
		
						<div class="col-xs-6 text-right">
							<?php if (get_previous_posts_link()) : ?>
							<div class="nav-previous add-rel-prev"><?php previous_posts_link( __( '<span class="post-nav glyphicon glyphicon-chevron-left"></span>', 'upbootwp' ) ); ?></div>
							<?php endif; ?>
						</div>
						<div class="col-xs-6 text-left">
							<?php if (get_next_posts_link()) : ?>
							<div class="nav-next add-rel-next"><?php next_posts_link( __( '<span class="post-nav glyphicon glyphicon-chevron-right"></span>', 'upbootwp' ) ); ?></div>
							<?php endif; ?>
						</div>

					</nav>

				<?php }else{ ?>

					<p><?php _e("Aucun voyage n'a été trouvé avec vos critères. Essayez une différente destination et/ou thématique."); ?></p>

				<?php } ?>


				<?php /* <div class="resultat">
					<div class="row">
						<div class="col-sm-6 resultat-content">
							<h3><a href="/voyages/cicruit-chili-argentine/">La gastronomie en Italie</a></h3>
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object" src="<?php echo get_template_directory_uri(); ?>/images/cdc1.png" alt="...">
									</a>
								</div>
								<div class="media-body">
									<strong>Élaboré par : Isabelle Provost</strong><br />
									Spécialiste gastronomie
								</div>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
						</div>
						<div class="col-sm-6 nopadding text-right resultat-image hidden-xs">
							<img src="<?php echo get_template_directory_uri(); ?>/images/resultat1.jpg">
						</div>
					</div>
				</div>

				<div class="resultat">
					<div class="row">
						<div class="col-sm-6 resultat-content">
							<h3><a href="/voyages/cicruit-chili-argentine/">Mariage romantique en Italie</a></h3>
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object" src="<?php echo get_template_directory_uri(); ?>/images/cdc2.png" alt="...">
									</a>
								</div>
								<div class="media-body">
									<strong>Élaboré par : Sylvain Marcoux</strong><br />
									Spécialiste Italie, Mariage
								</div>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
						</div>
						<div class="col-sm-6 nopadding text-right resultat-image hidden-xs">
							<img src="<?php echo get_template_directory_uri(); ?>/images/resultat2.jpg">
						</div>
					</div>
				</div>

				<div class="resultat">
					<div class="row">
						<div class="col-sm-6 resultat-content">
							<h3><a href="/voyages/cicruit-chili-argentine/">La gastronomie en Italie</a></h3>
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object" src="<?php echo get_template_directory_uri(); ?>/images/cdc1.png" alt="...">
									</a>
								</div>
								<div class="media-body">
									<strong>Élaboré par : Isabelle Provost</strong><br />
									Spécialiste gastronomie
								</div>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
						</div>
						<div class="col-sm-6 nopadding text-right resultat-image hidden-xs">
							<img src="<?php echo get_template_directory_uri(); ?>/images/resultat1.jpg">
						</div>
					</div>
				</div>

				<div class="resultat">
					<div class="row">
						<div class="col-sm-6 resultat-content">
							<h3><a href="">La gastronomie en Italie</a></h3>
							<p>Long séjour - Argentine - 10 jours </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
							<p><a href="">Voir les dates de départ</a></p>
						</div>
						<div class="col-sm-6 nopadding text-right resultat-image hidden-xs">
							<img src="<?php echo get_template_directory_uri(); ?>/images/resultat1.jpg">
						</div>
					</div>
				</div> */ ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
