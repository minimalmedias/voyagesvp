<?php
/*
Template Name: Témoignages
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>



<?php
	$urlImage = "";
	$image = get_field('image_banniere');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<div class="banniere-voyage-daffaire" style="<?php echo $urlImage; ?>">
	<div class="container">
		<?php the_field('texte_banniere'); ?>
	</div>
</div>


<div class="bloc-principal-temoignage">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>

				<?php if(get_field('temoignage')): ?>
					<div class="liste-temoignages">
						<div class="row">
							<div class="col-md-4 selecteur-temoignage">
								<ul>
									<?php $i = 0; ?>
									<?php while( has_sub_field('categorie') ): ?>
										<?php $i++; ?>
										<?php if($i == 1){ ?>
											<li class="active">
										<?php }else{ ?>
											<li>
										<?php } ?>
											<a href="#temoignage<?php echo $i; ?>" aria-controls="temoignage<?php echo $i; ?>" role="tab" data-toggle="tab">
												<?php the_sub_field('titre'); ?>
											</a>
										</li>
									<?php endwhile; ?>
								</ul>
							</div>
							<div class="col-md-8">
								<div class="tab-content">
									<?php $i = 0; ?>
									<?php while( has_sub_field('categorie') ): ?>
										<?php $i++; ?>
										<?php if($i == 1){ ?>
											<div role="tabpanel" class="tab-pane fade active in" id="temoignage<?php echo $i; ?>">
										<?php }else{ ?>
											<div role="tabpanel" class="tab-pane fade" id="temoignage<?php echo $i; ?>">
										<?php } ?>
											
											<?php while( has_sub_field('liste_temoignages') ): ?>
												<div class="boite-page-temoignages">
													<?php
														$url = get_sub_field('video');
														if($url){
															if (strpos($url,'www.youtube.com/watch?v=') !== false) {
																$thumb = $url;
																if(strcmp(substr($thumb, 0, 8), 'https://')){
																	$thumb = substr($thumb, 8);
																}
																elseif(strcmp(substr($thumb, 0, 7), 'http://')){
																	$thumb = substr($thumb, 7);
																}
																$thumb = substr($thumb, 25);
																$thumb = 'http://img.youtube.com/vi/' . $thumb . '/hqdefault.jpg';
																$noVideo = "";
														?>
														<div class="img-container">
															<a href="<?php the_sub_field('video'); ?>" rel="prettyPhoto" title="<?php the_sub_field('nom'); ?>">
																<div class="prettyPhoto">
																	<img src="<?php echo $thumb; ?>" class="img-responsive video-thumbnail equipe-video-thumb" />
																	<div class="btn btn-play">
																		<img src="<?php echo get_template_directory_uri(); ?>/images/play.png" alt="play" />
																	</div>
																</div>
															</a>
														</div>
													<?php }else{$noVideo = "no-video";} }else{$noVideo = "no-video";} ?>
													<div class="info-temoignage <?php echo $noVideo; ?>">
														<?php if(get_sub_field('citation')): ?>
															<?php the_sub_field('citation'); ?>
														<?php endif; ?>
														<?php if(get_sub_field('nom')): ?>
															<p class="nom"><?php the_sub_field('nom'); ?></p>
														<?php endif; ?>
														<?php if(get_sub_field('titre_de_la_personne')): ?>
															<p class="titre"><?php the_sub_field('titre_de_la_personne'); ?></p>
														<?php endif; ?>
													</div>
												</div>
											<?php endwhile; ?>
	
										</div>
									<?php endwhile; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</div>

			<div class="col-sm-5 col-md-4 col-md-offset-1">
				<div class="formulaire-container">
					<?php if(get_field('formulaire')): ?>
						<?php the_field('formulaire'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>