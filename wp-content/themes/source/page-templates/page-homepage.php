<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

<?php //the_content(); ?>

<?php if(get_field('titre_h1')): ?>
	<h1 class="display-none"><?php the_field('titre_h1'); ?></h1>
<?php endif; ?>

<?php if( have_rows('banner') ): ?>
<div class="carousel-haut owl-carousel-container">
	<div class="owl-carousel-haut">
		<?php while ( have_rows('banner') ) : the_row(); ?>
		 	<div class="item <?php if($i == 0){echo 'active';}?>">
		    	<!--<img src="<?php echo get_template_directory_uri(); ?>/images/header.jpg" alt="" />-->
		    	<?php $img = get_sub_field('image');?>
		    	<img src="<?php echo $img["url"]; ?>" alt="" />
		    	<?php if(get_sub_field('contenu')){ ?>
			    	<div class="carousel-caption">
						<?php the_sub_field('contenu'); ?>
					</div>
				<?php } ?>
				<?php
					$indicators .= '<li data-target="#carousel-banner" data-slide-to="' . $i . '"';
					if($i == 0){
						$indicators .= ' class="active"';
					}
					$indicators .= '></li>';
				?>
		    </div>
		 	<?php ++$i; ?>
		<?php endwhile; ?>

	</div>
	<?php if($i > 1){ ?>
		<!-- Controls -->
		<!--ol class="carousel-indicators">
			<?php echo $indicators; ?>
		</ol-->
	<?php } ?>
</div>

<?php else : ?>


<?php endif; ?>

<div class="boite-bleue-et-blanche">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="boite bg-bleu">
					<?php if(get_field('titre_bleu')): ?>
						<h2><?php the_field('titre_bleu'); ?></h2>
					<?php endif; ?>
					<?php if(get_field('texte_bleu')): ?>
						<?php the_field('texte_bleu'); ?>
					<?php endif; ?>
					<?php if(get_field('bouton_texte_bleu')): ?>
						<?php if(get_field('bouton_lien_bleu')){ ?>
							<a class="btn" href="<?php the_field('bouton_lien_bleu'); ?>">
						<?php }else{ ?>
							<a class="btn">
						<?php } ?>
							<?php the_field('bouton_texte_bleu'); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="boite bg-blanc">
					<?php if(get_field('titre_blanche')): ?>
						<h2><?php the_field('titre_blanche'); ?></h2>
					<?php endif; ?>
					<?php if(get_field('texte_blanche')): ?>
						<?php the_field('texte_blanche'); ?>
					<?php endif; ?>
					<?php if(get_field('liste_liens_blanche')): ?>
						<div class="liste_liens">
							<?php while ( have_rows('liste_liens_blanche') ) : the_row(); ?>
								<?php if(get_sub_field('lien')){ ?>
									<a href="<?php the_sub_field('lien'); ?>">
								<?php }else{ ?>
									<a>
								<?php } ?>
									<?php $image = get_sub_field('image');
										if($image):
										?>
											<div class="img-container">
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</div>
									<?php endif; ?>
									<?php if(get_sub_field('texte')): ?>
										<?php the_sub_field('texte'); ?>
									<?php endif; ?>
								</a>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="voyage-coup-de-coeur">
	<div class="container titre">
		<?php if(get_field('titre_coup_de_coeur')): ?>
			<h2><?php the_field('titre_coup_de_coeur'); ?></h2>
		<?php endif; ?>
	</div>
	<?php if(get_field('voyage_coup_de_coeur')): ?>
	<div class="container owl-carousel-container">
		<div class="owl-carousel-bas">

			<?php while( has_sub_field('voyage_coup_de_coeur') ): ?>
				<?php $id = get_sub_field('voyage');
				if( $id ):
				?>
					<div class="item">
						<div class="boite-coup-de-coeur">
							<?php
							$post = get_post($id);
							setup_postdata( $post );
							//$img = get_field('banniere', $id);
							$img = get_the_post_thumbnail( $id, 'thumb-carousel-bas' );
							if($img):
							//$thumb = $img['sizes']['thumb-carousel-bas'];
							?>
								<a href="<?php echo get_permalink( $id ); ?>">
									<?php /*<img src="<?php echo $thumb; ?>" alt="<?php echo $img['alt']; ?>" class="img-responsive" />*/ ?>
									<?php echo $img; ?>
								</a>
							<?php endif; ?>
							<div class="info">
								<h2><a href="<?php echo get_permalink( $id ); ?>"><?php the_title(); ?></a></h2>
								<?php the_excerpt(); ?>
								<?php /*<a class="coup-de-coeur-lien" href="<?php echo get_permalink( $id ); ?>">En savoir plus</a> */ ?>
								<?php wp_reset_query(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>

		</div>

		<a class="owl-bas-prev btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-prev.png" alt="prev" /></a>
		<a class="owl-bas-next btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" alt="next" /></a>

	</div>
	<?php endif; ?>
</div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>