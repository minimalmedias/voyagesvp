<?php
/*
Template Name: Voyages d'affaires
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>



<?php
	$urlImage = "";
	$image = get_field('image_banniere');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<div class="banniere-voyage-daffaire" style="<?php echo $urlImage; ?>">
	<div class="container">
		<?php the_field('texte_banniere'); ?>
	</div>
</div>


<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-7">
				<?php the_content(); ?>
			</div>

			<div class="col-sm-5 col-md-4 col-sm-offset-1">
				<div class="formulaire-container position-absolute">
					<?php if(get_field('formulaire')): ?>
						<?php the_field('formulaire'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="bloc-nos-services">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<?php if(get_field('titre_services')): ?>
					<h2><?php the_field('titre_services'); ?></h2>
				<?php endif; ?>
				<?php if(get_field('texte_sevices')): ?>
					<?php the_field('texte_sevices'); ?>
				<?php endif; ?>

				<?php $lien = get_field('lien_services'); ?>

				<?php if(get_field('liste_de_lien_services')): ?>
					<div class="liste_de_lien_services row">
						<?php while ( have_rows('liste_de_lien_services') ) : the_row(); ?>
							<div class="col-sm-6 col-md-5">
								<?php if(get_sub_field('id_du_bloc_services')){ ?>
									<a href="<?php echo $lien; ?>#<?php the_sub_field('id_du_bloc_services'); ?>">
								<?php }elseif($lien){ ?>
									<a href="<?php echo $lien; ?>">
								<?php }else{ ?>
									<a>
								<?php } ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/checked.png" alt="" />
									<?php if(get_sub_field('texte_lien_services')): ?>
										<?php the_sub_field('texte_lien_services'); ?>
									<?php endif; ?>
								</a>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<div class="bloc-equipe">
	<div class="container">
		<div class="row">
			<?php if(!get_field('cacher_le_bloc_temoignages')){
				$col = 'col-sm-6 col-md-7';
			}
			else{
				$col = 'col-sm-12 col-md-7';
			} ?>
			<div class="<?php echo $col; ?>">
				<?php if(get_field('titre_equipe')): ?>
					<h2><?php the_field('titre_equipe'); ?></h2>
				<?php endif; ?>
				<?php if( have_rows('liste_de_membre') ): ?>
				<div class="liste-membres">
					 <?php while( have_rows('liste_de_membre') ): the_row(); 
					
					// get this page's specified author
					$author = get_sub_field("membre");
					// find ID for ACF
					$authorid = $author["ID"];

					// vars
					$photo = get_field("photo", "user_".$authorid."");
					$nom_complet = get_field("nom_complet",  "user_".$authorid."");
					$titre = get_field("titre",  "user_".$authorid."");
					$lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");
					
					?>
						<div class="membre">
							<a data-pageurl="<?php echo $lien_vers_la_page_de_profil; ?>" data-nom="<?php echo $nom_complet; ?>" data-img="<?php echo $photo["url"]; ?>">
								<img src="<?php echo $photo["url"]; ?>" alt="<?php echo $photo["alt"]; ?>" class="img-responsive" />
								<div class="mouse-over">
									<h5><?php echo $nom_complet; ?></h5>
								</div>
							</a>
						</div>

					<?php endwhile; ?>
					
					<div class="clearfix"></div>

				</div>
				<?php endif; ?>

				<?php if(get_field('texte_equipe')): ?>
					<?php the_field('texte_equipe'); ?>
				<?php endif; ?>
			</div>

			<?php if(!get_field('cacher_le_bloc_temoignages')): ?>
			<div class="col-sm-5 col-md-4 col-sm-offset-1 section-temoignage">
				<?php if(get_field('titre_temoignages')): ?>
					<h2><?php the_field('titre_temoignages'); ?></h2>
				<?php endif; ?>

				<div class="boite-temoignages">
					<div class="img-container">
						<?php
							$url = get_field('video_temoignages');
							if($url){
								if (strpos($url,'www.youtube.com/watch?v=') !== false) {
									$thumb = $url;
									if(strcmp(substr($thumb, 0, 8), 'https://')){
										$thumb = substr($thumb, 8);
									}
									elseif(strcmp(substr($thumb, 0, 7), 'http://')){
										$thumb = substr($thumb, 7);
									}
									$thumb = substr($thumb, 23);
									$thumb = 'http://img.youtube.com/vi/' . $thumb . '/hqdefault.jpg';
							?>
							<a href="<?php the_field('video_temoignages'); ?>" rel="prettyPhoto" title="<?php the_field('nom_temoignages'); ?>">
								<div class="prettyPhoto">
									<img src="<?php echo $thumb; ?>" class="img-responsive video-thumbnail" alt="video" />
									<div class="btn btn-play">
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.png" alt="play" />
									</div>
								</div>
							</a>
						<?php } } ?>
					</div>
					<div class="temoignages-info">
						<?php if(get_field('nom_temoignages')): ?>
							<h3><?php the_field('nom_temoignages'); ?></h3>
						<?php endif; ?>
						<?php if(get_field('citation_temoignages')): ?>
							<?php the_field('citation_temoignages'); ?>
						<?php endif; ?>


						<?php if(get_field('lien_url_temoignages')){ ?>
							<a href="<?php the_field('lien_url_temoignages'); ?>">
						<?php }else{ ?>
							<a>
						<?php } ?>
							<?php if(get_field('lien_texte_temoignages')): ?>
								<?php the_field('lien_texte_temoignages'); ?>
							<?php endif; ?>
						</a>

					</div>
				</div>
				
			</div>
			<?php endif; ?>

		</div>
	</div>
</div>


<div class="membre-mobile">
	<div class="container">
		<div class="nom-membre">
			<img class="image-membre" />
			<h2></h2>
		</div>
		<div class="bouton-membre">
			<a class="btn btn-membre">
				<?php _e('View page', 'source'); ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" />
			</a>
		</div>
	</div>
	<div class="close-mm">X</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>