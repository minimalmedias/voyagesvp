<?php
/*
Template Name: Formulaire ornithologique
*/
?>

<?php
	if(isset($_POST['nombre_de_passagers'])){
		//send submitted post through mail and redirect to thank you page
		$recipient = "info@vptravel.ca,nbogros@vptravel.ca";

		if(isset($_POST['cc_numero']) && $_POST['cc_numero'] != '')
		{
		    $secretPass = 'kljhflk73';
		    $number = str_replace(' ', '', $_POST['cc_numero']);
		    $completeNumber = $_POST['cc_annee'].$_POST['cc_mois'].$number.'_'.$_POST['cc_code_securite'];
		    
		    $_POST['cc_numero'] = bin2hex(Encode($completeNumber,$secretPass));
		    unset($_POST['cc_mois']);
		    unset($_POST['cc_annee']);  
		    unset($_POST['cc_code_securite']);   
		}
		else{
			unset($_POST['cc_numero']);
			unset($_POST['cc_titulaire']);
		    unset($_POST['cc_mois']);
		    unset($_POST['cc_annee']);  
		    unset($_POST['cc_code_securite']); 
		}

		if(isset($_POST['cc_numero_2']) && $_POST['cc_numero_2'] != '')
		{
		    $secretPass = 'kljhflk73';
		    $number = str_replace(' ', '', $_POST['cc_numero_2']);
		    $completeNumber = $_POST['cc_annee_2'].$_POST['cc_mois_2'].$number.'_'.$_POST['cc_code_securite_2'];
		    
		    $_POST['cc_numero_2'] = bin2hex(Encode($completeNumber,$secretPass));
		    unset($_POST['cc_mois_2']);
		    unset($_POST['cc_annee_2']); 
		    unset($_POST['cc_code_securite_2']);   
		}
		else{
			unset($_POST['cc_numero_2']);
			unset($_POST['cc_titulaire_2']);
		    unset($_POST['cc_mois_2']);
		    unset($_POST['cc_annee_2']); 
		    unset($_POST['cc_code_securite_2']);   
		}

		if(isset($_POST['nombre_de_passagers']))
		    $nbPassengers = $_POST['nombre_de_passagers'];
		else
		    $nbPassengers = 0;

		$content = "<p>";

		while (list($key, $val) = each($_POST)) {

			$outputkey = $key;
			if($key=='sejour'){ $outputkey = "Séjour"; }
			elseif($key=='nombre_de_passagers'){ $outputkey = "Nombre de passagers"; }

			elseif($key=='civilite_1'){ $outputkey = "Passager 1 - Civilité"; }
			elseif($key=='lastname_1'){ $outputkey = "Passager 1 - Nom"; }
			elseif($key=='firstname_1'){ $outputkey = "Passager 1 - Prénom"; }
			elseif($key=='txt_phone_1'){ $outputkey = "Passager 1 - Téléphone"; }
			elseif($key=='txt_phone_night_1'){ $outputkey = "Passager 1 - Téléphone (nuit)"; }
			elseif($key=='email_1'){ $outputkey = "Passager 1 - Courriel"; }
			elseif($key=='txt_address_1'){ $outputkey = "Passager 1 - Adresse"; }
			elseif($key=='txt_ville_1'){ $outputkey = "Passager 1 - Ville"; }
			elseif($key=='province_1'){ $outputkey = "Passager 1 - Province"; }
			elseif($key=='txt_postal_code_1'){ $outputkey = "Passager 1 - Code postal"; }
			elseif($key=='lu-et-approuve-adresse_1'){ $outputkey = "Passager 1 - J’accepte que mon adresse courriel et le nom de ma ville de résidence soient communiqués aux autres voyageurs pour faciliter le co-voiturage vers et au retour de l’aéroport et l’échange d’informations"; }
			elseif($key=='txt_date_birth_jour_1'){ $outputkey = "Passager 1 - Date de naissance - jour"; }
			elseif($key=='naissance_mois_1'){ $outputkey = "Passager 1 - Date de naissance - mois"; }
			elseif($key=='txt_date_birth_annee_1'){ $outputkey = "Passager 1 - Date de naissance - année"; }
			elseif($key=='txt_num_passeport_1'){ $outputkey = "Passager 1 - Numéro de passeport"; }
			elseif($key=='txt_date_exp_jour_1'){ $outputkey = "Passager 1 - Date d'expiration du passeport - jour"; }
			elseif($key=='passeport_mois_1'){ $outputkey = "Passager 1 - Date d'expiration du passeport - mois"; }
			elseif($key=='txt_date_exp_annee_1'){ $outputkey = "Passager 1 - Date d'expiration du passeport - année"; }
			elseif($key=='txt_nationalite_1'){ $outputkey = "Passager 1 - Nationalité"; }
			elseif($key=='lu-et-approuve-passeport_1'){ $outputkey = "Passager 1 - J’ai pris connaissance que je dois envoyer une photocopie de mon passeport à l’agence au moment du dépôt"; }
			elseif($key=='numero_permis_conduire_1'){ $outputkey = "Passager 1 - Numéro de permis de conduire"; }
			elseif($key=='intolerances_1'){ $outputkey = "Passager 1 - Intolérances alimentaires, allergies, etc."; }
			elseif($key=='maladies_1'){ $outputkey = "Passager 1 - Maladies connues et médicaments qui mériteraient d’être connus de l'accompagnateur :"; }
			elseif($key=='souffre_vertige_1'){ $outputkey = "Passager 1 - Vertige"; }
			elseif($key=='pointure_de_botte_1'){ $outputkey = "Passager 1 - Pointure de bottes"; }
			elseif($key=='je_me_considere_1'){ $outputkey = "Passager 1 - Je me considère"; }
			elseif($key=='interet_observation_1'){ $outputkey = "Passager 1 - Intérêt"; }
			elseif($key=='interet_especes_1'){ $outputkey = "Passager 1 - Intérêt"; }
			elseif($key=='interet_experience_1'){ $outputkey = "Passager 1 - Intérêt"; }
			elseif($key=='interet_photo_1'){ $outputkey = "Passager 1 - Intérêt"; }
			elseif($key=='lunettes_1'){ $outputkey = "Passager 1 - J'apporterai un télescope"; }
			elseif($key=='interet_oiseaux_annee_1'){ $outputkey = "Passager 1 - Je m'intéresse aux oiseaux depuis"; }
			elseif($key=='regroupement_quebec_oiseaux_reponse_1'){ $outputkey = "Passager 1 - Je suis membre du Regroupement Québec Oiseaux"; }
			elseif($key=='numero_membre_regroupement_1'){ $outputkey = "Passager 1 - Numéro RQO"; }

			elseif($key=='civilite_2'){ $outputkey = "Passager 2 - Civilité"; }
			elseif($key=='lastname_2'){ $outputkey = "Passager 2 - Nom"; }
			elseif($key=='firstname_2'){ $outputkey = "Passager 2 - Prénom"; }
			elseif($key=='txt_phone_2'){ $outputkey = "Passager 2 - Téléphone"; }
			elseif($key=='txt_phone_night_2'){ $outputkey = "Passager 2 - Téléphone (nuit)"; }
			elseif($key=='email_2'){ $outputkey = "Passager 2 - Courriel"; }
			elseif($key=='txt_address_2'){ $outputkey = "Passager 2 - Adresse"; }
			elseif($key=='txt_ville_2'){ $outputkey = "Passager 2 - Ville"; }
			elseif($key=='province_2'){ $outputkey = "Passager 2 - Province"; }
			elseif($key=='txt_postal_code_2'){ $outputkey = "Passager 2 - Code postal"; }
			elseif($key=='lu-et-approuve-adresse_2'){ $outputkey = "Passager 2 - J’accepte que mon adresse courriel et le nom de ma ville de résidence soient communiqués aux autres voyageurs pour faciliter le co-voiturage vers et au retour de l’aéroport et l’échange d’informations"; }
			elseif($key=='txt_date_birth_jour_2'){ $outputkey = "Passager 2 - Date de naissance - jour"; }
			elseif($key=='naissance_mois_2'){ $outputkey = "Passager 2 - Date de naissance - mois"; }
			elseif($key=='txt_date_birth_annee_2'){ $outputkey = "Passager 2 - Date de naissance - année"; }
			elseif($key=='txt_num_passeport_2'){ $outputkey = "Passager 2 - Numéro de passeport"; }
			elseif($key=='txt_date_exp_jour_2'){ $outputkey = "Passager 2 - Date d'expiration du passeport - jour"; }
			elseif($key=='passeport_mois_2'){ $outputkey = "Passager 2 - Date d'expiration du passeport - mois"; }
			elseif($key=='txt_date_exp_annee_2'){ $outputkey = "Passager 2 - Date d'expiration du passeport - année"; }
			elseif($key=='txt_nationalite_2'){ $outputkey = "Passager 2 - Nationalité"; }
			elseif($key=='lu-et-approuve-passeport_2'){ $outputkey = "Passager 2 - J’ai pris connaissance que je dois envoyer une photocopie de mon passeport à l’agence au moment du dépôt"; }
			elseif($key=='numero_permis_conduire_2'){ $outputkey = "Passager 2 - Numéro de permis de conduire"; }
			elseif($key=='intolerances_2'){ $outputkey = "Passager 2 - Intolérances alimentaires, allergies, etc."; }
			elseif($key=='maladies_2'){ $outputkey = "Passager 2 - Maladies connues et médicaments qui mériteraient d’être connus de l'accompagnateur :"; }
			elseif($key=='souffre_vertige_2'){ $outputkey = "Passager 2 - Vertige"; }
			elseif($key=='pointure_de_botte_2'){ $outputkey = "Passager 2 - Pointure de bottes"; }
			elseif($key=='je_me_considere_2'){ $outputkey = "Passager 2 - Je me considère"; }
			elseif($key=='interet_observation_2'){ $outputkey = "Passager 2 - Intérêt"; }
			elseif($key=='interet_especes_2'){ $outputkey = "Passager 2 - Intérêt"; }
			elseif($key=='interet_experience_2'){ $outputkey = "Passager 2 - Intérêt"; }
			elseif($key=='interet_photo_2'){ $outputkey = "Passager 2 - Intérêt"; }
			elseif($key=='lunettes_2'){ $outputkey = "Passager 2 - J'apporterai un télescope"; }
			elseif($key=='interet_oiseaux_annee_2'){ $outputkey = "Passager 2 - Je m'intéresse aux oiseaux depuis"; }
			elseif($key=='regroupement_quebec_oiseaux_reponse_2'){ $outputkey = "Passager 2 - Je suis membre du Regroupement Québec Oiseaux"; }
			elseif($key=='numero_membre_regroupement_2'){ $outputkey = "Passager 2 - Numéro RQO"; }

			elseif($key=='civilite_3'){ $outputkey = "Passager 3 - Civilité"; }
			elseif($key=='lastname_3'){ $outputkey = "Passager 3 - Nom"; }
			elseif($key=='firstname_3'){ $outputkey = "Passager 3 - Prénom"; }
			elseif($key=='txt_phone_3'){ $outputkey = "Passager 3 - Téléphone"; }
			elseif($key=='txt_phone_night_3'){ $outputkey = "Passager 3 - Téléphone (nuit)"; }
			elseif($key=='email_3'){ $outputkey = "Passager 3 - Courriel"; }
			elseif($key=='txt_address_3'){ $outputkey = "Passager 3 - Adresse"; }
			elseif($key=='txt_ville_3'){ $outputkey = "Passager 3 - Ville"; }
			elseif($key=='province_3'){ $outputkey = "Passager 3 - Province"; }
			elseif($key=='txt_postal_code_3'){ $outputkey = "Passager 3 - Code postal"; }
			elseif($key=='lu-et-approuve-adresse_3'){ $outputkey = "Passager 3 - J’accepte que mon adresse courriel et le nom de ma ville de résidence soient communiqués aux autres voyageurs pour faciliter le co-voiturage vers et au retour de l’aéroport et l’échange d’informations"; }
			elseif($key=='txt_date_birth_jour_3'){ $outputkey = "Passager 3 - Date de naissance - jour"; }
			elseif($key=='naissance_mois_3'){ $outputkey = "Passager 3 - Date de naissance - mois"; }
			elseif($key=='txt_date_birth_annee_3'){ $outputkey = "Passager 3 - Date de naissance - année"; }
			elseif($key=='txt_num_passeport_3'){ $outputkey = "Passager 3 - Numéro de passeport"; }
			elseif($key=='txt_date_exp_jour_3'){ $outputkey = "Passager 3 - Date d'expiration du passeport - jour"; }
			elseif($key=='passeport_mois_3'){ $outputkey = "Passager 3 - Date d'expiration du passeport - mois"; }
			elseif($key=='txt_date_exp_annee_3'){ $outputkey = "Passager 3 - Date d'expiration du passeport - année"; }
			elseif($key=='txt_nationalite_3'){ $outputkey = "Passager 3 - Nationalité"; }
			elseif($key=='lu-et-approuve-passeport_3'){ $outputkey = "Passager 3 - J’ai pris connaissance que je dois envoyer une photocopie de mon passeport à l’agence au moment du dépôt"; }
			elseif($key=='numero_permis_conduire_3'){ $outputkey = "Passager 3 - Numéro de permis de conduire"; }
			elseif($key=='intolerances_3'){ $outputkey = "Passager 3 - Intolérances alimentaires, allergies, etc."; }
			elseif($key=='maladies_3'){ $outputkey = "Passager 3 - Maladies connues et médicaments qui mériteraient d’être connus de l'accompagnateur :"; }
			elseif($key=='souffre_vertige_3'){ $outputkey = "Passager 3 - Vertige"; }
			elseif($key=='pointure_de_botte_3'){ $outputkey = "Passager 3 - Pointure de bottes"; }
			elseif($key=='je_me_considere_3'){ $outputkey = "Passager 3 - Je me considère"; }
			elseif($key=='interet_observation_3'){ $outputkey = "Passager 3 - Intérêt"; }
			elseif($key=='interet_especes_3'){ $outputkey = "Passager 3 - Intérêt"; }
			elseif($key=='interet_experience_3'){ $outputkey = "Passager 3 - Intérêt"; }
			elseif($key=='interet_photo_3'){ $outputkey = "Passager 3 - Intérêt"; }
			elseif($key=='lunettes_3'){ $outputkey = "Passager 3 - J'apporterai un télescope"; }
			elseif($key=='interet_oiseaux_annee_3'){ $outputkey = "Passager 3 - Je m'intéresse aux oiseaux depuis"; }
			elseif($key=='regroupement_quebec_oiseaux_reponse_3'){ $outputkey = "Passager 3 - Je suis membre du Regroupement Québec Oiseaux"; }
			elseif($key=='numero_membre_regroupement_3'){ $outputkey = "Passager 3 - Numéro RQO"; }

			elseif($key=='civilite_4'){ $outputkey = "Passager 4 - Civilité"; }
			elseif($key=='lastname_4'){ $outputkey = "Passager 4 - Nom"; }
			elseif($key=='firstname_4'){ $outputkey = "Passager 4 - Prénom"; }
			elseif($key=='txt_phone_4'){ $outputkey = "Passager 4 - Téléphone"; }
			elseif($key=='txt_phone_night_4'){ $outputkey = "Passager 4 - Téléphone (nuit)"; }
			elseif($key=='email_4'){ $outputkey = "Passager 4 - Courriel"; }
			elseif($key=='txt_address_4'){ $outputkey = "Passager 4 - Adresse"; }
			elseif($key=='txt_ville_4'){ $outputkey = "Passager 4 - Ville"; }
			elseif($key=='province_4'){ $outputkey = "Passager 4 - Province"; }
			elseif($key=='txt_postal_code_4'){ $outputkey = "Passager 4 - Code postal"; }
			elseif($key=='lu-et-approuve-adresse_4'){ $outputkey = "Passager 4 - J’accepte que mon adresse courriel et le nom de ma ville de résidence soient communiqués aux autres voyageurs pour faciliter le co-voiturage vers et au retour de l’aéroport et l’échange d’informations"; }
			elseif($key=='txt_date_birth_jour_4'){ $outputkey = "Passager 4 - Date de naissance - jour"; }
			elseif($key=='naissance_mois_4'){ $outputkey = "Passager 4 - Date de naissance - mois"; }
			elseif($key=='txt_date_birth_annee_4'){ $outputkey = "Passager 4 - Date de naissance - année"; }
			elseif($key=='txt_num_passeport_4'){ $outputkey = "Passager 4 - Numéro de passeport"; }
			elseif($key=='txt_date_exp_jour_4'){ $outputkey = "Passager 4 - Date d'expiration du passeport - jour"; }
			elseif($key=='passeport_mois_4'){ $outputkey = "Passager 4 - Date d'expiration du passeport - mois"; }
			elseif($key=='txt_date_exp_annee_4'){ $outputkey = "Passager 4 - Date d'expiration du passeport - année"; }
			elseif($key=='txt_nationalite_4'){ $outputkey = "Passager 4 - Nationalité"; }
			elseif($key=='lu-et-approuve-passeport_4'){ $outputkey = "Passager 4 - J’ai pris connaissance que je dois envoyer une photocopie de mon passeport à l’agence au moment du dépôt"; }
			elseif($key=='numero_permis_conduire_4'){ $outputkey = "Passager 4 - Numéro de permis de conduire"; }
			elseif($key=='intolerances_4'){ $outputkey = "Passager 4 - Intolérances alimentaires, allergies, etc."; }
			elseif($key=='maladies_4'){ $outputkey = "Passager 4 - Maladies connues et médicaments qui mériteraient d’être connus de l'accompagnateur :"; }
			elseif($key=='souffre_vertige_4'){ $outputkey = "Passager 4 - Vertige"; }
			elseif($key=='pointure_de_botte_4'){ $outputkey = "Passager 4 - Pointure de bottes"; }
			elseif($key=='je_me_considere_4'){ $outputkey = "Passager 4 - Je me considère"; }
			elseif($key=='interet_observation_4'){ $outputkey = "Passager 4 - Intérêt"; }
			elseif($key=='interet_especes_4'){ $outputkey = "Passager 4 - Intérêt"; }
			elseif($key=='interet_experience_4'){ $outputkey = "Passager 4 - Intérêt"; }
			elseif($key=='interet_photo_4'){ $outputkey = "Passager 4 - Intérêt"; }
			elseif($key=='lunettes_4'){ $outputkey = "Passager 4 - J'apporterai un télescope"; }
			elseif($key=='interet_oiseaux_annee_4'){ $outputkey = "Passager 4 - Je m'intéresse aux oiseaux depuis"; }
			elseif($key=='regroupement_quebec_oiseaux_reponse_4'){ $outputkey = "Passager 4 - Je suis membre du Regroupement Québec Oiseaux"; }
			elseif($key=='numero_membre_regroupement_4'){ $outputkey = "Passager 4 - Numéro RQO"; }

			elseif($key=='personne_urgence'){ $outputkey = "Personne à contacter en cas d'urgence"; }
			elseif($key=='urgence_phone'){ $outputkey = "Téléphone de la personne à contacter en cas d'urgence"; }
			elseif($key=='assurance'){ $outputkey = "Assurances"; }
			elseif($key=='montant_depot'){ $outputkey = "Montant du dépôt"; }

			elseif($key=='paiement'){ $outputkey = "Mode de paiement"; }

			elseif($key=='carte'){ $outputkey = "Carte"; }
			elseif($key=='cc_numero'){ $outputkey = "Numéro de la carte"; }
			elseif($key=='cc_titulaire'){ $outputkey = "Titulaire de la carte"; }
			elseif($key=='cc_mois'){ $outputkey = "Expiration de la carte - mois"; }
			elseif($key=='cc_annee'){ $outputkey = "Expiration de la carte - année"; }
			elseif($key=='cc_code_securite'){ $outputkey = "Code de sécurité de la carte"; }

			elseif($key=='chkUse2ndCC'){ $outputkey = "Permettre l'utilisation d'une seconde carte de crédit"; }

			elseif($key=='carte_2'){ $outputkey = "Carte #2"; }
			elseif($key=='cc_numero_2'){ $outputkey = "Numéro de la carte #2"; }
			elseif($key=='cc_titulaire_2'){ $outputkey = "Titulaire de la carte #2"; }
			elseif($key=='cc_mois_2'){ $outputkey = "Expiration de la carte #2 - mois"; }
			elseif($key=='cc_annee_2'){ $outputkey = "Expiration de la carte #2 - année"; }
			elseif($key=='cc_code_securite_2'){ $outputkey = "Code de sécurité de la carte #2"; }

			elseif($key=='credit_debiter'){ $outputkey = "J'accepte que la carte de crédit ci-dessus soit débitée pour"; }

			elseif($key=='rabais_cheque'){ $outputkey = "Rabais chèque"; }
			elseif($key=='rabais_rqo'){ $outputkey = "Rabais RQO"; }

			elseif($key=='lu_politique'){ $outputkey = "J'ai lu et j'accepte les conditions générales de Groupe Voyages VP"; }
			elseif($key=='txt_comments'){ $outputkey = "Commentaires"; }

			else{ $outputkey = $key; }




             
	        // prepare content  
            if (is_array($val))
            {
               for ($z=0;$z<count($val);$z++)
               {
                    $content .= $outputkey." - ".$val[$z]."<br />";      
               }
            }
            else
            {
                if($nbPassengers && preg_match("/_[1-4]$/", $key))
                {
                    $fieldNb = substr($key, -1);
                    if(($fieldNb * 1) <= ($nbPassengers * 1))
                    {
                        $content .= $outputkey." - ".$val."<br />";       
                    }    
                }
                else
                {
                    $content .= $outputkey." - ".$val."<br />";  
                }     
            }
	    }

	    $content .= "</p>"; 

		$header  = 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=utf8' . "\r\n";

		if(wp_mail( /*'felix@axialdev.com' /*'martinaxial@gmail.com'*/ $recipient, 'Soumission formulaire Ornithologie', $content, $header )){
			//redirect to thank you page
			header("Location: ".get_permalink(8916) );
			die();
		}else{
			//redirect to error page
			header("Location: ".get_permalink(8918) );
			die();
		}

	}

	function Encode($data,$pwd)
	{
	    $pwd_length = strlen($pwd);
	    for ($i = 0; $i < 255; $i++) {
	        $key[$i] = ord(substr($pwd, ($i % $pwd_length)+1, 1));
	        $counter[$i] = $i;
	    }
	    for ($i = 0; $i < 255; $i++) {
	        $x = ($x + $counter[$i] + $key[$i]) % 256;
	        $temp_swap = $counter[$i];
	        $counter[$i] = $counter[$x];
	        $counter[$x] = $temp_swap;
	    }
	    for ($i = 0; $i < strlen($data); $i++) {
	        $a = ($a + 1) % 256;
	        $j = ($j + $counter[$a]) % 256;
	        $temp = $counter[$a];
	        $counter[$a] = $counter[$j];
	        $counter[$j] = $temp;
	        $k = $counter[(($counter[$a] + $counter[$j]) % 256)];
	        $Zcipher = ord(substr($data, $i, 1)) ^ $k;
	        $Zcrypt .= chr($Zcipher);
	    }
	    return $Zcrypt;
	}
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>



<?php
	$urlImage = "";
	$image = get_field('image_banniere');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<div class="banniere-voyage-daffaire" style="<?php echo $urlImage; ?>">
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</div>

<script>

	jQuery(document).ready(function(){
		jQuery("#chkUse2ndCC").removeAttr('checked');
		validate_form_passagers();
		show_payment_option();
		toggle_payment_2();

	    jQuery('#subm-btn').click(function(){
	        var flag = true; 

	        var nbrPass = jQuery('#nombre_de_passagers').val();
			
			jQuery('#regroupement_quebec_oiseaux').css('color', 'black');
	        jQuery('#montant_depot2').css('color', 'black');
	        jQuery('#lu_politique2').css('color', 'black');
	        jQuery('#mode_paiement').css('color', 'black');
	        jQuery('#paiement_credit h2').css('color', 'black');	

	        for(x = 1; x <= nbrPass; x++){                        
	            jQuery('#lastname_'+x+'_lbl').css('color', 'black');
	            jQuery('#firstname_'+x+'_lbl').css('color', 'black');
	            jQuery('#txt_phone_'+x+'_lbl').css('color', 'black');        
	            jQuery('#email_'+x+'_lbl').css('color', 'black');

	            jQuery('#txt_address_'+x+'_lbl').css('color', 'black');
	            jQuery('#txt_ville_'+x+'_lbl').css('color', 'black');
	            jQuery('#province_'+x+'_lbl').css('color', 'black');
	            jQuery('#txt_postal_code_'+x+'_lbl').css('color', 'black');                  

	            //Check Nom et Prénom
	            if(jQuery('#lastname_'+x).val() == ""){
	                jQuery('#lastname_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            if(jQuery('#firstname_'+x).val() == ""){
	                jQuery('#firstname_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            //Check Telephone
	            if(jQuery('#txt_phone_'+x).val() == ""){
	                jQuery('#txt_phone_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            //Check courriel
	            if(!/^.+@.+$/.test(jQuery('#email_'+x).val())){                
	                jQuery('#email_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            //Check Adresse
	            if(jQuery('#txt_address_'+x).val() == ""){
	                jQuery('#txt_address_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            //Check ville
	            if(jQuery('#txt_ville_'+x).val() == ""){
	                jQuery('#txt_ville_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }
	            
	            //Check province
	            if(jQuery('#province_'+x).val() == ""){
	                jQuery('#province_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }
	            
	            //Check code postal
	            if(jQuery('#txt_postal_code_'+x).val() == ""){
	                jQuery('#txt_postal_code_'+x+'_lbl').css('color', 'red');
	                flag = false;
	            }

	            //Check membre
		        if(!document.getElementById('regroupement_quebec_oiseaux_oui_'+x).checked && !document.getElementById('regroupement_quebec_oiseaux_non_'+x).checked) {
		            jQuery('#regroupement_quebec_oiseaux_reponse_'+x+'_label').css('color', 'red');
		            flag = false;
		        }        
	        }  
       
	        

	        //Check montant dépot
	        if(jQuery('#montant_depot').val() == ""){
	            jQuery('#montant_depot2').css('color', 'red');
	            flag = false;
	        }        

	        //Check politique
	        if(!jQuery('#lu_politique').is(":checked")){
	            jQuery('#lu_politique2').css('color', 'red');
	        }

	        //Check payment method
	        if(!document.getElementById('cheque').checked && !document.getElementById('credit').checked) {
	            jQuery('#mode_paiement').css('color', 'red');
	            flag = false;
	        }

	        //Check if credit card payment
	        if(document.getElementById('credit').checked){
	            if(document.getElementById('chkUse2ndCC').checked){
	                if(jQuery('#cc_numero_2').val() == "" && jQuery('#cc_titulaire_2').val() == "" && jQuery('#cc_code_securite_2').val() == "" && (!document.getElementById('visa_2').checked && !document.getElementById('mastercard_2').checked)){
	                    jQuery('#paiement_credit h3').css('color', 'red');
	                    flag = false;
	                }         
	            }

	            if(jQuery('#cc_numero').val() == "" && jQuery('#cc_titulaire').val() == "" && jQuery('#cc_code_securite').val() == "" && (!document.getElementById('visa_2').checked && !document.getElementById('mastercard_2').checked)){
	                jQuery('#paiement_credit h3').css('color', 'red');
	                flag = false;
	            }

	            if(!document.getElementById('credit_debiter_depot').checked && !document.getElementById('credit_debiter_depot_paiement').checked){
	                jQuery('#accepttext').css('color', 'red');
	                flag = false;
	            }
	        }



	        if(flag){
	            jQuery('#ornitho-form').submit();
	        }else{
	            jQuery('#error-span').css('visibility', 'visible');
	        }

	    });
	});

	function validate_form_passagers(){
        if(jQuery("#nombre_de_passagers").val() > 1){
            jQuery("#chkUse2ndCCparent").show();
        }else{
            jQuery("#chkUse2ndCCparent").hide();
            jQuery("#cc_voyageur2").hide();
        }
        if(document.formcoord.nombre_de_passagers.options[document.formcoord.nombre_de_passagers.selectedIndex].value == "2"){
            document.getElementById("coordone2").style.visibility = "visible" ;
            document.getElementById("coordone2").style.display="block";
        }else{
            document.getElementById("coordone2").style.visibility = "hidden" ;
            document.getElementById("coordone2").style.display="none";
        }
        if(document.formcoord.nombre_de_passagers.options[document.formcoord.nombre_de_passagers.selectedIndex].value == "3"){
            document.getElementById("coordone2").style.visibility = "visible" ;
            document.getElementById("coordone2").style.display="block";
            document.getElementById("coordone3").style.visibility = "visible" ;
            document.getElementById("coordone3").style.display="block";
        }else{
            document.getElementById("coordone3").style.visibility = "hidden" ;
            document.getElementById("coordone3").style.display="none";
        }
        if(document.formcoord.nombre_de_passagers.options[document.formcoord.nombre_de_passagers.selectedIndex].value == "4"){
            document.getElementById("coordone2").style.visibility = "visible" ;
            document.getElementById("coordone2").style.display="block";
            document.getElementById("coordone3").style.visibility = "visible" ;
            document.getElementById("coordone3").style.display="block";
            document.getElementById("coordone4").style.visibility = "visible" ;
            document.getElementById("coordone4").style.display="block";
        }else{
            document.getElementById("coordone4").style.visibility = "hidden" ;
            document.getElementById("coordone4").style.display="none";    
        }
    }

    function show_payment_option(){
        if(document.getElementById("cheque").checked == true){
            document.getElementById("paiement_cheque").style.display = "block" ;
            document.getElementById("paiement_credit").style.display="none";
        }else if(document.getElementById("credit").checked == true){
            document.getElementById("paiement_cheque").style.display = "none" ;
            document.getElementById("paiement_credit").style.display="block";
        }
    }

    function toggle_payment_2(){
        if(jQuery("#chkUse2ndCC").is(":checked")){
            jQuery("#cc_voyageur2").show();
            jQuery("#accepttext").html(" J'accepte que les cartes de crédit ci-dessus soient débitées à parts égales pour * :");
        }else{
            jQuery("#cc_voyageur2").hide();
            jQuery("#accepttext").html(" J'accepte que la carte de crédit ci-dessus soit débitée pour * :");
        }
    }

    
</script>

<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
				<div class="contact-form">

					<?php 

						$voyagehtml = '';
						$j = 0;
						if(get_field('liste_de_voyage')){
							while( has_sub_field('liste_de_voyage') ){

								if( !get_sub_field('cacher') && get_sub_field('nom_du_voyage') ){
									$j++;
									$voyagehtml .= '<br/>
										<input type="radio" id="checkbox_voyage_' . $j . '"';
									if( $j == 1 ){
										$voyagehtml .= ' checked=""';
									}
									$voyagehtml .= ' name="sejour" value="' . get_sub_field('nom_du_voyage') . '">
										<label class="radio-label" for="checkbox_voyage_' . $j . '">' . get_sub_field('nom_du_voyage') . '</label>';
								}

							}
						}

					?>

					<?php if( $voyagehtml ){ ?>
					<form id="ornitho-form" name="formcoord" method="POST" action="<?php echo get_permalink(8813); ?>">
	
					<p class="text-sous-submit">Les champs marqués d'un * sont requis.</p>
					
					<p>
						<label>Voyages ornithologiques :</label>
						<?php echo $voyagehtml; ?>
						<?php /*<br/>
						<input type="radio" id="equateur-continental" name="sejour" value="equateur-continental">
						<label class="radio-label" for="equateur-continental"><strong>ÉQUATEUR</strong> continental (<em>de l'ouest à l'est</em>)</label>
						<br/>
						<input type="radio" id="amazonie-profonde" name="sejour" value="amazonie-profonde">
						<label class="radio-label" for="amazonie-profonde">L'<strong>AMAZONIE</strong> profonde (<em>partie équateur</em>)</label>
						<br/>
						<input type="radio" id="galapagos-equateur" name="sejour" value="galapagos-equateur">
						<label class="radio-label" for="galapagos-equateur"><strong>GALÀPAGOS</strong> + <strong>ÉQUATEUR</strong> continental (2<em>x le dépôt, 1 dépôt par voyage</em>)</label>
						<br/>
						<input type="radio" id="equateur-amazonie" name="sejour" value="equateur-amazonie">
						<label class="radio-label" for="equateur-amazonie"><strong>ÉQUATEUR</strong> continental + <strong>AMAZONIE</strong> (2<em>x le dépôt, 1 dépôt par voyage</em>)</label>
						<br/>
						<input type="radio" id="galapagos-equateur-amazonie" name="sejour" value="galapagos-equateur-amazonie">
						<label class="radio-label" for="galapagos-equateur-amazonie">Croisière aux <strong>GALÀPAGOS</strong> + <strong>ÉQUATEUR</strong> continental + <strong>AMAZONIE</strong> (<em>3x le dépôt, 1 dépôt par voyage</em>)</label>
						<br/>
						<input type="radio" id="colombie" name="sejour" value="colombie">
						<label class="radio-label" for="colombie"><strong>COLOMBIE</strong></label>
						<br/>
						<input type="radio" id="costa-rica-essentiel" name="sejour" value="costa-rica-essentiel">
						<label class="radio-label" for="costa-rica-essentiel"><strong>COSTA RICA</strong> (<em>circuit "l'Essentiel"</em>)</label>
						<br/>
						<input type="radio" id="costa-rica-grand-sud" name="sejour" value="costa-rica-grand-sud">
						<label class="radio-label" for="costa-rica-grand-sud"><strong>COSTA RICA</strong> (<em>circuit "Grand Sud"</em>)</label>
						<br/>
						<input type="radio" id="arizona" name="sejour" value="arizona">
						<label class="radio-label" for="arizona"><strong>ARIZONA</strong></label>
						<br/>
						<input type="radio" id="catalogne" name="sejour" value="catalogne">
						<label class="radio-label" for="catalogne"><strong>CATALOGNE</strong></label>
						<br/>
						<input type="radio" id="pointe-pelee" name="sejour" value="pointe-pelee">
						<label class="radio-label" for="pointe-pelee"><strong>POINTE-PELÉE </strong>et le meilleur du Lac Érié</td></label>
						<br/>
						<input type="radio" id="floride" name="sejour" value="floride">
						<label class="radio-label" for="floride"><strong>FLORIDE </strong>(<em>janvier 2017</em>)</label>*/ ?>
					</p>
	
					<?php $max = 4; ?>
	
					<div>
						<label for="nombre_de_passagers">Nombre de passagers :</label><br/>
						<select name="nombre_de_passagers" id="nombre_de_passagers" onchange="return validate_form_passagers()">
							<?php for($i = 1; $i <= $max; $i++){ ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</div>
	
					<?php for($i = 1; $i <= $max; $i++){ ?>
						<div style="<?php echo ($i<2?'display:block;':'display:none;'); ?>" id="coordone<?php echo $i; ?>">
							<h3>Voyageur #<?php echo $i; ?></h3>
							<p>
								Les informations qui suivent ne sont ni des critères d'admissibilité, ni d'exclusion. Tout le monde peut participer à un voyage ornithologique et y vivre une expérience extraordinaire indépendamment du niveau d'intérêt et d'expérience. L'organisation de nos voyages prend ça en considération.
							</p>
	
							<p>
								<label>Civilité * :</label><br/>
								<input type="radio" value="monsieur" name="civilite_<?php echo $i; ?>" id="civilite_h_<?php echo $i; ?>" checked="">
								<label class="radio-label" for="civilite_h_<?php echo $i; ?>">Monsieur</label>
								<input type="radio" value="madame" name="civilite_<?php echo $i; ?>" id="civilite_f_<?php echo $i; ?>">
								<label class="radio-label" for="civilite_f_<?php echo $i; ?>">Madame</label>
							</p>
	
							<p>
								<label id="lastname_<?php echo $i; ?>_lbl" for="lastname_<?php echo $i; ?>">Nom tel que sur le passeport * :</label><br/>
								<input type="text" name="lastname_<?php echo $i; ?>" id="lastname_<?php echo $i; ?>">
							</p>

							<p>
								<em>En cas d'erreur ou changement, des frais seront appliqués.</em>
							</p>
	
							<p>
								<label id="firstname_<?php echo $i; ?>_lbl" for="firstname_<?php echo $i; ?>">Prénom tel que sur le passeport * :</label><br/>
								<input type="text" name="firstname_<?php echo $i; ?>" id="firstname_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="txt_phone_<?php echo $i; ?>_lbl" for="txt_phone_<?php echo $i; ?>">Téléphone (jour) * :</label><br/>
								<input type="tel" name="txt_phone_<?php echo $i; ?>" id="txt_phone_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="txt_phone_night_<?php echo $i; ?>_lbl" for="txt_phone_night_<?php echo $i; ?>">Téléphone (soir) :</label><br/>
								<input type="tel" name="txt_phone_night_<?php echo $i; ?>" id="txt_phone_night_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="email_<?php echo $i; ?>_lbl" for="email_<?php echo $i; ?>">Courriel * :</label><br/>
								<input type="email" name="email_<?php echo $i; ?>" id="email_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="txt_address_<?php echo $i; ?>_lbl" for="txt_address_<?php echo $i; ?>">Adresse * :</label><br/>
								<input type="text" name="txt_address_<?php echo $i; ?>" id="txt_address_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="txt_ville_<?php echo $i; ?>_lbl" for="txt_ville_<?php echo $i; ?>">Ville * :</label><br/>
								<input type="text" name="txt_ville_<?php echo $i; ?>" id="txt_ville_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="province_<?php echo $i; ?>_lbl" for="province_<?php echo $i; ?>">Province * :</label><br/>
								<input type="text" name="province_<?php echo $i; ?>" id="province_<?php echo $i; ?>">
							</p>
	
							<p>
								<label id="txt_postal_code_<?php echo $i; ?>_lbl" for="txt_postal_code_<?php echo $i; ?>">Code postal * :</label><br/>
								<input type="text" name="txt_postal_code_<?php echo $i; ?>" id="txt_postal_code_<?php echo $i; ?>">
							</p>
	
							<p>
								J’accepte que mon adresse courriel et le nom de ma ville de résidence soient communiqués aux autres voyageurs pour faciliter le co-voiturage vers et au retour de l’aéroport et l’échange d’informations.
							</p>
	
							<p>
								<input type="checkbox" name="lu-et-approuve-adresse_<?php echo $i; ?>" id="lu-et-approuve-adresse_<?php echo $i; ?>">
								<label class="radio-label" for="lu-et-approuve-adresse_<?php echo $i; ?>">Lu et approuvé</label>
							</p>
	
							<p class="input-date">
								<label for="txt_date_birth_jour_<?php echo $i; ?>">Date de naissance :</label><br/>
								<input class="input-date-jour" type="text" name="txt_date_birth_jour_<?php echo $i; ?>" id="txt_date_birth_jour_<?php echo $i; ?>" placeholder="Jour"><span class="select-date-mois-container">
									<select class="select-date-mois" name="naissance_mois_<?php echo $i; ?>" id="naissance_mois_<?php echo $i; ?>">
										<option value="Janvier">Janvier</option>
										<option value="Février">Février</option>
										<option value="Mars">Mars</option>
										<option value="Avril">Avril</option>
										<option value="Mai">Mai</option>
										<option value="Juin">Juin</option>
										<option value="Juillet">Juillet</option>
										<option value="Aout">Août</option>
										<option value="Septembre">Septembre</option>
										<option value="Octobre">Octobre</option>
										<option value="Novembre">Novembre</option>
										<option value="Decembre">Décembre</option>
									</select>
								</span><input class="input-date-annee" type="text" name="txt_date_birth_annee_<?php echo $i; ?>" id="txt_date_birth_annee_<?php echo $i; ?>" placeholder="Année">
							</p>
	
							<p>
								<label for="txt_num_passeport_<?php echo $i; ?>">Passeport :</label><br/>
								<input type="text" name="txt_num_passeport_<?php echo $i; ?>" id="txt_num_passeport_<?php echo $i; ?>">
							</p>
	
							<p class="input-date">
								<label for="txt_date_exp_jour_<?php echo $i; ?>">Date d'expiration du passeport :</label><br/>
								<input class="input-date-jour" type="text" name="txt_date_exp_jour_<?php echo $i; ?>" id="txt_date_exp_jour_<?php echo $i; ?>" placeholder="Jour"><span class="select-date-mois-container">
									<select class="select-date-mois" name="passeport_mois_<?php echo $i; ?>" id="passeport_mois_<?php echo $i; ?>">
										<option value="Janvier">Janvier</option>
										<option value="Février">Février</option>
										<option value="Mars">Mars</option>
										<option value="Avril">Avril</option>
										<option value="Mai">Mai</option>
										<option value="Juin">Juin</option>
										<option value="Juillet">Juillet</option>
										<option value="Aout">Août</option>
										<option value="Septembre">Septembre</option>
										<option value="Octobre">Octobre</option>
										<option value="Novembre">Novembre</option>
										<option value="Decembre">Décembre</option>
									</select>
								</span><input class="input-date-annee" type="text" name="txt_date_exp_annee_<?php echo $i; ?>" id="txt_date_exp_annee_<?php echo $i; ?>" placeholder="Année">
							</p>
	
							<p>
								<label for="txt_nationalite_<?php echo $i; ?>">Nationalité :</label><br/>
								<input type="text" name="txt_nationalite_<?php echo $i; ?>" id="txt_nationalite_<?php echo $i; ?>">
							</p>
							
							<p>
								<label for="numero_permis_conduire_<?php echo $i; ?>">Permis de conduire :</label><br/>
								Pour les voyages qui nécessitent des voitures louées (Floride), j'ai indiqué auparavant que je suis intéressé(e) à conduire une des voitures de location, j’ai un permis de conduire automobile valide, en voici le numéro :
							</p>
	
							<p>
								<input type="text" name="numero_permis_conduire_<?php echo $i; ?>" id="numero_permis_conduire_<?php echo $i; ?>" placeholder="Numéro permis de conduire">
							</p>
	
							<p>
								<label for="intolerances_<?php echo $i; ?>">Intolérances alimentaires, allergies, etc. :</label><br/>
								<textarea name="intolerances_<?php echo $i; ?>" id="intolerances_<?php echo $i; ?>"></textarea>
							</p>
	
							<p>
								<label for="maladies_<?php echo $i; ?>">Maladies connues et médicaments qui mériteraient d’être connus de l’accompagnateur :</label><br/>
								<textarea name="maladies_<?php echo $i; ?>" id="maladies_<?php echo $i; ?>"></textarea>
							</p>
	
							<p>
								<label>Je souffre de vertige (Équateur) :</label><br/>
								<input type="radio" value="Oui je souffre de vertige" name="souffre_vertige_<?php echo $i; ?>" id="oui_souffre_vertige_<?php echo $i; ?>">
								<label class="radio-label" for="oui_souffre_vertige_<?php echo $i; ?>">Oui</label>
								<input type="radio" value="Non je souffre de vertige" name="souffre_vertige_<?php echo $i; ?>" id="non_souffre_vertige_<?php echo $i; ?>">
								<label class="radio-label" for="non_souffre_vertige_<?php echo $i; ?>">Non</label>
							</p>
	
							<p>
								<label for="pointure_de_botte_<?php echo $i; ?>">Ma pointure de bottes est (Équateur) :</label><br/>
								<input type="text" name="pointure_de_botte_<?php echo $i; ?>" id="pointure_de_botte_<?php echo $i; ?>" placeholder="Pointure de botte">
							</p>
	
							<p>
								<label for="interet_oiseaux_annee_<?php echo $i; ?>">Je m’intéresse aux oiseaux depuis :</label><br/>
								<input type="number" name="interet_oiseaux_annee_<?php echo $i; ?>" id="interet_oiseaux_annee_<?php echo $i; ?>" placeholder="Nombre d'années">
							</p>
	
							<p>
								<label>Je me considère :</label><br/>
								<input type="radio" value="Débutant" name="je_me_considere_<?php echo $i; ?>" id="je_me_considere_debutant_<?php echo $i; ?>">
								<label class="radio-label" for="je_me_considere_debutant_<?php echo $i; ?>">Débutant</label>
								<input type="radio" value="Intermédiaire" name="je_me_considere_<?php echo $i; ?>" id="je_me_considere_intermediaire_<?php echo $i; ?>">
								<label class="radio-label" for="je_me_considere_intermediaire_<?php echo $i; ?>">Intermédiaire</label>
								<input type="radio" value="Expérimenté" name="je_me_considere_<?php echo $i; ?>" id="je_me_considere_experimente_<?php echo $i; ?>">
								<label class="radio-label" for="je_me_considere_experimente_<?php echo $i; ?>">Expérimenté</label>
							</p>
	
							<p>
								<label>Mon intérêt est principalement :</label><br/>
								<input type="checkbox" value="Observation" name="interet_observation_<?php echo $i; ?>" id="interet_observation_<?php echo $i; ?>">
								<label class="radio-label" for="interet_observation_<?php echo $i; ?>">L’observation</label><br/>
								<input type="checkbox" value="Voir le plus d’espèces possible" name="interet_especes_<?php echo $i; ?>" id="interet_especes_<?php echo $i; ?>">
								<label class="radio-label" for="interet_especes_<?php echo $i; ?>">Voir le plus d’espèces possible</label><br/>
								<input type="checkbox" value="Vivre une expérience riche en nature et je suis plutôt de style contemplatif" name="interet_experience_<?php echo $i; ?>" id="interet_experience_<?php echo $i; ?>">
								<label class="radio-label" for="interet_experience_<?php echo $i; ?>">Vivre une expérience riche en nature et je suis plutôt de style contemplatif</label><br/>
								<input type="checkbox" value="Faire de la photo" name="interet_photo_<?php echo $i; ?>" id="interet_photo_<?php echo $i; ?>">
								<label class="radio-label" for="interet_photo_<?php echo $i; ?>">Faire de la photo</label>
							</p>
	
							<p>
								<label>J’apporterai un télescope :</label><br/>
								<input type="radio" value="Lunettes oui" name="lunettes_<?php echo $i; ?>" id="lunettes_oui_<?php echo $i; ?>">
								<label class="radio-label" for="lunettes_oui_<?php echo $i; ?>">Oui</label>
								<input type="radio" value="Lunettes non" name="lunettes_<?php echo $i; ?>" id="lunettes_non_<?php echo $i; ?>">
								<label class="radio-label" for="lunettes_non_<?php echo $i; ?>">Non</label>
							</p>
	
							<p>
								<label id="regroupement_quebec_oiseaux_reponse_<?php echo $i; ?>_label">Je suis membre du Regroupement Québec Oiseaux * :</label><br/>
								<input type="radio" value="oui" name="regroupement_quebec_oiseaux_reponse_<?php echo $i; ?>" id="regroupement_quebec_oiseaux_oui_<?php echo $i; ?>">
								<label class="radio-label" for="regroupement_quebec_oiseaux_oui_<?php echo $i; ?>">Oui</label>
								<input type="radio" value="nom" name="regroupement_quebec_oiseaux_reponse_<?php echo $i; ?>" id="regroupement_quebec_oiseaux_non_<?php echo $i; ?>" checked="">
								<label class="radio-label" for="regroupement_quebec_oiseaux_non_<?php echo $i; ?>">Non</label>
							</p>

							<p>
								<label for="numero_membre_regroupement_<?php echo $i; ?>">Si oui, mon numéro est :</label><br/>
								<input type="text" name="numero_membre_regroupement_<?php echo $i; ?>" id="numero_membre_regroupement_<?php echo $i; ?>">
							</p>
	
	
						</div>
					<?php } ?>

					<div>
						<h3>En cas d'urgence :</h3>
	
						<p>
							<label for="personne_urgence">Personne à contacter en cas d'urgence :</label><br/>
							<input type="text" name="personne_urgence" id="personne_urgence">
						</p>

						<p>
							<label for="urgence_phone">Téléphone :</label><br/>
							<input type="tel" name="urgence_phone" id="urgence_phone">
						</p>

						<h3>Assurances :</h3>

						<p>
							<label>Nous vous conseillons fortement d'avoir une assurance voyage.</label><br/>
							<input type="radio" value="Assurance oui" name="assurance" id="assurance1">
							<label class="radio-label" for="assurance1"><strong>OUI</strong>, je veux souscrire une assurance voyage (frais médicaux, annulation, bagages...). </label><br/>
							<span><em>Un(e) conseiller(ère) Groupe Voyages VP prendra contact avec vous par téléphone.</em></span>
						</p>
						<p>
							<input type="radio" value="Assurance non" name="assurance" id="assurance2">
							<label class="radio-label" for="assurance2"><strong>NON</strong>, je décline l'offre d'assurance voyage. </label><br/>
							<span><em>Un <strong>formulaire</strong> de renonciation vous sera envoyé par courriel.</em></span>
						</p>

						<h3>Paiement</h3>
	
						<p>
							<label id="montant_depot2" for="montant_depot">Montant du dépôt * :</label><br/>
							<input type="text" name="montant_depot" id="montant_depot">
						</p>

    					<p>
    						<label>Rabais</label><br/>
    						<em>Les montants des rabais seront indiqués sur votre facture et déduits du montant final (et non au moment du dépôt).</em>
    					</p>
    					<p>
							<input type="checkbox" value="Rabais cheque" name="rabais_cheque" id="rabais_cheque">
							<label class="radio-label" for="rabais_cheque">Un rabais m'est offert si je paye la totalité de mon voyage par chèque (dépôt et paiement final)</label><br/>
							<input type="checkbox" value="Rabais RQO" name="rabais_rqo" id="rabais_rqo">
							<label class="radio-label" for="rabais_rqo">Un rabais m'est offert si je suis membre du Regroupement Québec Oiseaux.</label><br/>
    					</p>
	
						<p>
							<label id="mode_paiement">Mode de paiement * :</label><br/>
							<input type="radio" value="cheque" name="paiement" id="cheque" onClick="show_payment_option()">
							<label class="radio-label" onClick="show_payment_option()" for="cheque">Paiement par chèque</label>
							<input type="radio" value="credit" name="paiement" id="credit" onClick="show_payment_option()">
							<label class="radio-label" onClick="show_payment_option()" for="credit">Paiement par carte de crédit</label>
						</p>

						<div style="display: none;" id="paiement_cheque">
    						<h3>Paiement par chèque</h3>
    						<p>Veuillez envoyer votre chèque à :</p>
    						<p><strong>Groupe Voyages VP</strong><br>
    						355 rue Sainte Catherine Ouest, bureau 601<br>
    						Montréal, Qc<br>
    						H3B 1A5</p>
    					</div>

    					<div style="display: none;" id="paiement_credit">
    						<h3>Paiement par carte de crédit</h3>

    						<p>
    							<em>Le montant total vous sera confirmé avant le prélèvement.</em>
    						</p>
    						
							<p>
								<label id="">Voyageur #1</label><br/>
								<input type="radio" value="visa" name="carte" id="visa">
								<label class="radio-label" for="visa"><img src="<?php echo get_template_directory_uri(); ?>/images/visa.gif" alt="visa" /></label>
								<input type="radio" value="mastercard" name="carte" id="mastercard">
								<label class="radio-label" for="mastercard"><img src="<?php echo get_template_directory_uri(); ?>/images/master.gif" alt="mastercard" /></label>
							</p>
	
							<p>
								<label id="cc_numero_label" for="cc_numero">Numéro de la carte * :</label><br/>
								<input type="text" name="cc_numero" id="cc_numero">
							</p>
	
							<p>
								<label id="cc_titulaire_label" for="cc_titulaire">Titulaire de la carte * :</label><br/>
								<input type="text" name="cc_titulaire" id="cc_titulaire">
							</p>

							<p class="input-date">
								<label id="cc_mois_label">Expiration de la carte * :</label><br/><span class="select-gauche">
								<select class="cardcredit" name="cc_mois" id="cc_mois">
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select></span><span class="select-droite">
								<select class="cardcredit" name="cc_annee" id="cc_annee">
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
								</select></span>
							</p>
	
							<p>
								<label id="cc_code_securite_label" for="cc_code_securite">Code de sécurité de la carte * :</label><br/>
								<input type="text" name="cc_code_securite" id="cc_code_securite">
							</p>

							<p id="chkUse2ndCCparent" style="display: none;">
								<input type="checkbox" name="chkUse2ndCC" id="chkUse2ndCC" value="Oui" onclick="toggle_payment_2();">
								<label class="radio-label" onclick="toggle_payment_2();" for="chkUse2ndCC">Permettre l'utilisation d'une seconde carte de crédit.</label>
							</p>


							<div id="cc_voyageur2" style="display: none;">

								<p>
									<label>Carte de crédit #2</label><br/>
									<input type="radio" value="visa voyageur 2" name="carte_2" id="visa_2">
									<label class="radio-label" for="visa_2"><img src="<?php echo get_template_directory_uri(); ?>/images/visa.gif" alt="visa" /></label>
									<input type="radio" value="mastercard voyageur 2" name="carte_2" id="mastercard_2">
									<label class="radio-label" for="mastercard_2"><img src="<?php echo get_template_directory_uri(); ?>/images/master.gif" alt="mastercard" /></label>
								</p>
		
								<p>
									<label for="cc_numero_2">Numéro de la carte * :</label><br/>
									<input type="text" name="cc_numero_2" id="cc_numero_2">
								</p>
		
								<p>
									<label for="cc_titulaire_2">Titulaire de la carte * :</label><br/>
									<input type="text" name="cc_titulaire_2" id="cc_titulaire_2">
								</p>
	
								<p class="input-date">
									<label>Expiration de la carte * :</label><br/><span class="select-gauche">
									<select class="cardcredit" name="cc_mois_2" id="cc_mois_2">
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select></span><span class="select-droite">
									<select class="cardcredit" name="cc_annee_2" id="cc_annee_2">
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
									</select></span>
								</p>
		
								<p>
									<label for="cc_code_securite_2">Code de sécurité de la carte * :</label><br/>
									<input type="text" name="cc_code_securite_2" id="cc_code_securite_2">
								</p>

							</div>

							<p id="accepttext">
								J'accepte que la carte de crédit ci-dessus soit débitée à parts égales pour * :
							</p>

							<p>
								<input type="radio" value="depot" name="credit_debiter" id="credit_debiter_depot">
								<label class="radio-label" for="credit_debiter_depot">Le dépôt</label>
								<input type="radio" value="depot et paiement final" name="credit_debiter" id="credit_debiter_depot_paiement">
								<label class="radio-label" for="credit_debiter_depot_paiement">Le dépôt et le paiement final</label>
							</p>


    					</div>

						<p>
							<input type="checkbox" id="lu_politique" name="lu_politique">
							<label class="radio-label" id="lu_politique2">J'ai lu et j'accepte les <a class="lien-bleu" target="_blank" href="<?php echo get_permalink(8696); ?>">conditions générales de Groupe Voyages VP.</a> *</label>
						</p>
	
						<p>
							<label for="txt_comments">Commentaires ou précisions:</label><br/>
							<textarea name="txt_comments" id="txt_comments" rows="5"></textarea>
						</p>

						<p>
							<input type="button" value="Soumettre" id="subm-btn" class="btn btn-contact">
						</p>

						<p>
							<span style="display:block; color:red; visibility:hidden;" id="error-span">Veuillez corrigez les champs requis afin de soumettre le formulaire</span>
						</p>

						<p class="text-sous-submit">Les champs marqués d'un * sont requis.</p>

						<p>
							<em>Sujet à disponibilité. Cette réservation vous sera confirmée par votre conseiller Groupe Voyages VP.</em>
						</p>

					</div>


					</form>
					<?php }else{ ?>

						<?php the_field('message_aucun_voyage'); ?>

					<?php } ?>
					
				</div>
			</div>


		</div>
	</div>
</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>