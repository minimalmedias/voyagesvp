<?php
/**
 * Template Name: Page - Équipe
 * The template used for displaying page content in page.php
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

		<?php //if ( function_exists('yoast_breadcrumb') ) {
		    //yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		//} ?>
		
<?php
	$urlImage = "";
	$image = get_field('image-banner');
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
		<section class="blue" <?php if(!empty($image)){ ?>style="<?php echo $urlImage; ?>" <?php } ?>>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="text-center"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</section>

		

	    <?php //the_content(); ?>
	    <?php endwhile; // end of the loop. ?>
	    
	    <?php if( have_rows('equipe') ): $i=0; ?>
		    <?php while( have_rows('equipe') ): the_row(); ?>

		    <section class="<?php if ($i % 2 == 0) {echo "lightgray";} else {echo "gray";} ?>">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">

						    <h2 class="text-center"><?php the_sub_field("nom_de_lequipe");?></h2>


							    <?php if( have_rows('auteurs_(membres_de_lequipe)') ): ?>
							    <div class="row">
								    <?php while( have_rows('auteurs_(membres_de_lequipe)') ): the_row(); 
									
								    // get this page's specified author
								    $author = get_sub_field("membre");
								    // find ID for ACF
								    $authorid = $author["ID"];

									// vars
									$photo = get_field("photo", "user_".$authorid."");
								    $nom_complet = get_field("nom_complet",  "user_".$authorid."");
								    
								    if ( langWp('en')) {
								    	$titre = get_field("titre-an",  "user_".$authorid."");
								    }	
								    else {
								    	$titre = get_field("titre",  "user_".$authorid."");
								    }


								    $lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");
									
								    ?>

								    <div class="col-xs-6 col-sm-4 col-md-3 text-center boite membre-equipe">
								    <?php if ( !langWp('en')) {	?>
								    	<a href="<?php echo $lien_vers_la_page_de_profil; ?>">
								    <?php } ?>
								    		<img src="<?php echo $photo["url"]; ?>" alt="<?php echo $photo["alt"]; ?>">
									    	<p class="nom"><?php echo $nom_complet; ?></p>
									    	<p class="titre"><small><?php echo $titre; ?></small></p>
									    	<img src="<?php echo get_template_directory_uri(); ?>/images/hr.png" class="hr">
									<?php if ( !langWp('en')) {	 ?>
								    	</a>
								    <?php } ?>
									  
								    </div>

								    <?php endwhile; ?>
								</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</section>

			<?php $i++; endwhile; ?>
			<?php endif; ?>

	
<?php get_footer(); ?>