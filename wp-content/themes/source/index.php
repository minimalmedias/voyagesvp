<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */

get_header(); ?>
<?php
	$urlImage = "";
	$image = get_field('image-banner',20);
	if($image){
		$urlImage = "background-image: url(" . $image['url'] . ");";
	}
?>
<section class="blue" <?php if(!empty($image)){ ?>style="<?php echo $urlImage; ?>" <?php } ?>>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center">Le blogue de Groupe Voyages VP</h1>
			</div>
		</div>
	</div>
</section>

<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">

			<?php /*<div class="col-sm-12">
				<h1 class="page-title text-center">Le blogue de Groupe Voyages VP</h1>
			</div> */ ?>

			<div class="col-sm-8 col-sm-push-4">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
					<div class="row">
					<?php
						global $wp_query;
						$term = get_term_by( 'slug', 'videos', 'post_tag' );
						$args = array_merge( $wp_query->query_vars, array( 'tag__not_in' => array( $term->term_id ), 'posts_per_page' => '9' ) );
						query_posts( $args );
					?>
					<?php if ( have_posts() ) : ?>
					
						<?php while ( have_posts() ) : the_post(); ?>
			
							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part('content', get_post_format());
							?>
			
						<?php endwhile; ?>
			
						<?php upbootwp_content_nav('nav-below'); ?>
			
					<?php else : ?>
						<?php get_template_part( 'no-results', 'index' ); ?>
					<?php endif; 

					wp_reset_postdata();
					?>
					</div>
					</main>
				</div>
			</div>

			<div class="col-sm-4 col-sm-pull-8">
				<div class="formulaire-container">
					<?php get_sidebar(); ?>
				</div>
			</div>
			
			
		</div>
	</div>
</div>
<?php get_footer(); ?>
