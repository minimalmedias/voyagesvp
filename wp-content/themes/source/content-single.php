<?php
/**
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */
?>

<div class="col-sm-4 hidden-xs">
	<div class="formulaire-container">
		<?php get_sidebar(); ?>
	</div>
</div>

<div class="col-sm-8">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
	
			<div class="entry-meta">
				<div class="media authordate">
					<?php
					$authorid = get_the_author_meta( 'ID' );
					$lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");
					$photo = get_field("photo", "user_".$authorid."");
					 ?>
						<div class="media-left">
							<a href="<?php echo $lien_vers_la_page_de_profil; ?>"><?php 
								if ($photo) {
									echo "<img src='".$photo["url"]."' />";
								} else {
									echo get_avatar(get_the_author_meta( 'ID' )); 
								}
							?></a>
						</div>
						<div class="media-body">
							<div>Par <a href="<?php echo $lien_vers_la_page_de_profil; ?>"><?php echo get_the_author(); ?></a></div>
							<div><?php echo get_the_category_list(","); ?> - <?php echo get_the_date(); ?></div>
						</div>
						<div class="media-right">
							<p>
								<span class='st_email_large' displayText='Email'></span><span class='st_facebook_large' displayText='Facebook'></span><span class='st_pinterest_large' displayText='Print'></span><span class='st_sharethis_large' displayText='ShareThis'></span><span class='st_print_large' displayText='Print'></span>
		                    </p>
						</div>
	
						<script type="text/javascript">var switchTo5x=true;</script>
						<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
						<script type="text/javascript">stLight.options({publisher: "a9241dc9-5ad3-4372-b9b2-6a60d6d656d2", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
					
				</div>
			</div><!-- .entry-meta -->
		</header><!-- .entry-header -->
	
		<div class="entry-content">
			<h1 class="blogue-h1"><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</div><!-- .entry-content -->
		<p class="text-center">
			<span class='st_email_large' displayText='Email'></span><span class='st_facebook_large' displayText='Facebook'></span><span class='st_pinterest_large' displayText='Print'></span><span class='st_sharethis_large' displayText='ShareThis'></span><span class='st_print_large' displayText='Print'></span>
	    </p>
	</article><!-- #post-## -->
</div>
