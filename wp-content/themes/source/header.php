<!doctype html>  

<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>
			<?php /*if ( !is_front_page() ) { echo wp_title( ' ', true, 'left' ); echo ' | '; }
			echo bloginfo( 'name' ); echo ' - '; bloginfo( 'description', 'display' );*/
			global $wp_the_query;
			$id = $wp_the_query->get_queried_object_id();
			if ( $id == 257 || $id == 259 || $id == 261 || $id == 263 ){
				$url_h = $_SERVER['REQUEST_URI'];
				$tokens_h = explode('/', $url_h);
				$slug_h = $tokens_h[sizeof($tokens_h)-2];

				$echo_h = '';
				$pays_h = '';
				$theme_h = '';
				$compagnie_h = '';

				if( term_exists( $slug_h, 'pays' ) ){
					$pays_h = $slug_h;
				}
				else if( term_exists( $slug_h, 'theme' ) ){
					$theme_h = $slug_h;
				}
				else if( term_exists( $slug_h, 'compagnie' ) ){
					$compagnie_h = $slug_h;
				}

				if($pays_h){
					$pays_term_h = get_term_by( 'slug', $pays_h, 'pays' );
					$echo_h = $pays_term_h->name;
				}
				if($theme_h){
					$theme_term_h = get_term_by( 'slug', $theme_h, 'theme' );
					$echo_h = $theme_term_h->name;
				}
				if($compagnie_h){
					$compagnie_term_h = get_term_by( 'slug', $compagnie_h, 'compagnie' );
					$echo_h = $compagnie_term_h->name;
				}

				if($echo_h){
					echo $echo_h . ' - ';
				}

			}
			echo wp_title();  ?> 
		</title>
		
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />

		<link href='https://fonts.googleapis.com/css?family=Reenie+Beanie' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
		<?php //<link href='https://fonts.googleapis.com/css?family=Lora:400italic,700italic' rel='stylesheet' type='text/css'>?>
		
		<?php wp_head(); ?>

		<!--<link rel="stylesheet" type="text/css" href="<?php //echo get_template_directory_uri(); ?>/style.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

	    <?php if(get_field('code_google_analytic', 'option') != ""){ ?>
		<!-- Google Analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', '<?php the_field('code_google_analytic', 'option');?>', 'auto');
			ga('send', 'pageview');
			
			(function(){var t=function(e,t){t.length>0?window.open(e,t):window.location.href=e};if(document.getElementsByTagName){var n=document.getElementsByTagName("a"),r=document.domain.split(".").reverse()[1]+"."+document.domain.split(".").reverse()[0];for(var i=0;i<n.length;i++){var s=typeof n[i].getAttribute("href")=="string"?n[i].getAttribute("href"):"",o=s.match(r);(s.match(/^https?\:/i)&&!o||s.match(/^mailto\:/i))&&n[i].addEventListener("click",function(e){var n=this.getAttribute("href"),r=typeof (this.getAttribute("target")=="string")?this.getAttribute("target"):"";ga("send","event","outbound","click",n,{hitCallback:t(n,r)},{nonInteraction:1});e.preventDefault()})}}})();
			
		</script>
		<!-- End Google Analytics -->
	<?php } ?>

	</head>
	
	<body <?php body_class(); ?>>
		<div id="page" class="hfeed site">
			<?php do_action( 'before' ); ?>
			<div class="header-container">
				<header id="masthead" class="site-header" role="banner">
					<nav class="navbar" role="navigation">
					
						<div class="container">
							<div class="navbar-header">
							    <button type="button" class="navbar-toggle navbar-toggle-header" data-toggle="collapse" data-target=".navbar-collapse-header">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"  class="navbar-brand"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo<?php if(ICL_LANGUAGE_CODE=='en') {echo "-en";}?>.png" alt="<?php bloginfo( 'name' ); ?>" width="322" height="80" /></a>
							</div>
							<div class="header-droite">
								<div class="topmenu">
									<?php 
										$args = array('theme_location' => 'top', 'container_class' => 'topmenu2'); 
										wp_nav_menu($args);
									?>
									<?php if(get_field('telephone_1', 'option') || get_field('telephone_2', 'option')): ?>
										<div class="telephone-header" itemscope itemtype="http://schema.org/LocalBusiness">
<?php if(get_field('telephone_1', 'option')): ?><span itemprop="telephone"><?php the_field('telephone_1', 'option'); ?></span><?php endif; ?>
<?php if(get_field('telephone_1', 'option') && get_field('telephone_2', 'option')): ?><span>&nbsp;&nbsp;/&nbsp;&nbsp;</span><?php endif; ?>
<?php if(get_field('telephone_2', 'option')): ?><span><?php the_field('telephone_2', 'option'); ?></span><?php endif; ?>
										</div>
									<?php endif; ?>
								</div>
								<div class="navbar-collapse collapse navbar-collapse-header">
									<?php 
									$args = array('theme_location' => 'primary',
							    		  		  'container_class' => 'main-menu-container', 
									    		  'menu_class' => 'nav navbar-nav',
									    		  'fallback_cb' => '',
								                  'menu_id' => 'main-menu',
								                  'walker' => new Upbootwp_Walker_Nav_Menu()); 
									wp_nav_menu($args);
									?>
									<?php
										$languages = icl_get_languages('skip_missing=1');
										if(1 < count($languages) && true ){
											foreach($languages as $l){
												if(!$l['active']){
													$langs[] = '<a href="'.$l['url'].'">'.$l['native_name'].'</a>';
												}
											} ?>
											<div class="selecteur-langue">
												<?php echo join('<span> / </span>', $langs); ?>
											</div>
									<?php } ?>
								</div>
							</div>
						</div><!-- container -->
					</nav>
					
				</header><!-- #masthead -->
				<header class="site-header-2 hidden-xs">
					<div class="container">
						<?php 
						$args = array('theme_location' => 'sousmenu',
							  		  'container_class' => 'sous-menu-container', 
						    		  'menu_class' => 'sous-menu',
						    		  'fallback_cb' => '',
						              'menu_id' => 'sous-menu',
						              'walker' => new Upbootwp_Walker_Nav_Menu()); 
						wp_nav_menu($args);
						?>
					</div>
				</header>
				<?php $type_list = array(785, 786, 787, 788); ?>
				<?php for ($i = 1; $i <= 4; $i++) { ?>
				<div data-number="<?php echo $i; ?>" class="sous-menu-horizontal hidden-xs invisible" style="display:block;">
					<ul>

						<?php 

						$taxonomy_array = array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'type',
									'field'    => 'term_id',
									'terms'    => $type_list[ $i-1 ],
								)
							);
						$posts_array = get_posts(
						    array(
						        'posts_per_page' => -1,
						        'post_type' => 'voyages',
						        'tax_query' => $taxonomy_array,
						    )
						);

						$term_pays_list = array();
						$term_pays_list_slug = array();
						
						foreach ($posts_array as $post) {
							$term_list = wp_get_post_terms( $post->ID, 'pays' );
							foreach ($term_list as $term ) {
								if( $term->parent == 0 ){
									array_push($term_pays_list, $term->name);
									$term_pays_list_slug[$term->name] = $term->slug;
								}
							}
						}
	
						$terms = array();
						$terms_slug = array();
	
						foreach ($posts_array as $post) {
							$term_list = wp_get_post_terms( $post->ID, 'theme' );
							foreach ($term_list as $term ) {
								array_push($terms, $term->name);
								$terms_slug[$term->name] = $term->slug;
							}
						}

						$term_compagnie = array();
						$term_compagnie_slug = array();

						if($type_list[ $i-1 ] == 787){

							foreach ($posts_array as $post) {
								$term_list = wp_get_post_terms( $post->ID, 'compagnie' );
								foreach ($term_list as $term ) {
									array_push($term_compagnie, $term->name);
									$term_compagnie_slug[$term->name] = $term->slug;
								}
							}

						}

						$term_pays_list = array_unique($term_pays_list);
						asort($term_pays_list);

						$terms = array_unique($terms);
						asort($terms);

						$term_compagnie = array_unique($term_compagnie);
						asort($term_compagnie);

						?>
						<?php if( $term_pays_list ): ?>
							<li><span class="lien-pays active">Destinations</span></li>
						<?php endif; ?>
						<?php if( $terms ): ?>
							<li><span class="lien-theme">Thématiques</span></li>
						<?php endif; ?>
						<?php if( $term_compagnie ): ?>
							<li><span class="lien-compagnie">Compagnies de croisières</span></li>
						<?php endif; ?>
						<li><a class="lien-tous" href="#"><?php if( $i == 3 ){ echo "Toutes"; }else{ echo "Tous"; } ?></a></li>
					</ul>

					<div class="owl-carousel-horizontal-container">

					<?php if( $term_pays_list ){ ?>
						<div class="container owl-carousel-container carousel-pays carousel-size-<?php echo count( $term_pays_list ); ?>">
							<div class="owl-carousel-menu-horizontal">

								<?php 

								foreach ($term_pays_list as $term_pays ) { ?>

									<?php $name = $term_pays; ?>
									<?php $slug = $term_pays_list_slug[$name]; ?>
									<div class="item">
										<div class="boite-menu-horizontal">
											<a href="#" data-slug="<?php echo $slug; ?>" class="lien-boite">
												<?php 
													$img = get_field( 'image_destination', get_term_by( 'slug', $slug, 'pays' ) );
													if( $img ){
													$thumb = $img['sizes']['thumb-menu-horizontal']; ?>
													<img src="<?php echo $thumb; ?>" alt="<?php echo $name; ?>" />
												<?php }else{ ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/PlaceHolderDestination.jpg" alt="<?php echo $name; ?>" />
												<?php } ?>
												<span><?php echo $name; ?></span>
											</a>
										</div>
									</div>

								<?php } ?>

							</div>

							<a class="owl-bas-prev btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-prev.png" alt="prev" /></a>
							<a class="owl-bas-next btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" alt="next" /></a>

						</div>
					<?php } ?>

					<?php if( $terms ){ ?>

						<div class="container owl-carousel-container carousel-theme carousel-size-<?php echo count( $terms ); ?> invisible" style="height:0px; overflow:hidden;">
							<div class="owl-carousel-menu-horizontal">

							<?php foreach($terms as $term) { ?>
								
								<?php $name = $term; ?>
								<?php $slug = $terms_slug[$name]; ?>
								<div class="item">
									<div class="boite-menu-horizontal">
										<a href="#" data-slug="<?php echo $slug; ?>" class="lien-boite">
											<?php 
												$img = get_field( 'image_destination', get_term_by( 'slug', $slug, 'theme' ) );
												if( $img ){
												$thumb = $img['sizes']['thumb-menu-horizontal']; ?>
												<img src="<?php echo $thumb; ?>" alt="<?php echo $name; ?>" />
											<?php }else{ ?>
												<img src="<?php echo get_template_directory_uri(); ?>/images/PlaceHolderDestination.jpg" alt="<?php echo $name; ?>" />
											<?php } ?>
											<span><?php echo $name; ?></span>
										</a>
									</div>
								</div>

							<?php } ?>

							</div>
	
							<a class="owl-bas-prev btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-prev.png" alt="prev" /></a>
							<a class="owl-bas-next btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" alt="next" /></a>
	
						</div>

					<?php } ?>

					<?php if( $term_compagnie ){ ?>

						<div class="container owl-carousel-container carousel-compagnie carousel-size-<?php echo count( $term_compagnie ); ?> invisible" style="height:0px; overflow:hidden;">
							<div class="owl-carousel-menu-horizontal">

							<?php foreach($term_compagnie as $term) { ?>
								
								<?php $name = $term; ?>
								<?php $slug = $term_compagnie_slug[$name]; ?>
								<div class="item">
									<div class="boite-menu-horizontal">
										<a href="#" data-slug="<?php echo $slug; ?>" class="lien-boite">
											<?php 
												$img = get_field( 'image_destination', get_term_by( 'slug', $slug, 'compagnie' ) );
												if( $img ){
												$thumb = $img['sizes']['thumb-menu-horizontal']; ?>
												<img src="<?php echo $thumb; ?>" alt="<?php echo $name; ?>" />
											<?php }else{ ?>
												<img src="<?php echo get_template_directory_uri(); ?>/images/PlaceHolderDestination.jpg" alt="<?php echo $name; ?>" />
											<?php } ?>
											<span><?php echo $name; ?></span>
										</a>
									</div>
								</div>

							<?php } ?>

							</div>
	
							<a class="owl-bas-prev btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-prev.png" alt="prev" /></a>
							<a class="owl-bas-next btn"><img src="<?php echo get_template_directory_uri(); ?>/images/Arrow-white-next.png" alt="next" /></a>
	
						</div>

					<?php } ?>

					</div>

				</div>
				<?php } ?>
				<?php wp_reset_query(); ?>
			</div>
		
			<div id="content" class="site-content">
			
				<?php //Banner ?>
				<?php //get_template_part( 'banner' ); ?> 
