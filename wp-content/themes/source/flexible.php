<?php while(has_sub_field("contenu_flexible")): ?>
    <?php if(get_row_layout() == "une_colonne"):  ?>
    	<section class="section">
			<div class="container">
				<div class="row">
		    		<div class="col-xs-12">
		    			<?php the_sub_field("contenu"); ?>
		    		</div>
		    	</div>
			</div>
    	</section>
    <?php elseif(get_row_layout() == "multi_colonnes"): ?>
    	<section class="section">
			<div class="container">
		    	<div class="row">
		    		<?php if( have_rows('colonne') ): ?>
		    			<?php while( have_rows('colonne') ): the_row(); ?>
		    				<div class="col-<?php the_sub_field('point_de_rupture'); ?>-<?php the_sub_field('largeur_de_la_colonne'); ?>">
		    					<?php the_sub_field('contenu'); ?>
		    				</div>
		    			<?php endwhile; ?>
		    		<?php endif; ?>
		    	</div>
			</div>
    	</section>
    <?php endif; ?>
<?php endwhile; ?>