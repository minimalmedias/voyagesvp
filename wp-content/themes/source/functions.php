<?php
/**
 *
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */

if (!isset($content_width)) $content_width = 770;

/**
 * upbootwp_setup function.
 * 
 * @access public
 * @return void
 */
function upbootwp_setup() {

	require 'inc/general/class-Upbootwp_Walker_Nav_Menu.php';

	load_theme_textdomain('upbootwp', get_template_directory().'/languages');

	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'Bootstrap WP Primary' ),
		'top' => __( 'Top Menu', 'Bootstrap WP Top' ),
		'sousmenu' => __( 'Sous Menu', 'Bootstrap WP Sous Menu' ),
		'footer' => __( 'Footer Menu', 'Bootstrap WP Footer' ),
		'footer2' => __( 'Footer Menu 2', 'Bootstrap WP Footer 2' ),
		'footer3' => __( 'Footer Menu 3', 'Bootstrap WP Footer 3' ),
		'footer4' => __( 'Footer Menu 4', 'Bootstrap WP Footer 4' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	add_theme_support( 'custom-background', apply_filters( 'upbootwp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	)));
	
	
}

add_action( 'after_setup_theme', 'upbootwp_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function upbootwp_widgets_init() {
	register_sidebar(array(
		'name'          => __('Sidebar','upbootwp'),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
}
add_action( 'widgets_init', 'upbootwp_widgets_init' );

function upbootwp_scripts() {
	/* use this for standard CSS */
	wp_enqueue_style( 'upbootwp-css', get_template_directory_uri().'/css/bootstrap.min.css', array(), '20130908');
	wp_enqueue_style( 'owlcarousel-css', get_template_directory_uri().'/css/owl.carousel.css', array(), '20130908');
	wp_enqueue_style( 'prettyphoto', get_template_directory_uri().'/css/prettyPhoto.css', array(), '20130908');
	wp_enqueue_style( 'style', get_template_directory_uri().'/style.css', array(), '20130908');

	/* use this for LESS */
	//wp_enqueue_style( 'style', get_template_directory_uri().'/style.less', array(), '20130908'); This does not work, adds the file before bootstrap.less, so it does not overwrite any styles. Moved to upbootwp_less until we find a better solution.

	/* dem scripts */
	//wp_enqueue_script( 'upbootwp-jQuery', get_template_directory_uri().'/js/jquery.js',array(),'2.0.3',true);
	wp_enqueue_script( 'upbootwp-jQuery', get_template_directory_uri().'/js/jquery.js',array(),'1.11.3',true);
	wp_enqueue_script( 'upbootwp-basefile', get_template_directory_uri().'/js/bootstrap.min.js',array(),'20130905',true);
	wp_enqueue_script( 'prettyPhoto', get_template_directory_uri().'/js/jquery.prettyPhoto.min.js',array(),'20130905',true);
	wp_enqueue_script( 'owlcarousel-js', get_template_directory_uri().'/js/owl.carousel.js',array(),'20130905',true);
	wp_enqueue_script( 'matchheight', get_template_directory_uri().'/js/jquery.matchHeight.js',array(),'20130905',true);
	wp_enqueue_script( 'custom', get_template_directory_uri().'/js/custom.js',array(),'20130905',true);
}
add_action( 'wp_enqueue_scripts', 'upbootwp_scripts');


/**
 * upbootwp_less function.
 * Load less for development or even on the running website. If you want to use less just enable this function
 * @access public
 * @return void
 */
function upbootwp_less() {
	printf('<link rel="stylesheet" type="text/less" href="%s" />', get_template_directory_uri().'/less/bootstrap.less?ver=0.1'); // raus machen :) 
	printf('<link rel="stylesheet" type="text/less" href="%s" />', get_template_directory_uri().'/style.less');
	printf('<script type="text/javascript" src="%s"></script>', get_template_directory_uri().'/js/less.js');
}
/* Enable this when you want to work with less */
//add_action('wp_head', 'upbootwp_less');



/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory().'/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory().'/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory().'/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory().'/inc/jetpack.php';


/**
 * upbootwp_breadcrumbs function.
 * Edit the standart breadcrumbs to fit the bootstrap style without producing more css
 * @access public
 * @return void
 */
function upbootwp_breadcrumbs() {

	$delimiter = '&raquo;';
	$home = 'Home';
	$before = '<li class="active">';
	$after = '</li>';

	if (!is_home() && !is_front_page() || is_paged()) {

		echo '<ol class="breadcrumb">';

		global $post;
		$homeLink = get_bloginfo('url');
		echo '<li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li> ';

		if (is_category()) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
			echo $before . single_cat_title('', false) . $after;

		} elseif (is_day()) {
			echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_time('d') . $after;

		} elseif (is_month()) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_time('F') . $after;

		} elseif (is_year()) {
			echo $before . get_the_time('Y') . $after;

		} elseif (is_single() && !is_attachment()) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
				echo $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				echo $before . get_the_title() . $after;
			}

		} elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;

		} elseif (is_attachment()) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && !$post->post_parent ) {
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;

		} elseif ( is_search() ) {
			echo $before . 'Search results for "' . get_search_query() . '"' . $after;

		} elseif ( is_tag() ) {
			echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo $before . 'Articles posted by ' . $userdata->display_name . $after;

		} elseif ( is_404() ) {
			echo $before . 'Error 404' . $after;
		}

		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo ': ' . __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</ol>';

	}
}

define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

/* core updates, disable dev+night // also enable plugin updates */
add_filter( 'auto_update_plugin', '__return_true' );

add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
add_image_size( 'new-size', 767, 341, true ); //(cropped)
add_image_size( 'thumbvideo1', 370, 230, true ); //(cropped)
add_image_size( 'galeriephoto', 270, 179, true ); //(cropped)
}
add_filter('image_size_names_choose', 'my_image_sizes');
function my_image_sizes($sizes) {
$addsizes = array(
"new-size" => __( "New Size")
);
$newsizes = array_merge($sizes, $addsizes);
return $newsizes;
}

add_filter( 'get_image_tag', 'remove_width_and_height_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_width_and_height_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_and_height_attribute', 10 );

function remove_width_and_height_attribute( $html ) {
   return preg_replace( '/(height|width)="\d*"\s/', "", $html );
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/*
class WP_HTML_Compression
{
	// Settings
	protected $compress_css = true;
	protected $compress_js = true;
	protected $info_comment = true;
	protected $remove_comments = true;

	// Variables
	protected $html;
	public function __construct($html)
	{
		if (!empty($html))
		{
			$this->parseHTML($html);
		}
	}
	public function __toString()
	{
		return $this->html;
	}
	protected function bottomComment($raw, $compressed)
	{
		$raw = strlen($raw);
		$compressed = strlen($compressed);
		
		$savings = ($raw-$compressed) / $raw * 100;
		
		$savings = round($savings, 2);
		
		return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
	}
	protected function minifyHTML($html)
	{
		$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
		preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
		$overriding = false;
		$raw_tag = false;
		// Variable reused for output
		$html = '';
		foreach ($matches as $token)
		{
			$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
			
			$content = $token[0];
			
			if (is_null($tag))
			{
				if ( !empty($token['script']) )
				{
					$strip = $this->compress_js;
				}
				else if ( !empty($token['style']) )
				{
					$strip = $this->compress_css;
				}
				else if ($content == '<!--wp-html-compression no compression-->')
				{
					$overriding = !$overriding;
					
					// Don't print the comment
					continue;
				}
				else if ($this->remove_comments)
				{
					if (!$overriding && $raw_tag != 'textarea')
					{
						// Remove any HTML comments, except MSIE conditional comments
						$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
					}
				}
			}
			else
			{
				if ($tag == 'pre' || $tag == 'textarea')
				{
					$raw_tag = $tag;
				}
				else if ($tag == '/pre' || $tag == '/textarea')
				{
					$raw_tag = false;
				}
				else
				{
					if ($raw_tag || $overriding)
					{
						$strip = false;
					}
					else
					{
						$strip = true;
						
						// Remove any empty attributes, except:
						// action, alt, content, src
						$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
						
						// Remove any space before the end of self-closing XHTML tags
						// JavaScript excluded
						$content = str_replace(' />', '/>', $content);
					}
				}
			}
			
			if ($strip)
			{
				$content = $this->removeWhiteSpace($content);
			}
			
			$html .= $content;
		}
		
		return $html;
	}
		
	public function parseHTML($html)
	{
		$this->html = $this->minifyHTML($html);
		
		if ($this->info_comment)
		{
			$this->html .= "\n" . $this->bottomComment($html, $this->html);
		}
	}
	
	protected function removeWhiteSpace($str)
	{
		$str = str_replace("\t", ' ', $str);
		$str = str_replace("\n",  '', $str);
		$str = str_replace("\r",  '', $str);
		
		while (stristr($str, '  '))
		{
			$str = str_replace('  ', ' ', $str);
		}
		
		return $str;
	}
}

function wp_html_compression_finish($html)
{
	return new WP_HTML_Compression($html);
}

function wp_html_compression_start()
{
	ob_start('wp_html_compression_finish');
}
add_action('get_header', 'wp_html_compression_start'); */

add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin( $show ) {
    
    return current_user_can('manage_options');
    
}


add_action( 'init', 'create_type_hierarchical_taxonomy', 0 );

function create_type_hierarchical_taxonomy() {

	$labels = array(

		'name' => _x( 'Types de voyage', 'taxonomy general name' ),
		'singular_name' => _x( 'Type de voyage', 'taxonomy singular name' ),
		'search_items' =>  __( 'Chercher parmis les type de voyage' ),
		'all_items' => __( 'Tous les types de voyage' ),
		'parent_item' => __( 'Type parent' ),
		'parent_item_colon' => __( 'Type parent:' ),
		'edit_item' => __( 'Modifier type' ),
		'update_item' => __( 'Mettre à jour' ),
		'add_new_item' => __( 'Ajouter' ),
		'new_item_name' => __( 'Nouveau type de voyage' ),
		'menu_name' => __( 'Type de voyage' ),

	);   

	register_taxonomy('type',array(), array(

		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'type' ),

	));
}


add_action( 'init', 'create_pays_hierarchical_taxonomy', 0 );

function create_pays_hierarchical_taxonomy() {

	$labels = array(

		'name' => _x( 'Pays', 'taxonomy general name' ),
		'singular_name' => _x( 'Pays', 'taxonomy singular name' ),
		'search_items' =>  __( 'Chercher parmis les pays' ),
		'all_items' => __( 'Tous les pays' ),
		'parent_item' => __( 'Pays parent' ),
		'parent_item_colon' => __( 'Pays parent:' ),
		'edit_item' => __( 'Modifier pays' ),
		'update_item' => __( 'Mettre à jour' ),
		'add_new_item' => __( 'Ajouter' ),
		'new_item_name' => __( 'Nouveau pays' ),
		'menu_name' => __( 'Pays' ),

	);   

	register_taxonomy('pays',array(), array(

		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'pays' ),

	));
}


add_action( 'init', 'create_theme_hierarchical_taxonomy', 0 );

function create_theme_hierarchical_taxonomy() {

	$labels = array(

		'name' => _x( 'Thèmes', 'taxonomy general name' ),
		'singular_name' => _x( 'Thème', 'taxonomy singular name' ),
		'search_items' =>  __( 'Chercher parmis les thèmes' ),
		'all_items' => __( 'Tous les thèmes' ),
		'parent_item' => __( 'Thème parent' ),
		'parent_item_colon' => __( 'Thèmes parent:' ),
		'edit_item' => __( 'Modifier thème' ),
		'update_item' => __( 'Mettre à jour' ),
		'add_new_item' => __( 'Ajouter' ),
		'new_item_name' => __( 'Nouveau thème' ),
		'menu_name' => __( 'Thèmes' ),

	);   

	register_taxonomy('theme',array(), array(

		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'theme' ),

	));
}


add_action( 'init', 'create_compagnie_hierarchical_taxonomy', 0 );

function create_compagnie_hierarchical_taxonomy() {

	$labels = array(

		'name' => _x( 'Compagnies', 'taxonomy general name' ),
		'singular_name' => _x( 'Compagnie', 'taxonomy singular name' ),
		'search_items' =>  __( 'Chercher parmis les compagnies' ),
		'all_items' => __( 'Toutes les compagnies' ),
		'parent_item' => __( 'Compagnie parent' ),
		'parent_item_colon' => __( 'Compagnies parent:' ),
		'edit_item' => __( 'Modifier compagnie' ),
		'update_item' => __( 'Mettre à jour' ),
		'add_new_item' => __( 'Ajouter' ),
		'new_item_name' => __( 'Nouvelle compagnie' ),
		'menu_name' => __( 'Compagnies' ),

	);   

	register_taxonomy('compagnie',array(), array(

		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'compagnie' ),

	));
}

function my_custom_post_blog() {
  $labels = array(
    'name'               => _x( 'Voyages', 'post type general name', 'source' ),
    'singular_name'      => _x( 'Voyage', 'post type singular name', 'source' ),
    'add_new'            => _x( 'Ajouter', 'book' ),
    'add_new_item'       => __( 'Ajouter un voyage' ),
    'edit_item'          => __( 'Modifier cette fiche de voyage' ),
    'new_item'           => __( 'Nouveau' ),
    'all_items'          => __( 'Tous les voyages' ),
    'view_item'          => __( 'Afficher la fiche de voyage' ),
    'search_items'       => __( 'Chercher parmis les voyages' ),
    'not_found'          => __( 'Aucun voyage trouvée' ),
    'not_found_in_trash' => __( 'Aucun voyage trouvée dans la corbeille' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Voyages'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Voyages',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'taxonomies'    => array( 'pays', 'theme', 'type', 'compagnie' ),
    'has_archive'   => true,
  );
  register_post_type( 'voyages', $args );
}
add_action( 'init', 'my_custom_post_blog' );


add_action( 'admin_menu', 'decoder_cc_register' );

function decoder_cc_register()
{
    add_menu_page(
        'Décodeur CC',     // page title
        'Décodeur CC',     // menu title
        'manage_options',   // capability
        'decodeur-CC',     // menu slug
        'decoder_cc_render' // callback function
    );
}
function decoder_cc_render()
{
    global $title;

    $secretPass = 'kljhflk73';
    $code = '';
    if(isset($_POST['decoder']))
    {
        $code = $_POST['decoder'];   
    }

    //TMP TO TEST ENCODING
    /*$completeNumber = '2016'.'10'.'4530123445677890'.'_123';
    
    $encodedNumber = bin2hex(cc_Encode($completeNumber,$secretPass));*/
    //TMP TO TEST ENCODING

    print '<div class="wrap">';
    print "<h1>$title</h1>";

//TMP TO TEST ENCODING
    //print '<div><p>Test carte: 4530 1234 4567 7890  --  2016/10  --  123</p><p>'.$encodedNumber.'</p></div>';
//TMP TO TEST ENCODING
    
    print '<div><p>Décodeur</p>
    		<p>
                <form id="" name="" method="post" action="/wp-admin/admin.php?page=decodeur-CC">
                <input type="text" id="decoder" name="decoder" style="width:430px" />
                <input type="submit" value="Décoder" /></form>
            </p>
          </div>';
    
    if($code != '')
    {
        $value = cc_Encode(hex2bin($code),$secretPass); 
        $annee = substr($value,0,4);
        $mois = substr($value,4,2);
        $numero = substr($value,6);
        $numParts = explode('_', $numero);
        $numero = $numParts[0];
        $numeroSec = isset($numParts[1])?$numParts[1]:'';
        
        print '<p>numéro: '.trim(chunk_split($numero, 4, ' ')).'<br/>mois : '.$mois.'<br/>année : '.$annee.'<br>code de sécurité : '.$numeroSec.'</p>';   
    }

    print '</div>';
}

function cc_Encode($data,$pwd)
{
    $pwd_length = strlen($pwd);
    for ($i = 0; $i < 255; $i++) {
        $key[$i] = ord(substr($pwd, ($i % $pwd_length)+1, 1));
        $counter[$i] = $i;
    }
    for ($i = 0; $i < 255; $i++) {
        $x = ($x + $counter[$i] + $key[$i]) % 256;
        $temp_swap = $counter[$i];
        $counter[$i] = $counter[$x];
        $counter[$x] = $temp_swap;
    }
    for ($i = 0; $i < strlen($data); $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $counter[$a]) % 256;
        $temp = $counter[$a];
        $counter[$a] = $counter[$j];
        $counter[$j] = $temp;
        $k = $counter[(($counter[$a] + $counter[$j]) % 256)];
        $Zcipher = ord(substr($data, $i, 1)) ^ $k;
        $Zcrypt .= chr($Zcipher);
    }
    return $Zcrypt;
}

function hex2bin($hexdata) {
    for ($i=0;$i<strlen($hexdata);$i+=2) {
        $bindata.=chr(hexdec(substr($hexdata,$i,2)));
    }  
    return $bindata;
}

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



function custom_rewrite_rule() {
   add_rewrite_rule('^voyages-sur-mesure/(.*)/?','index.php?page_id=257','top');
   add_rewrite_rule('^voyages-dexception/(.*)/?','index.php?page_id=259','top');
   add_rewrite_rule('^croisieres/(.*)/?','index.php?page_id=261','top');
   add_rewrite_rule('^departs-de-groupes/(.*)/?','index.php?page_id=263','top');
}
add_action('init', 'custom_rewrite_rule', 10, 0);

function rel_canonical_2() {
    if ( is_singular() ) {
		$obj       = get_queried_object();
		$canonical = get_permalink( $obj->ID );

		$canonical_override = WPSEO_Meta::get_value( 'canonical' );

		// Fix paginated pages canonical, but only if the page is truly paginated.
		if ( get_query_var( 'page' ) > 1 ) {
			$num_pages = ( substr_count( $obj->post_content, '<!--nextpage-->' ) + 1 );
			if ( $num_pages && get_query_var( 'page' ) <= $num_pages ) {
				if ( ! $GLOBALS['wp_rewrite']->using_permalinks() ) {
					$link = add_query_arg( 'page', get_query_var( 'page' ), $canonical );
				}
				else {
					$link = user_trailingslashit( trailingslashit( $canonical ) . get_query_var( 'page' ) );
				}
			}
		}
	}

    global $wp_the_query;
    if ( !$id = $wp_the_query->get_queried_object_id() )
        return;

    $link = get_permalink( $id );

    if ( $page = get_query_var('cpage') )
        $link = get_comments_pagenum_link( $page );

    if ( $id == 257 || $id == 259 || $id == 261 || $id == 263 )
    	$link = curPageURL();

    echo "<link rel='canonical' href='$link' />\n";
}

function rel_canonical_3() {
	$canonical          = false;
	$canonical_override = false;

	// Set decent canonicals for homepage, singulars and taxonomy pages.
	if ( is_singular() ) {
		$obj       = get_queried_object();
		$canonical = get_permalink( $obj->ID );

		$canonical_override = WPSEO_Meta::get_value( 'canonical' );

		// Fix paginated pages canonical, but only if the page is truly paginated.
		if ( get_query_var( 'page' ) > 1 ) {
			$num_pages = ( substr_count( $obj->post_content, '<!--nextpage-->' ) + 1 );
			if ( $num_pages && get_query_var( 'page' ) <= $num_pages ) {
				if ( ! $GLOBALS['wp_rewrite']->using_permalinks() ) {
					$canonical = add_query_arg( 'page', get_query_var( 'page' ), $canonical );
				}
				else {
					$canonical = user_trailingslashit( trailingslashit( $canonical ) . get_query_var( 'page' ) );
				}
			}
		}
	}
	else {
		if ( is_search() ) {
			$canonical = get_search_link();
		}
		elseif ( is_front_page() ) {
			$canonical = home_url();
		}
		elseif ( is_home() ) {
			$canonical = get_permalink( get_option( 'page_for_posts' ) );
		}
		elseif ( is_tax() || is_tag() || is_category() ) {
			$term = get_queried_object();

			$canonical_override = WPSEO_Taxonomy_Meta::get_term_meta( $term, $term->taxonomy, 'canonical' );

			$canonical = get_term_link( $term, $term->taxonomy );
		}
		elseif ( is_post_type_archive() ) {
			$post_type = get_query_var( 'post_type' );
			if ( is_array( $post_type ) ) {
				$post_type = reset( $post_type );
			}
			$canonical = get_post_type_archive_link( $post_type );
		}
		elseif ( is_author() ) {
			$canonical = get_author_posts_url( get_query_var( 'author' ), get_query_var( 'author_name' ) );
		}
		elseif ( is_archive() ) {
			if ( is_date() ) {
				if ( is_day() ) {
					$canonical = get_day_link( get_query_var( 'year' ), get_query_var( 'monthnum' ), get_query_var( 'day' ) );
				}
				elseif ( is_month() ) {
					$canonical = get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) );
				}
				elseif ( is_year() ) {
					$canonical = get_year_link( get_query_var( 'year' ) );
				}
			}
		}

		if ( $canonical && get_query_var( 'paged' ) > 1 ) {
			global $wp_rewrite;
			if ( ! $wp_rewrite->using_permalinks() ) {
				if ( is_front_page() ) {
					$canonical = trailingslashit( $canonical );
				}
				$canonical = add_query_arg( 'paged', get_query_var( 'paged' ), $canonical );
			}
			else {
				if ( is_front_page() ) {
					$canonical = wpseo_xml_sitemaps_base_url( '' );
				}
				$canonical = user_trailingslashit( trailingslashit( $canonical ) . trailingslashit( $wp_rewrite->pagination_base ) . get_query_var( 'paged' ) );
			}
		}
	}


	global $wp_the_query;
	$id = $wp_the_query->get_queried_object_id();

	if ( $id == 257 || $id == 259 || $id == 261 || $id == 263 )
    	$canonical = curPageURL();

	echo "<link rel='canonical' href='$canonical' />\n";
}

function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";

	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	
	return $pageURL;
}


remove_action('wp_head', 'rel_canonical');
add_filter( 'wpseo_canonical', '__return_false' );
add_action('wp_head', 'rel_canonical_3');



// Ajouter voyage dans formulaires
add_action("wpcf7_before_send_mail", "wpcf7_do_something_else_with_the_data");
function wpcf7_do_something_else_with_the_data($cf7)
{
	$voyage = $_GET["voyage"];

	if( $voyage ){

		if( get_post_type( $voyage ) == 'voyages' ){

			$voyageOK = true;

			$titre = get_the_title( $voyage );

		}

	}
	$mail = $cf7->prop('mail');
	$mail['body'] = str_replace("[voyage]", $titre, $mail['body']);
	$cf7->set_properties(array("mail" => $mail));
}



// Helper pour simplifier la comparaison des langues 

function langWp($lang) {
	if ($lang === ICL_LANGUAGE_CODE) {
		return true;
	}
	else {
		return false;
	}	
}

// Ajoute un classe css pour la portion anglaise
add_filter('body_class', 'append_language_class');
function append_language_class($classes){
  $classes[] = "languague-wpml-".ICL_LANGUAGE_CODE;
  return $classes;
}
