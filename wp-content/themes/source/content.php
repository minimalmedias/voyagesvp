<?php
/**
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */
?>
<div class="col-sm-6 col-md-4 result">
<article id="post-<?php the_ID(); ?>" <?php post_class("boite"); ?>>
	<a href="<?php the_permalink(); ?>" rel="bookmark">
		<div class="post-img equal <?php if (!has_post_thumbnail()) {echo "placeholder"; } ?>">
			<?php 
			if (has_post_thumbnail()) {
				echo the_post_thumbnail("resultat-img"); 
			} else {
				?><img src="<?php echo get_template_directory_uri(); ?>/images/Logo<?php if(ICL_LANGUAGE_CODE=='en') {echo "-en";}?>.png"><?php
			}
			?>
		</div>
		<header class="entry-header equal2 clearfix">
			<h3 class="entry-title"><?php the_title(); ?></h3>
		</header><!-- .entry-header -->
	</a>
	<div class="media authordate equal3">
		<?php
		$authorid = get_the_author_meta( 'ID' );
		$lien_vers_la_page_de_profil = get_field("lien_vers_la_page_de_profil",  "user_".$authorid."");
		$photo = get_field("photo", "user_".$authorid."");
		 ?>
			<div class="media-left">
				<a href="<?php echo $lien_vers_la_page_de_profil; ?>"><?php 
					if ($photo) {
						echo "<img src='".$photo["url"]."' />";
					} else {
						echo get_avatar(get_the_author_meta( 'ID' )); 
					}
				?></a>
			</div>
			<div class="media-body">
				<div><a href="<?php echo $lien_vers_la_page_de_profil; ?>"><?php echo get_the_author(); ?></a></div>
				<div><?php echo get_the_date(); ?></div>
			</div>
		
	</div>
</article><!-- #post-## -->
</div>