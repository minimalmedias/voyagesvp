<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author Matthias Thom | http://upplex.de
 * @package upBootWP 0.1
 */

get_header(); ?>
<div class="bloc-principal-voyage-daffaire">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="page-title text-center">
					<?php
						if ( is_category() ) :
							echo 'Blogue - '; single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( '%s', 'upbootwp' ), '<span class="vcard">Blogue - ' . get_the_author() . '</span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'upbootwp' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'upbootwp' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'upbootwp' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'upbootwp' );

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'upbootwp');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'upbootwp' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'upbootwp' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'upbootwp' );

						else :
							_e( 'Archives', 'upbootwp' );

						endif;
					?>
				</h1>
			</div>
			<div class="col-sm-4">
				<div class="formulaire-container">
					<?php get_sidebar(); ?>
				</div>
			</div>
			<div class="col-sm-8">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
					<?php
						global $wp_query;
						$term = get_term_by( 'slug', 'videos', 'post_tag' );
						$args = array_merge( $wp_query->query_vars, array( 'tag__not_in' => array( $term->term_id ), 'posts_per_page' => '9' ) );
						query_posts( $args );
					?>
					<?php if ( have_posts() ) : ?>
			
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
			
							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							?>
			
						<?php endwhile; ?>
			
						<?php upbootwp_content_nav( 'nav-below' ); ?>
			
					<?php else : ?>
			
						<?php get_template_part( 'no-results', 'archive' ); ?>
			
					<?php endif; 
					wp_reset_postdata(); ?>
			
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .col-md-8 -->
		</div><!-- .row -->
	</div><!-- .container -->
</dov>
<?php get_footer(); ?>
