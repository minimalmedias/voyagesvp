<?php
	//Banner - Slider or not
?>
<?php if( have_rows('banner') ): ?>
	<?php $i = 0;?>
	<?php $indicators = ''; ?>
	<header class="entry-header">
	    <div id="carousel-banner" class="carousel slide">
	    	<!-- Indicators -->
			<!--<ol class="carousel-indicators">
	    	    <li data-target="#carousel-banner" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-banner" data-slide-to="1"></li>
			    <li data-target="#carousel-banner" data-slide-to="2"></li>
			</ol>-->
	    	
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				 <?php while ( have_rows('banner') ) : the_row(); ?>
				 	<div class="item <?php if($i == 0){echo 'active';}?>">
			        	<!--<img src="<?php echo get_template_directory_uri(); ?>/images/header.jpg" alt="" />-->
			        	<?php $img = get_sub_field('image');?>
				    	<img src="<?php echo $img["url"]; ?>" alt="" />
				    	<?php if(get_sub_field('contenu')){ ?>
					    	<div class="carousel-caption">
								<?php the_sub_field('contenu'); ?>
							</div>
						<?php } ?>
						<?php
							$indicators .= '<li data-target="#carousel-banner" data-slide-to="' . $i . '"';
							if($i == 0){
								$indicators .= ' class="active"';
							}
							$indicators .= '></li>';
						?>
			        </div>
				 	<?php ++$i; ?>
				<?php endwhile; ?>
			</div>
	    	
	    	<?php if($i > 1){ ?>
				<!-- Controls -->
				<ol class="carousel-indicators">
					<?php echo $indicators; ?>
				</ol>
			<?php } ?>
	    </div>
	</header>

<?php else : ?>


<?php endif; ?>