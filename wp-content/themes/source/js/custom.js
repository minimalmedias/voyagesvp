$(document).ready(function(){

	$('.equal').matchHeight({
    byRow: false});
	$('.equal2').matchHeight();
	$('.equal3').matchHeight();

	// yolo tiroirs
	$(".menu-thematiques li.dropdown .dropdown-open").click(function(e) {
		$(this).closest("li.dropdown").find(".sub-menu").slideToggle();
		e.preventDefault();
	});

	$(".sub-menu .active").closest(".sub-menu").slideDown(0);

	// disable clicks on dropdown links
	/*$(".menu-thematiques li.dropdown a").click(function(e) {
		e.preventDefault();
	});*/

	$('.site-header .search-toggle').click(function(){
		$('.site-header .search-form').slideToggle();
	});

	$('.add-rel-prev a').attr('rel', 'prev');
	$('.add-rel-next a').attr('rel', 'next');

	$('.owl-carousel-haut').owlCarousel({
		animateOut: 'fadeOut',
		mouseDrag:false,
		touchDrag:true,
		items:1,
		margin:0,
		nav:true,
		loop:true,
		autoplay:true,
		autoplayTimeout:4000,
		autoplayHoverPause:false
	});

	$('.owl-carousel-bas').owlCarousel({
	    mouseDrag:false,
	    touchDrag:true,
	    responsive:{
	        0:{
	            items:1,
	            stagePadding:0,
	            margin:10
	        },
	        768:{
	            items:2,
				stagePadding: 0,
				margin:30
	        },
	        992:{
	            items:3,
				stagePadding: 0,
				margin:30
	        }
	    }
	});

	$('.owl-carousel-menu-horizontal').owlCarousel({
	    mouseDrag:false,
	    touchDrag:true,
	    responsive:{
	        0:{
	            items:1,
	            stagePadding:1,
	            margin:10
	        },
	        768:{
	            items:2,
				stagePadding: 90,
				margin:30
	        },
	        992:{
	            items:3,
				stagePadding: 90,
				margin:30
	        },
	        1400:{
	            items:4,
				stagePadding: 1,
				margin:30
	        }
	    }
	});

	$(".owl-bas-next").click(function(){
    	$(this).closest('.owl-carousel-container').find('.owl-carousel').trigger('next.owl');
	});
	$(".owl-bas-prev").click(function(){
		$(this).closest('.owl-carousel-container').find('.owl-carousel').trigger('prev.owl');
	});

	var delai = 300;
	var time = 100;
	/* Menu vertical */
	$('.dropdown>a.dropdown-toggle').mouseover(function() {
		if($(window).width() >= 768){
			var $dropdown = $(this);
			var $sousmenu = $(this).closest('.dropdown').find('.dropdown-menu');
			$sousmenu.addClass('open');
			setTimeout(function(){
				if($sousmenu.hasClass('open')){
					$sousmenu.slideDown(time);
					$dropdown.addClass('triangle-visible');
				}
			}, delai);
		}
	});
	$('.dropdown>a.dropdown-toggle').mouseleave(function() {
		if($(window).width() >= 768){
			var $dropdown = $(this);
			var $sousmenu = $(this).closest('.dropdown').find('.dropdown-menu');
			$sousmenu.removeClass('open');
			setTimeout(function(){
				if(! $sousmenu.hasClass('open')){
					$sousmenu.slideUp(time);
					$dropdown.removeClass('triangle-visible');
				}
			}, delai);
		}
	});
	$('.dropdown>.dropdown-menu').mouseover(function() {
		if($(window).width() >= 768){
			var $dropdown = $(this).closest('.dropdown').find('a.dropdown-toggle');
			var $sousmenu = $(this);
			$sousmenu.addClass('open');
			setTimeout(function(){
				if($sousmenu.hasClass('open')){
					$sousmenu.slideDown(time);
					$dropdown.addClass('triangle-visible');
				}
			}, delai);
		}
	});
	$('.dropdown>.dropdown-menu').mouseleave(function() {
		if($(window).width() >= 768){
			var $dropdown = $(this).closest('.dropdown').find('a.dropdown-toggle');
			var $sousmenu = $(this);
			$sousmenu.removeClass('open');
			setTimeout(function(){
				if(! $sousmenu.hasClass('open')){
					$sousmenu.slideUp(time);
					$dropdown.removeClass('triangle-visible');
				}
			}, delai);
		}
	});

	/* Menu horizontal */
	$('.menu-horizontal a').mouseover(function() {
		var $dropdown = $(this);
		var number = $dropdown.closest( ".menu-horizontal" ).index( ".sous-menu>li" );
		var $sousmenu = $(".sous-menu-horizontal[data-number='" + number + "']");
		$sousmenu.css('display', 'none');
		$sousmenu.removeClass('invisible');
		$sousmenu.addClass('open');

		var href = $dropdown.attr('href');
		$sousmenu.find('a.lien-tous').attr('href', href);

		$sousmenu.find('.carousel-pays a.lien-boite').each(function(){
			var slug = $(this).data('slug');
			$(this).attr('href', href + slug + '/');
		});

		$sousmenu.find('.carousel-theme a.lien-boite').each(function(){
			var slug = $(this).data('slug');
			$(this).attr('href', href + slug + '/');
		});

		$sousmenu.find('.carousel-compagnie a.lien-boite').each(function(){
			var slug = $(this).data('slug');
			$(this).attr('href', href + slug + '/');
		});

		setTimeout(function(){
			if($sousmenu.hasClass('open')){
				$sousmenu.slideDown(time);
				$dropdown.addClass('triangle-visible');
			}
		}, delai);
	});
	$('.menu-horizontal a').mouseleave(function() {
		var $dropdown = $(this);
		var number = $dropdown.closest( ".menu-horizontal" ).index( ".sous-menu>li" );
		var $sousmenu = $(".sous-menu-horizontal[data-number='" + number + "']");
		$sousmenu.removeClass('open');
		setTimeout(function(){
			if(! $sousmenu.hasClass('open')){
				$sousmenu.slideUp(time);
				$dropdown.removeClass('triangle-visible');
				setTimeout(function(){
					$sousmenu.css('display', 'block');
					$sousmenu.addClass('invisible');
				}, time + 50);
			}
		}, delai);
	});

	$('.sous-menu-horizontal').mouseover(function() {
		var $sousmenu = $(this);
		var number = $sousmenu.data('number') + 1;
		var $dropdown = $('.sous-menu>li:nth-child(' + number + ') a');
		$sousmenu.addClass('open');
		setTimeout(function(){
			if($sousmenu.hasClass('open')){
				$sousmenu.slideDown(time);
				$dropdown.addClass('triangle-visible');
			}
		}, delai);
	});
	$('.sous-menu-horizontal').mouseleave(function() {
		var $sousmenu = $(this);
		var number = $sousmenu.data('number') + 1;
		var $dropdown = $('.sous-menu>li:nth-child(' + number + ') a');
		$sousmenu.removeClass('open');
		setTimeout(function(){
			if(! $sousmenu.hasClass('open')){
				$sousmenu.slideUp(time);
				$dropdown.removeClass('triangle-visible');
				setTimeout(function(){
					$sousmenu.css('display', 'block');
					$sousmenu.addClass('invisible');
				}, time + 50);
			}
		}, delai);
	});

	$('.lien-pays').click(function(){
		if( !$(this).hasClass('active') ){
			var $sousmenu = $(this).closest('.sous-menu-horizontal');
			var $pays = $sousmenu.find('.carousel-pays');
			var $theme = $sousmenu.find('.carousel-theme');
			var $compagnie = $sousmenu.find('.carousel-compagnie');
			var $autreBouton1 = $sousmenu.find('.lien-theme');
			var $autreBouton2 = $sousmenu.find('.lien-compagnie');

			$autreBouton1.removeClass('active');
			$autreBouton2.removeClass('active');
			$(this).addClass('active');
			$pays.removeClass('invisible');
			$pays.height('auto');
			$pays.css('overflow', 'visible');
			$theme.addClass('invisible');
			$theme.height(0);
			$theme.css('overflow', 'hidden');
			$compagnie.addClass('invisible');
			$compagnie.height(0);
			$compagnie.css('overflow', 'hidden');
		}
	});

	$('.lien-theme').click(function(){
		if( !$(this).hasClass('active') ){
			var $sousmenu = $(this).closest('.sous-menu-horizontal');
			var $pays = $sousmenu.find('.carousel-pays');
			var $theme = $sousmenu.find('.carousel-theme');
			var $compagnie = $sousmenu.find('.carousel-compagnie');
			var $autreBouton1 = $sousmenu.find('.lien-pays');
			var $autreBouton2 = $sousmenu.find('.lien-compagnie');

			$autreBouton1.removeClass('active');
			$autreBouton2.removeClass('active');
			$(this).addClass('active');
			$pays.addClass('invisible');
			$pays.height(0);
			$pays.css('overflow', 'hidden');
			$theme.removeClass('invisible');
			$theme.height('auto');
			$theme.css('overflow', 'visible');
			$compagnie.addClass('invisible');
			$compagnie.height(0);
			$compagnie.css('overflow', 'hidden');
		}
	});

	$('.lien-compagnie').click(function(){
		if( !$(this).hasClass('active') ){
			var $sousmenu = $(this).closest('.sous-menu-horizontal');
			var $pays = $sousmenu.find('.carousel-pays');
			var $theme = $sousmenu.find('.carousel-theme');
			var $compagnie = $sousmenu.find('.carousel-compagnie');
			var $autreBouton1 = $sousmenu.find('.lien-theme');
			var $autreBouton2 = $sousmenu.find('.lien-pays');

			$autreBouton1.removeClass('active');
			$autreBouton2.removeClass('active');
			$(this).addClass('active');
			$pays.addClass('invisible');
			$pays.height(0);
			$pays.css('overflow', 'hidden');
			$theme.addClass('invisible');
			$theme.height(0);
			$theme.css('overflow', 'hidden');
			$compagnie.removeClass('invisible');
			$compagnie.height('auto');
			$compagnie.css('overflow', 'visible');
		}
	});

	var mmSpeed = 200;

	$('.liste-membres .membre a').click(function(){
		var url = $(this).data("pageurl");
		var nom = $(this).data("nom");
		var img = $(this).data("img");
		if($(window).width()>=768){
			window.location.href = url;
		}
		else{
			$('.membre-mobile .nom-membre img').attr("src", img);
			$('.membre-mobile .nom-membre h2').html(nom);
			$('.membre-mobile .btn-membre').attr("href", url);
			$('.membre-mobile').slideDown(mmSpeed);
		}
	});

	$(document).click(function(event) { 
	    if(!$(event.target).closest('.membre-mobile').length && !$(event.target).closest('.liste-membres .membre a').length) {
	        $('.membre-mobile').slideUp(mmSpeed);
	    }        
	});

	$('.membre-mobile .close-mm').click(function(){
		$('.membre-mobile').slideUp(mmSpeed);
	});
	

	jQuery("a[rel^='prettyPhoto']").prettyPhoto({
		theme: 'pp_default',
		social_tools: '',
		deeplinking:false,
	});

	if(!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i).test(navigator.userAgent) ) {

		jQuery(".galerie-video a[rel^='prettyPhoto']").prettyPhoto({
			theme: 'pp_default',
			social_tools: '<span class="st_facebook_hcount" id="galerie-photo-facebook"></span><span class="st_pinterest_hcount" id="galerie-photo-pinterest"></span>',
			deeplinking:false,
			changepicturecallback: function(){
				var iframesrc = $('#pp_full_res iframe').attr('src');
				iframesrc = iframesrc.replace("http://", "https://");
				$('#pp_full_res iframe').attr('src', iframesrc)
				var url = $('#video-url').text();
				var src = $('#pp_full_res iframe').attr('src');
				$('#galerie-photo-facebook').attr('st_url', url);
				$('#galerie-photo-pinterest').attr('st_url', url);
				stButtons.locateElements();
			},
		});

	} else {
		jQuery(".galerie-video a[rel^='prettyPhoto']").attr('href', $(this).attr('href').replace('www','m')).attr('target', '_blank');
	}
	
	jQuery("a[rel^='prettyPhoto[Galery]']").prettyPhoto({
		theme: 'pp_default',
		social_tools: '<span class="st_facebook_hcount" id="galerie-facebook"></span><span class="st_pinterest_hcount" id="galerie-pinterest"></span>',
		deeplinking:false,
		changepicturecallback: function(){
			var src = $('#fullResImage').attr("src");
			$('#galerie-facebook').attr('st_image', src);
			$('#galerie-pinterest').attr('st_image', src);
			stButtons.locateElements();
		},
	});

	$('.voir-tous-les-prix').click(function(){
		$('#bouton-prix').trigger('click');
		$('html, body').animate({
			scrollTop: ($(".tab-content").offset().top)
		}, 'slow'); 
		return false;
	});

	$('.banniere-bleu-gris .navbar-collapse-fiche-voyage ul li a').click(function(){
		var $ul = $(this).closest('ul');
		var $li = $(this).closest('li');
		var number = $li.index( 'ul.tab-selector-mobile>li' ) + 1;
		$('.banniere-bleu-gris .navbar-collapse-fiche-voyage ul li.active').removeClass('active');
		$li.addClass('active');
		$('.tab-selector ul li:nth-child(' + number + ') a').trigger('click');
		$('html, body').animate({
			scrollTop: ($(".tab-content").offset().top)
		}, 'slow'); 
		return false;
	});

	if( $('#titre-du-voyage').length && $('#voyage-hidden').length){
		var titreVoyage = $('#titre-du-voyage').text();
		$('#voyage-hidden').val( titreVoyage );
	}

	$('#checkbox-1-container input').attr('id', 'checkbox-1');
	$('#checkbox-2-container input').attr('id', 'checkbox-2');
	$('#checkbox-3-container input').attr('id', 'checkbox-3');
	$('#checkbox-4-container input').attr('id', 'checkbox-4');
	$('#checkbox-5-container input').attr('id', 'checkbox-5');

	boiteEqualizer();
	carouselBasEqualizer();

});

jQuery(window).resize(function() {
	boiteEqualizer();
	carouselBasEqualizer();
});

function boiteEqualizer(){
	$('.boite').matchHeight();
}

function carouselBasEqualizer(){
	$('.boite-coup-de-coeur .info').matchHeight();
}

function responsiveMenu() {
    if($(window).width() <= 767){
        $('.dropdown-toggle.disabled').removeClass("disabled");
    } else {
        $('.dropdown-toggle').addClass("disabled");
    }
}

$(window).on("load resize scroll",function(e){
    responsiveMenu();
    $('.membre-mobile').css('top', window.pageYOffset );
});

function validaterInfolettreFooter(){
	var x = document.forms["infolettreFooter"]["courriel"].value;
	if (x == null || x == "") {
		return false;
	}
}

function validaterInfolettre(){
	var x = document.forms["infolettre"]["courriel"].value;
	if (x == null || x == "") {
		$('#error-span').css('visibility', 'visible');
		$('#courriel-label').css('color', 'red');
		return false;
	}
}
