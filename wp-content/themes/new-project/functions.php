<?php
function client_enqueues() {
    // All site CSS combined and minified into style.css via Sass & Gulp
    wp_register_style('client-css', get_template_directory_uri() . '/assets/css/style.css', false, null);
    wp_enqueue_style('client-css');
    // All site JS combined and minified into client.js via Gulp
    wp_register_script('client-js', get_template_directory_uri() . '/assets/js/client.js', false, null, true);
    wp_enqueue_script('client-js');
}
add_action('wp_enqueue_scripts', 'client_enqueues', 100);

